package tw.edu.ntu.esoe.hsru.web.data.station;

public class PointType {

	private double positionLat = 0;
	private double positionLon = 0;
	private String geoHash = "";

	public double getPositionLat() {
		return positionLat;
	}

	public double getPositionLon() {
		return positionLon;
	}

	public String getGeoHash() {
		return geoHash;
	}

	@Override
	public String toString() {
		return "PointType [positionLat=" + positionLat + ", positionLon=" + positionLon + ", geoHash=" + geoHash + "]";
	}
}
