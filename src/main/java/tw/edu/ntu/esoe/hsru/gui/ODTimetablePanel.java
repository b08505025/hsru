package tw.edu.ntu.esoe.hsru.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Optional;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import com.github.lgooddatepicker.components.DatePicker;
import com.github.lgooddatepicker.components.TimePicker;
import com.github.lgooddatepicker.optionalusertools.DateChangeListener;
import com.github.lgooddatepicker.optionalusertools.TimeChangeListener;
import com.github.lgooddatepicker.zinternaltools.DateChangeEvent;
import com.github.lgooddatepicker.zinternaltools.TimeChangeEvent;

import tw.edu.ntu.esoe.hsru.data.Station;
import tw.edu.ntu.esoe.hsru.userdata.Settings;
import tw.edu.ntu.esoe.hsru.web.data.timetable.od.Discount;
import tw.edu.ntu.esoe.hsru.web.data.timetable.od.DiscountType;
import tw.edu.ntu.esoe.hsru.web.data.timetable.od.ODTimetableData;
import tw.edu.ntu.esoe.hsru.web.data.timetable.od.ODTimetableRequest;
import tw.edu.ntu.esoe.hsru.web.data.timetable.od.PriceTable;
import tw.edu.ntu.esoe.hsru.web.data.timetable.od.PriceTableColumn;
import tw.edu.ntu.esoe.hsru.web.data.timetable.od.StationInfo;
import tw.edu.ntu.esoe.hsru.web.data.timetable.od.TrainItem;

/**
 * The origin-to-destination timetable panel, replicating the timetable and fare search function found on the
 * THSR website.
 */
public class ODTimetablePanel extends JPanel {

	public static final Settings SETTINGS = HsruWindow.SETTINGS;

	public static final DateTimeFormatter DATE = DateTimeFormatter.ofPattern("yyyy/MM/dd");
	public static final DateTimeFormatter TIME = DateTimeFormatter.ofPattern("HH:mm");

	public static final Color GAINSBORO = new Color(0xDDDDDD);

	public static String[] stationNames = {
			"南港", "台北", "板橋", "桃園", "新竹", "苗栗", "台中", "彰化", "雲林", "嘉義", "台南", "左營"
	};
	public static String[] tripType = {
			"單程", "去回程"
	};

	private final JComboBox fromStationComboBox;
	private final JLabel fromStationLabel;
	private final JButton swapButton;
	private final JComboBox toStationComboBox;
	private final JLabel toStationLabel;
	private final JComboBox tripTypeComboBox;
	private final DatePicker outboundDatePicker;
	private final JLabel outboundDateLabel;
	private final TimePicker outboundTimePicker;
	private final JLabel outboundTimeLabel;
	private final DatePicker inboundDatePicker;
	private final JLabel inboundDateLabel;
	private final TimePicker inboundTimePicker;
	private final JLabel inboundTimeLabel;
	private final JCheckBox earlyBirdCheckBox;
	private final JCheckBox collegeStudentCheckBox;
	private final JLabel discountLabel;
	private final JButton searchButton;
	private final DefaultTableModel outboundTableModel;
	private final JTable outboundTable;
	private final JScrollPane outboundScrollPane;
	private final DefaultTableModel inboundTableModel;
	private final JTable inboundTable;
	private final JScrollPane inboundScrollPane;
	private final DefaultTableModel priceTableModel;
	private final JTable priceTable;
	private final JScrollPane priceScrollPane;
	private final JTabbedPane tabs;

	private boolean searching = false;

	private int outboundFromColumn = -1;
	private int outboundToColumn = -1;
	private int inboundFromColumn = -1;
	private int inboundToColumn = -1;

	ODTimetablePanel() {
		super();

		setLayout(null);

		fromStationComboBox = new JComboBox(stationNames);
		fromStationComboBox.setEditable(false);
		fromStationComboBox.setSelectedIndex(0);
		fromStationComboBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				searchButton.setEnabled(fromStationComboBox.getSelectedIndex() != toStationComboBox.getSelectedIndex());
			}
		});
		add(fromStationComboBox);

		fromStationLabel = new JLabel("起程站");
		add(fromStationLabel);

		swapButton = new JButton("⇄");
		swapButton.setMargin(new Insets(0, 0, 0, 0));
		swapButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int fromIndex = fromStationComboBox.getSelectedIndex();
				int toIndex = toStationComboBox.getSelectedIndex();
				fromStationComboBox.setSelectedIndex(toIndex);
				toStationComboBox.setSelectedIndex(fromIndex);
			}
		});
		add(swapButton);

		toStationComboBox = new JComboBox(stationNames);
		toStationComboBox.setEditable(false);
		toStationComboBox.setSelectedIndex(11);
		toStationComboBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				searchButton.setEnabled(canSearch());
			}
		});
		add(toStationComboBox);

		toStationLabel = new JLabel("到達站");
		add(toStationLabel);

		tripTypeComboBox = new JComboBox(tripType);
		tripTypeComboBox.setEditable(false);
		tripTypeComboBox.setSelectedIndex(0);
		tripTypeComboBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(tripTypeComboBox.getSelectedIndex() == 1) {
					inboundDatePicker.setVisible(true);
					inboundDateLabel.setVisible(true);
					inboundTimePicker.setVisible(true);
					inboundTimeLabel.setVisible(true);
				}
				else {
					inboundDatePicker.setVisible(false);
					inboundDateLabel.setVisible(false);
					inboundTimePicker.setVisible(false);
					inboundTimeLabel.setVisible(false);
				}
			}
		});
		add(tripTypeComboBox);

		outboundDatePicker = new DatePicker();
		outboundDatePicker.getSettings().setSizeTextFieldMinimumWidth(0);
		outboundDatePicker.getSettings().setSizeTextFieldMinimumWidthDefaultOverride(false);
		outboundDatePicker.getSettings().setFormatForDatesCommonEra(DATE);
		outboundDatePicker.getSettings().setFormatForTodayButton(DATE);
		outboundDatePicker.getSettings().setAllowEmptyDates(false);
		outboundDatePicker.getSettings().setVetoPolicy(this::isOutboundDateAllowed);
		outboundDatePicker.setDateToToday();
		outboundDatePicker.addDateChangeListener(new DateChangeListener() {
			@Override
			public void dateChanged(DateChangeEvent e) {
				if(!isInboundDateAllowed(inboundDatePicker.getDate())) {
					inboundDatePicker.setDate(outboundDatePicker.getDate());
				}
				if(!isInboundTimeAllowed(inboundTimePicker.getTime())) {
					inboundTimePicker.setTime(nextTwoHalfHours(outboundTimePicker.getTime().minusMinutes(30)));
				}
			}
		});
		add(outboundDatePicker);

		outboundDateLabel = new JLabel("去程日期");
		add(outboundDateLabel);

		outboundTimePicker = new TimePicker();
		outboundTimePicker.getSettings().setSizeTextFieldMinimumWidth(0);
		outboundTimePicker.getSettings().setSizeTextFieldMinimumWidthDefaultOverride(false);
		outboundTimePicker.getSettings().setFormatForDisplayTime(TIME);
		outboundTimePicker.getSettings().setFormatForMenuTimes(TIME);
		outboundTimePicker.getSettings().setAllowEmptyTimes(false);
		outboundTimePicker.getSettings().setVetoPolicy(this::isOutboundTimeAllowed);
		outboundTimePicker.setTime(nextHalfHour(LocalTime.now()));
		outboundTimePicker.addTimeChangeListener(new TimeChangeListener() {
			@Override
			public void timeChanged(TimeChangeEvent e) {
				if(!isInboundTimeAllowed(inboundTimePicker.getTime())) {
					inboundTimePicker.setTime(nextTwoHalfHours(outboundTimePicker.getTime().minusMinutes(30)));
				}
			}
		});
		add(outboundTimePicker);

		outboundTimeLabel = new JLabel("去程時刻");
		add(outboundTimeLabel);

		inboundDatePicker = new DatePicker();
		inboundDatePicker.getSettings().setSizeTextFieldMinimumWidth(0);
		inboundDatePicker.getSettings().setSizeTextFieldMinimumWidthDefaultOverride(false);
		inboundDatePicker.getSettings().setFormatForDatesCommonEra(DATE);
		inboundDatePicker.getSettings().setFormatForTodayButton(DATE);
		inboundDatePicker.getSettings().setAllowEmptyDates(false);
		inboundDatePicker.getSettings().setVetoPolicy(this::isInboundDateAllowed);
		inboundDatePicker.setDateToToday();
		add(inboundDatePicker);
		inboundDatePicker.setVisible(false);

		inboundDateLabel = new JLabel("回程日期");
		add(inboundDateLabel);
		inboundDateLabel.setVisible(false);

		inboundTimePicker = new TimePicker();
		inboundTimePicker.getSettings().setSizeTextFieldMinimumWidth(0);
		inboundTimePicker.getSettings().setSizeTextFieldMinimumWidthDefaultOverride(false);
		inboundTimePicker.getSettings().setFormatForDisplayTime(TIME);
		inboundTimePicker.getSettings().setFormatForMenuTimes(TIME);
		inboundTimePicker.getSettings().setAllowEmptyTimes(false);
		inboundTimePicker.getSettings().setVetoPolicy(this::isInboundTimeAllowed);
		inboundTimePicker.setTime(nextTwoHalfHours(LocalTime.now()));
		add(inboundTimePicker);
		inboundTimePicker.setVisible(false);

		inboundTimeLabel = new JLabel("回程時刻");
		add(inboundTimeLabel);
		inboundTimeLabel.setVisible(false);

		earlyBirdCheckBox = new JCheckBox("早鳥");
		add(earlyBirdCheckBox);

		collegeStudentCheckBox = new JCheckBox("大學生");
		add(collegeStudentCheckBox);

		discountLabel = new JLabel("適用優惠");
		add(discountLabel);

		searchButton = new JButton("查詢");
		searchButton.setMargin(new Insets(0, 0, 0, 0));
		searchButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				requestTable();
			}
		});
		add(searchButton);

		outboundTableModel = new ReadOnlyTableModel();
		inboundTableModel = new ReadOnlyTableModel();
		priceTableModel = new ReadOnlyTableModel();

		DefaultTableCellRenderer outboundTableRenderer = new DefaultTableCellRenderer.UIResource() {
			@Override
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
				setBackground(column == 0 || column == outboundFromColumn || column == outboundToColumn ? GAINSBORO : Color.WHITE);
				setForeground(column < 2 || column > 13 || column == outboundFromColumn || column == outboundToColumn ? Color.BLACK : Color.GRAY);
				return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			}
		};
		outboundTableRenderer.setHorizontalAlignment(SwingConstants.CENTER);

		outboundTable = new JTable(outboundTableModel);
		outboundTable.setDefaultRenderer(Object.class, outboundTableRenderer);
		outboundTable.getTableHeader().setReorderingAllowed(false);
		outboundTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		DefaultTableCellRenderer inboundTableRenderer = new DefaultTableCellRenderer.UIResource() {
			@Override
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
				setBackground(column == 0 || column == inboundFromColumn || column == inboundToColumn ? GAINSBORO : Color.WHITE);
				setForeground(column < 2 || column > 13 || column == inboundFromColumn || column == inboundToColumn ? Color.BLACK : Color.GRAY);
				return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			}
		};
		inboundTableRenderer.setHorizontalAlignment(SwingConstants.CENTER);

		inboundTable = new JTable(inboundTableModel);
		inboundTable.setDefaultRenderer(Object.class, inboundTableRenderer);
		inboundTable.getTableHeader().setReorderingAllowed(false);
		inboundTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		DefaultTableCellRenderer priceTableRenderer = new DefaultTableCellRenderer.UIResource() {
			@Override
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
				setBackground(column == 0 ? GAINSBORO : Color.WHITE);
				return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			}
		};
		priceTableRenderer.setHorizontalAlignment(SwingConstants.CENTER);

		priceTable = new JTable(priceTableModel);
		priceTable.setDefaultRenderer(Object.class, priceTableRenderer);
		priceTable.getTableHeader().setReorderingAllowed(false);
		priceTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		outboundScrollPane = new JScrollPane(outboundTable);
		inboundScrollPane = new JScrollPane(inboundTable);
		priceScrollPane = new JScrollPane(priceTable);

		tabs = new JTabbedPane();
		tabs.addTab("去程", outboundScrollPane);
		tabs.addTab("回程", inboundScrollPane);
		tabs.addTab("車廂票價參考", priceScrollPane);
		tabs.setEnabledAt(0, false);
		tabs.setEnabledAt(1, false);
		tabs.setEnabledAt(2, false);
		add(tabs);
	}

	@Override
	public void doLayout() {
		int s = getWidth()/2-290;
		fromStationComboBox.setBounds(s, 20, 70, 20);
		fromStationLabel.setBounds(s, 0, 70, 20);
		swapButton.setBounds(s+75, 20, 20, 20);
		toStationComboBox.setBounds(s+100, 20, 70, 20);
		toStationLabel.setBounds(s+100, 0, 70, 20);
		tripTypeComboBox.setBounds(s+175, 20, 70, 20);
		outboundDatePicker.setBounds(s+250, 20, 105, 20);
		outboundDateLabel.setBounds(s+250, 0, 105, 20);
		outboundTimePicker.setBounds(s+360, 20, 70, 20);
		outboundTimeLabel.setBounds(s+360, 0, 70, 20);
		inboundDatePicker.setBounds(s+250, 60, 105, 20);
		inboundDateLabel.setBounds(s+250, 40, 105, 20);
		inboundTimePicker.setBounds(s+360, 60, 70, 20);
		inboundTimeLabel.setBounds(s+360, 40, 70, 20);
		earlyBirdCheckBox.setBounds(s+435, 20, 65, 20);
		collegeStudentCheckBox.setBounds(s+435, 40, 65, 20);
		discountLabel.setBounds(s+435, 0, 65, 20);
		searchButton.setBounds(s+505, 20, 75, 20);
		tabs.setBounds(0, 80, getWidth(), getHeight()-80);
	}

	private boolean canSearch() {
		return !searching && fromStationComboBox.getSelectedIndex() != toStationComboBox.getSelectedIndex();
	}

	private void requestTable() {
		searching = true;
		searchButton.setEnabled(false);
		Station fromStation = Station.values()[fromStationComboBox.getSelectedIndex()];
		Station toStation = Station.values()[toStationComboBox.getSelectedIndex()];
		boolean roundTrip = tripTypeComboBox.getSelectedIndex() == 1;
		LocalTime outboundTime = outboundTimePicker.getTime();
		LocalTime inboundTime = inboundTimePicker.getTime();
		LocalDateTime outboundDateTime = outboundDatePicker.getDate().atTime(outboundTime);
		LocalDateTime inboundDateTime = inboundDatePicker.getDate().atTime(inboundTime);
		EnumSet<DiscountType> discountTypes = EnumSet.noneOf(DiscountType.class);
		if(earlyBirdCheckBox.isSelected()) {
			discountTypes.add(DiscountType.EARLY_BIRD);
		}
		if(collegeStudentCheckBox.isSelected()) {
			discountTypes.add(DiscountType.COLLEGE_STUDENT);
		}
		new SwingWorker<ODTimetableData, Void>() {
			@Override
			protected ODTimetableData doInBackground() throws Exception {
				return ODTimetableRequest.getTimetableData(
						"TW", fromStation, toStation, roundTrip, outboundDateTime, inboundDateTime, discountTypes);
			}
			@Override
			protected void done() {
				try {
					updateTable(get(), fromStation, toStation, discountTypes, outboundTime, inboundTime);
				}
				catch(Exception e) {
					HsruWindow.showErrorMessage(e);
				}
				searching = false;
				searchButton.setEnabled(canSearch());
			}
		}.execute();
	}

	private void updateTable(ODTimetableData timetableData, Station fromStation, Station toStation, EnumSet<DiscountType> discountTypes, LocalTime outboundTime, LocalTime inboundTime) {
		String[] columns;
		String[][] data;
		List<TrainItem> trainItems;

		tabs.setSelectedIndex(0);

		trainItems = timetableData.getDepartureTable().getTrainItem();
		{
			Station[] stations = getDirection(fromStation, toStation);
			columns = getTimetableColumns(stations, discountTypes);
			data = trainItems.stream().
					map(trainItem->getTimetableRow(trainItem, stations, discountTypes)).
					toArray(String[][]::new);
			outboundTableModel.setDataVector(data, columns);
			for(int i = 0; i < 12; ++i) {
				if(stations[i] == fromStation) {
					outboundFromColumn = i+2;
				}
				if(i == toStation.ordinal()) {
					outboundToColumn = i+2;
				}
			}
			for(int i = 0; i < data.length; ++i) {
				if(!TIME.parse(data[i][outboundFromColumn], LocalTime::from).isBefore(outboundTime)) {
					outboundTable.setRowSelectionInterval(i, i);
					outboundTable.scrollRectToVisible(outboundTable.getCellRect(data.length-1, 0, true));
					outboundTable.scrollRectToVisible(outboundTable.getCellRect(i, 0, true));
					break;
				}
			}
			tabs.setEnabledAt(0, !trainItems.isEmpty());
		}

		trainItems = timetableData.getDestinationTable().getTrainItem();
		{
			Station[] stations = getDirection(toStation, fromStation);
			columns = getTimetableColumns(stations, discountTypes);
			data = trainItems.stream().
					map(trainItem->getTimetableRow(trainItem, stations, discountTypes)).
					toArray(String[][]::new);
			inboundTableModel.setDataVector(data, columns);
			for(int i = 0; i < 12; ++i) {
				if(i == toStation.ordinal()) {
					inboundFromColumn = i+2;
				}
				if(i == fromStation.ordinal()) {
					inboundToColumn = i+2;
				}
			}
			for(int i = 0; i < data.length; ++i) {
				if(!TIME.parse(data[i][inboundFromColumn], LocalTime::from).isBefore(inboundTime)) {
					inboundTable.setRowSelectionInterval(i, i);
					inboundTable.scrollRectToVisible(inboundTable.getCellRect(data.length-1, 0, true));
					inboundTable.scrollRectToVisible(inboundTable.getCellRect(i, 0, true));
					break;
				}
			}
			tabs.setEnabledAt(1, !trainItems.isEmpty());
		}

		PriceTable priceTable = timetableData.getPriceTable();
		columns = getPriceTableColumns(priceTable);
		data = getPriceTableData(priceTable);
		priceTableModel.setDataVector(data, columns);
		tabs.setEnabledAt(2, true);
	}

	private String[] getTimetableColumns(Station[] stations, EnumSet<DiscountType> discountTypes) {
		List<String> list = new ArrayList<>();
		list.add("車次");
		list.add("行車時間");
		for(Station station : stations) {
			list.add(station.getNameZh());
		}
		list.add("自由座車廂");
		if(discountTypes.contains(DiscountType.EARLY_BIRD) || !discountTypes.contains(DiscountType.COLLEGE_STUDENT)) {
			list.add("早鳥");
		}
		if(discountTypes.contains(DiscountType.COLLEGE_STUDENT)) {
			list.add("大學生");
		}
		return list.toArray(new String[list.size()]);
	}

	private String[] getTimetableRow(TrainItem trainItem, Station[] stations, EnumSet<DiscountType> discountTypes) {
		List<String> list = new ArrayList<>();
		list.add(trainItem.getTrainNumber());
		list.add(trainItem.getDuration());
		List<StationInfo> stationInfos = trainItem.getStationInfo();
		boolean started = false;
		for(StationInfo stationInfo : stationInfos) {
			if(stationInfo.isShow()) {
				list.add(stationInfo.getDepartureTime());
				started = true;
			}
			else {
				list.add(started ? "─" : "");
			}
		}
		list.add(trainItem.getNonReservedCar());
		if(discountTypes.contains(DiscountType.EARLY_BIRD) || !discountTypes.contains(DiscountType.COLLEGE_STUDENT)) {
			Optional<Discount> oDiscount = trainItem.getDiscount().stream().filter(DiscountType.EARLY_BIRD::isTypeOf).findAny();
			if(oDiscount.isPresent()) {
				list.add(oDiscount.get().getValue());
			}
			else {
				list.add("");
			}
		}
		if(discountTypes.contains(DiscountType.COLLEGE_STUDENT)) {
			Optional<Discount> oDiscount = trainItem.getDiscount().stream().filter(DiscountType.COLLEGE_STUDENT::isTypeOf).findAny();
			if(oDiscount.isPresent()) {
				list.add(oDiscount.get().getValue());
			}
			else {
				list.add("");
			}
		}
		return list.toArray(new String[list.size()]);
	}

	private Station[] getDirection(Station fromStation, Station toStation) {
		return fromStation.compareTo(toStation) <= 0 ? Station.SOUTHBOUND : Station.NORTHBOUND;
	}

	private String[] getPriceTableColumns(PriceTable priceTable) {
		List<String> list = new ArrayList<>();
		list.add("");
		list.add("全票");
		list.add("優待票");
		list.add("團體票");
		for(PriceTableColumn column : priceTable.getColumn()) {
			list.add(column.getColumnName());
		}
		return list.toArray(new String[list.size()]);
	}

	private String[][] getPriceTableData(PriceTable priceTable) {
		List<String> coach = new ArrayList<>();
		List<String> business = new ArrayList<>();
		List<String> unreserved = new ArrayList<>();
		coach.add("標準車廂");
		business.add("商務車廂");
		unreserved.add("自由座車廂");
		coach.addAll(priceTable.getCoach());
		business.addAll(priceTable.getBusiness());
		unreserved.addAll(priceTable.getUnreserved());
		for(PriceTableColumn column : priceTable.getColumn()) {
			coach.add(column.getCoachPrice());
			business.add(column.getBusinessPrice());
			unreserved.add(column.getUnreserved());
		}
		return new String[][] {
			coach.toArray(new String[coach.size()]),
			business.toArray(new String[business.size()]),
			unreserved.toArray(new String[unreserved.size()]),
		};
	}

	private boolean isOutboundDateAllowed(LocalDate date) {
		long diff = date.toEpochDay()-LocalDate.now().toEpochDay();
		return diff >= 0 && diff <= 28;
	}

	private boolean isOutboundTimeAllowed(LocalTime time) {
		return time.getHour() >= 5;
	}

	private boolean isInboundDateAllowed(LocalDate date) {
		long diff = date.toEpochDay()-LocalDate.now().toEpochDay();
		long diff1 = outboundDatePicker.getDate().toEpochDay()-LocalDate.now().toEpochDay();
		return diff >= diff1 && diff <= 28;
	}

	private boolean isInboundTimeAllowed(LocalTime time) {
		if(outboundDatePicker.getDate().isEqual(inboundDatePicker.getDate())) {
			return time.isAfter(outboundTimePicker.getTime());
		}
		return time.getHour() >= 5;
	}

	private LocalTime nextHalfHour(LocalTime time) {
		if(time.getHour() < 5) {
			return LocalTime.of(5, 0);
		}
		time = time.truncatedTo(ChronoUnit.HOURS).
				plusMinutes(time.getMinute() < 30 ? 30 : 60);
		if(time.getHour() < 5) {
			return LocalTime.of(23, 58);
		}
		return time;
	}

	private LocalTime nextTwoHalfHours(LocalTime time) {
		if(time.getHour() < 5) {
			return LocalTime.of(5, 30);
		}
		time = time.truncatedTo(ChronoUnit.HOURS).
				plusMinutes(time.getMinute() < 30 ? 60 : 90);
		if(time.getHour() < 5) {
			return LocalTime.of(23, 59);
		}
		return time;
	}
}
