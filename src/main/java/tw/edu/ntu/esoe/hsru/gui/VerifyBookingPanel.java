package tw.edu.ntu.esoe.hsru.gui;

import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;

import tw.edu.ntu.esoe.hsru.ticket.BookingForm;
import tw.edu.ntu.esoe.hsru.userdata.Settings;
import tw.edu.ntu.esoe.hsru.web.irs.BookingFlow;

public class VerifyBookingPanel extends JPanel {

	public static final Settings SETTINGS = HsruWindow.SETTINGS;

	private final StackedPanel stack;
	private final SelectBookingPanel select;
	private final ConfirmBookingPanel confirm;

	private final JLabel veriCodeImageLabel;
	private final JLabel veriCodeLabel;
	private final JTextField veriCodeField;
	private final JButton submitButton;
	private final JButton backButton;

	private BookingForm bookingForm;
	private BookingFlow bookingFlow;

	VerifyBookingPanel(StackedPanel stack) {
		super();
		this.stack = stack;

		setLayout(null);

		confirm = new ConfirmBookingPanel(stack);
		select = new SelectBookingPanel(stack, confirm);

		veriCodeImageLabel = new JLabel();
		veriCodeImageLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(veriCodeImageLabel);

		veriCodeLabel = new JLabel("請輸入上圖中之驗證碼");
		veriCodeLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(veriCodeLabel);

		veriCodeField = new JTextField();
		veriCodeField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					submitButton.doClick();
				}
			}
		});
		add(veriCodeField);

		submitButton = new JButton("送出");
		submitButton.setMargin(new Insets(0, 0, 0, 0));
		submitButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				submit();
			}
		});
		add(submitButton);

		backButton = new JButton("返回");
		backButton.setMargin(new Insets(0, 0, 0, 0));
		backButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				stack.closeToRoot();
			}
		});
		add(backButton);
	}

	@Override
	public void doLayout() {
		int s = getWidth()/2;
		veriCodeImageLabel.setBounds(s-65, 40, 130, 60);
		veriCodeLabel.setBounds(s-65, 100, 130, 20);
		veriCodeField.setBounds(s-40, 120, 80, 20);
		submitButton.setBounds(s+10, 150, 80, 20);
		backButton.setBounds(s-90, 150, 80, 20);
	}

	@Override
	public void addNotify() {
		super.addNotify();
		veriCodeField.requestFocusInWindow();
	}

	void setData(BookingForm bookingForm, BookingFlow bookingFlow, BufferedImage veriCodeImage) {
		this.bookingForm = bookingForm;
		this.bookingFlow = bookingFlow;
		veriCodeImageLabel.setIcon(new ImageIcon(veriCodeImage));
		veriCodeField.setText("");
	}

	private void submit() {
		if(bookingFlow != null && bookingFlow.getState() == BookingFlow.State.INQUIRE) {
			submitButton.setEnabled(false);
			backButton.setEnabled(false);
			new SwingWorker<Void, Void>() {
				@Override
				protected Void doInBackground() throws Exception {
					bookingFlow.inquire(bookingForm, veriCodeField.getText());
					return null;
				}
				@Override
				protected void done() {
					try {
						get();
						if(bookingFlow.getState() == BookingFlow.State.SELECT) {
							select.setData(bookingForm, bookingFlow);
							stack.openPane(select);
						}
						else if(bookingFlow.getState() == BookingFlow.State.CONFIRM) {
							confirm.setData(bookingForm, bookingFlow);
							stack.openPane(confirm);
						}
						else {
							HsruWindow.showErrorMessage(bookingFlow.getErrorMessage());
							stack.closeToRoot();
						}
					}
					catch(Exception e) {
						HsruWindow.showErrorMessage(e);
						stack.closeToRoot();
					}
					submitButton.setEnabled(true);
					backButton.setEnabled(true);
				}
			}.execute();
		}
		else {
			stack.closeToRoot();
			HsruWindow.showErrorMessage("Unexpected error: flow in invalid state");
		}
	}
}
