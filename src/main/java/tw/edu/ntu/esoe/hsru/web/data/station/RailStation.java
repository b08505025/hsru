package tw.edu.ntu.esoe.hsru.web.data.station;

import tw.edu.ntu.esoe.hsru.web.data.ptx.NameType;

public class RailStation {

	private String stationUID = "";
	private String stationID = "";
	private String stationCode = "";
	private NameType stationName = new NameType();
	private String stationAddress = "";
	private String operatorID = "";
	private String updateTime = "";
	private int versionID = -1;
	private PointType stationPosition = new PointType();
	private String locationCity = "";
	private String locationCityCode = "";
	private String locationTown = "";
	private String locationTownCode = "";

	public String getStationUID() {
		return stationUID;
	}

	public String getStationID() {
		return stationID;
	}

	public String getStationCode() {
		return stationCode;
	}

	public NameType getStationName() {
		return stationName;
	}

	public String getStationAddress() {
		return stationAddress;
	}

	public String getOperatorID() {
		return operatorID;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public int getVersionID() {
		return versionID;
	}

	public PointType getStationPosition() {
		return stationPosition;
	}

	public String getLocationCity() {
		return locationCity;
	}

	public String getLocationCityCode() {
		return locationCityCode;
	}

	public String getLocationTown() {
		return locationTown;
	}

	public String getLocationTownCode() {
		return locationTownCode;
	}

	@Override
	public String toString() {
		return "RailStation [stationUID=" + stationUID + ", stationID=" + stationID + ", stationCode=" + stationCode
				+ ", stationName=" + stationName + ", stationAddress=" + stationAddress + ", operatorID=" + operatorID
				+ ", updateTime=" + updateTime + ", versionID=" + versionID + ", stationPosition=" + stationPosition
				+ ", locationCity=" + locationCity + ", locationCityCode=" + locationCityCode + ", locationTown="
				+ locationTown + ", locationTownCode=" + locationTownCode + "]";
	}
}
