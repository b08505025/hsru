package tw.edu.ntu.esoe.hsru.web.data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.time.Instant;
import java.util.Optional;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

/**
 * Contains utility methods related to web data storing.
 */
public class DataUtils {

	private static File dataDir;

	static {
		dataDir = new File("web_data");
		if(dataDir.isFile()) {
			dataDir.delete();
		}
		dataDir.mkdir();
	}

	/**
	 * Gets the Instant when a file in the data folder was last modified.
	 * @param fileName The name of the file.
	 * @return The Instant when the file was last modified.
	 */
	public static Instant getLastModified(String fileName) {
		return Instant.ofEpochMilli(new File(dataDir, fileName).lastModified());
	}

	/**
	 * This method does the following: A file is specified in the data folder. If the Optional is present,
	 * then the JsonElement is stored in the file; otherwise a JsonElement is read from the file.
	 * @param fileName The name of the file.
	 * @param optionalJsonElement The Optional of the JsonElement.
	 * @return The JsonElement read.
	 * @throws IOException
	 */
	public static JsonElement getData(String fileName, Optional<JsonElement> optionalJsonElement) throws FileNotFoundException {
		if(optionalJsonElement.isPresent()) {
			JsonElement jsonElement = optionalJsonElement.get();
			writeData(fileName, jsonElement);
			return jsonElement;
		}
		else {
			return readData(fileName);
		}
	}

	/**
	 * Writes a JsonElement into a file in the data folder.
	 * @param fileName The name of the file.
	 * @param jsonElement The JsonElement.
	 * @throws IOException
	 */
	public static void writeData(String fileName, JsonElement jsonElement) throws FileNotFoundException {
		File file = new File(dataDir, fileName);
		try(PrintStream out = new PrintStream(new FileOutputStream(file))) {
			JsonUtils.GSON.toJson(jsonElement, out);
		}
	}

	/**
	 * Reads a JsonElement from a file in the data folder.
	 * @param fileName The name of the file.
	 * @return The JsonElement read.
	 * @throws IOException
	 */
	public static JsonElement readData(String fileName) throws FileNotFoundException {
		File file = new File(dataDir, fileName);
		return JsonParser.parseReader(new BufferedReader(new FileReader(file)));
	}

	/**
	 * Deletes a file in the data folder.
	 * @param fileName The name of the file.
	 * @return true if and only if the file or directory is successfully deleted; false otherwise
	 */
	public static boolean deleteData(String fileName) {
		File file = new File(dataDir, fileName);
		return file.delete();
	}
}
