package tw.edu.ntu.esoe.hsru.gui;

import javax.swing.JPanel;

import tw.edu.ntu.esoe.hsru.userdata.Settings;

/**
 * The station info panel.
 */
public class StationPanel extends JPanel {

	public static final Settings SETTINGS = HsruWindow.SETTINGS;

	StationPanel() {
		super();

		setLayout(null);
	}

	@Override
	public void doLayout() {

	}
}
