package tw.edu.ntu.esoe.hsru.ticket;

import java.util.ArrayList;
import java.util.List;

public class TripCandidate {

	private String formValue = "";
	private String trainNumber = "";
	private DiscountType studentDiscount = DiscountType.NONE;
	private DiscountType earlyBirdDiscount = DiscountType.NONE;
	private String departureTime = "";
	private String destinationTime = "";
	private String duration = "";

	public TripCandidate() {}

	public TripCandidate(String formValue, String trainNumber, DiscountType studentDiscount, DiscountType earlyBirdDiscount, String departureTime, String destinationTime, String duration) {
		this.formValue = formValue;
		this.trainNumber = (trainNumber.length() == 3 ? "0" : "") + trainNumber;
		this.studentDiscount = studentDiscount;
		this.earlyBirdDiscount = earlyBirdDiscount;
		this.departureTime = departureTime;
		this.destinationTime = destinationTime;
		this.duration = duration;
	}

	public String getFormValue() {
		return formValue;
	}

	public String getTrainNumber() {
		return trainNumber;
	}

	public DiscountType getStudentDiscount() {
		return studentDiscount;
	}

	public DiscountType getEarlyBirdDiscount() {
		return earlyBirdDiscount;
	}

	public String getDepartureTime() {
		return departureTime;
	}

	public String getDestinationTime() {
		return destinationTime;
	}

	public String getDuration() {
		return duration;
	}

	public String[] toCandidateRow() {
		List<String> ret = new ArrayList<>();
		ret.add(trainNumber);
		if(studentDiscount != DiscountType.NONE) {
			ret.add(studentDiscount.getNameZh());
		}
		ret.add(earlyBirdDiscount.getNameZh());
		ret.add(departureTime);
		ret.add(destinationTime);
		ret.add(duration);
		return ret.toArray(new String[ret.size()]);
	}
}
