package tw.edu.ntu.esoe.hsru.web.data.ptx;

public class NameType {

	private String zh_tw = "";
	private String en = "";

	public NameType() {}

	public NameType(String zh_tw, String en) {
		this.zh_tw = zh_tw;
		this.en = en;
	}

	public String zh_tw() {
		return zh_tw;
	}

	public String en() {
		return en;
	}

	@Override
	public String toString() {
		return "NameType [zh_tw=" + zh_tw + ", en=" + en + "]";
	}
}
