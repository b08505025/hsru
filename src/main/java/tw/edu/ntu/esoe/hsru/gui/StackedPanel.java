package tw.edu.ntu.esoe.hsru.gui;

import java.awt.Component;
import java.awt.GridLayout;
import java.util.LinkedList;

import javax.swing.JPanel;

/**
 * A panel that allows components to be "stacked" on each other, creating a hierarchy. Back buttons need to be
 * added to the components themselves.
 */
public class StackedPanel extends JPanel {

	private LinkedList<Component> componentStack = new LinkedList<>();

	public StackedPanel() {
		super();
		setLayout(new GridLayout(1, 1));
	}

	public void openPane(Component comp) {
		if(!componentStack.isEmpty()) {
			remove(componentStack.getLast());
		}
		componentStack.addLast(comp);
		add(comp);
		validate();
		repaint();
	}

	public void closePane() {
		if(componentStack.size() > 1) {
			remove(componentStack.pollLast());
			add(componentStack.getLast());
			validate();
			repaint();
		}
	}

	public void closeToRoot() {
		if(componentStack.size() > 1) {
			remove(componentStack.getLast());
			Component comp = componentStack.getFirst();
			componentStack.clear();
			componentStack.addLast(comp);
			add(comp);
			validate();
			repaint();
		}
	}
}
