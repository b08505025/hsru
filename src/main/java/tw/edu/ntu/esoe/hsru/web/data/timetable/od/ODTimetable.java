package tw.edu.ntu.esoe.hsru.web.data.timetable.od;

import java.util.Collections;
import java.util.List;

public class ODTimetable {

	private ODTimetableTitle title = new ODTimetableTitle();
	private List<TrainItem> trainItem = Collections.emptyList();

	public ODTimetableTitle getTitle() {
		return title;
	}

	public List<TrainItem> getTrainItem() {
		return trainItem;
	}

	@Override
	public String toString() {
		final int maxLen = 30;
		return "ODTimetable [title=" + title + ", trainItem="
				+ (trainItem != null ? trainItem.subList(0, Math.min(trainItem.size(), maxLen)) : null) + "]";
	}
}
