package tw.edu.ntu.esoe.hsru.web.data.timetable;

import tw.edu.ntu.esoe.hsru.web.data.ptx.NameType;

public class TrainInfo {

	private String trainNo = "";
	private byte direction = -1;
	private String startingStationID = "";
	private NameType startingStationName = new NameType();
	private String endingStationID = "";
	private NameType endingStationName = new NameType();
	private NameType note = new NameType();

	public String getTrainNo() {
		return trainNo;
	}

	public byte getDirection() {
		return direction;
	}

	public String getStartingStationID() {
		return startingStationID;
	}

	public NameType getStartingStationName() {
		return startingStationName;
	}

	public String getEndingStationID() {
		return endingStationID;
	}

	public NameType getEndingStationName() {
		return endingStationName;
	}

	public NameType getNote() {
		return note;
	}

	@Override
	public String toString() {
		return "TrainInfo [trainNo=" + trainNo + ", direction=" + direction + ", startingStationID="
				+ startingStationID + ", startingStationName=" + startingStationName + ", endingStationID="
				+ endingStationID + ", endingStationName=" + endingStationName + ", note=" + note + "]";
	}
}
