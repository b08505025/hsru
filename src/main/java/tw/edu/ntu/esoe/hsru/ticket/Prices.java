package tw.edu.ntu.esoe.hsru.ticket;

import tw.edu.ntu.esoe.hsru.data.Station;

public class Prices {

	public static final int[][] STANDARD_BASE = {
			{   0,   40,   70,  200,  330,  480,  750,  870,  970, 1120, 1390, 1530},
			{  40,    0,   40,  160,  290,  430,  700,  820,  930, 1080, 1350, 1490},
			{  70,   40,    0,  130,  260,  400,  670,  790,  900, 1050, 1320, 1460},
			{ 200,  160,  130,    0,  130,  280,  540,  670,  780,  920, 1190, 1330},
			{ 330,  290,  260,  130,    0,  140,  410,  540,  640,  790, 1060, 1200},
			{ 480,  430,  400,  280,  140,    0,  270,  390,  500,  640,  920, 1060},
			{ 750,  700,  670,  540,  410,  270,    0,  130,  230,  380,  650,  790},
			{ 870,  820,  790,  670,  540,  390,  130,    0,  110,  250,  530,  670},
			{ 970,  930,  900,  780,  640,  500,  230,  110,    0,  150,  420,  560},
			{1120, 1080, 1050,  920,  790,  640,  380,  250,  150,    0,  280,  410},
			{1390, 1350, 1320, 1190, 1060,  920,  650,  530,  420,  280,    0,  140},
			{1530, 1490, 1460, 1330, 1200, 1060,  790,  670,  560,  410,  140,    0},
	};

	public static final int[][] BUSINESS_BASE = {
			{   0,  260,  310,  500,  700,  920, 1330, 1510, 1660, 1880, 2290, 2500},
			{ 260,    0,  260,  440,  640,  850, 1250, 1430, 1600, 1820, 2230, 2440},
			{ 310,  260,    0,  400,  590,  800, 1210, 1390, 1550, 1780, 2180, 2390},
			{ 500,  440,  400,    0,  400,  620, 1010, 1210, 1370, 1580, 1990, 2200},
			{ 700,  640,  590,  400,    0,  410,  820, 1010, 1160, 1390, 1790, 2000},
			{ 920,  850,  800,  620,  410,    0,  610,  790,  950, 1160, 1580, 1790},
			{1330, 1250, 1210, 1010,  820,  610,    0,  400,  550,  770, 1180, 1390},
			{1510, 1430, 1390, 1210, 1010,  790,  400,    0,  370,  580, 1000, 1210},
			{1660, 1600, 1550, 1370, 1160,  950,  550,  370,    0,  430,  830, 1040},
			{1880, 1820, 1780, 1580, 1390, 1160,  770,  580,  430,    0,  620,  820},
			{2290, 2230, 2180, 1990, 1790, 1580, 1180, 1000,  830,  620,    0,  410},
			{2500, 2440, 2390, 2200, 2000, 1790, 1390, 1210, 1040,  820,  410,    0},
	};

	public static int getAdult(Station fromStation, Station toStation, CarType carType) {
		switch(carType) {
		case STANDARD:
			return get(STANDARD_BASE, fromStation, toStation, 1);
		case BUSINESS:
			return get(BUSINESS_BASE, fromStation, toStation, 1);
		}
		return 0;
	}

	public static int getConcession(Station fromStation, Station toStation, CarType carType) {
		switch(carType) {
		case STANDARD:
			return get(STANDARD_BASE, fromStation, toStation, 0.5);
		case BUSINESS:
			return get(BUSINESS_BASE, fromStation, toStation, 0.5);
		}
		return 0;
	}

	public static int getDiscount(Station fromStation, Station toStation, DiscountType discountType) {
		return get(STANDARD_BASE, fromStation, toStation, discountType.getMultiplier());
	}

	public static int get(int[][] table, Station fromStation, Station toStation, double multiplier) {
		int basePrice = table[fromStation.ordinal()][toStation.ordinal()];
		return (int)Math.round(Math.floor((basePrice/5)*multiplier))*5;
	}
}