package tw.edu.ntu.esoe.hsru.web.data.timetable.general;

import java.io.IOException;
import java.lang.reflect.Type;
import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

import tw.edu.ntu.esoe.hsru.web.data.DataUtils;
import tw.edu.ntu.esoe.hsru.web.data.JsonUtils;
import tw.edu.ntu.esoe.hsru.web.data.ptx.PTXRequest;

public class GeneralTimetableRequest {

	public static final Type GENERAL_TIMETABLE_ENTRY_LIST_TYPE = new TypeToken<List<GeneralTimetableEntry>>(){}.getType();
	/**
	 * 10 days in milliseconds.
	 */
	public static final long UPDATE_INTERVAL = 10*24*60*60*1000;

	/**
	 * Requests the general timetable.
	 * @return The list containing entries of the general timetable.
	 * @throws IOException
	 */
	public static List<GeneralTimetableEntry> getTimetableEntries() throws IOException {
		String fileName = "generalTimetable.json";
		Instant lastModified = DataUtils.getLastModified(fileName);
		Optional<JsonElement> optional = Optional.empty();
		if(System.currentTimeMillis()-lastModified.toEpochMilli() > UPDATE_INTERVAL) {
			optional = PTXRequest.getJsonElement("v2/Rail/THSR/GeneralTimetable", Collections.emptyMap(), lastModified);
		}
		JsonElement jsonElement = DataUtils.getData(fileName, optional);
		return JsonUtils.GSON.fromJson(jsonElement, GENERAL_TIMETABLE_ENTRY_LIST_TYPE);
	}
}
