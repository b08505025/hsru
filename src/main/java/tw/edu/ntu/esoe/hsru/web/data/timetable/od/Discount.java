package tw.edu.ntu.esoe.hsru.web.data.timetable.od;

public class Discount {

	private String id = "";
	private String name = "";
	private String value = "";
	private String color = "";
	private String discount = "";

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getValue() {
		return value;
	}

	public String getColor() {
		return color;
	}

	public String getDiscount() {
		return discount;
	}

	@Override
	public String toString() {
		return "Discount [id=" + id + ", name=" + name + ", value=" + value + ", color=" + color + ", discount="
				+ discount + "]";
	}
}
