package tw.edu.ntu.esoe.hsru.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import com.github.lgooddatepicker.components.DatePicker;

import tw.edu.ntu.esoe.hsru.data.Station;
import tw.edu.ntu.esoe.hsru.userdata.Settings;
import tw.edu.ntu.esoe.hsru.web.data.timetable.StopTime;
import tw.edu.ntu.esoe.hsru.web.data.timetable.TrainInfo;
import tw.edu.ntu.esoe.hsru.web.data.timetable.daily.DailyTimetableEntry;
import tw.edu.ntu.esoe.hsru.web.data.timetable.daily.DailyTimetableRequest;

/**
 * The daily timetable panel.
 */
public class DailyTimetablePanel extends JPanel {

	public static final Settings SETTINGS = HsruWindow.SETTINGS;

	public static final DateTimeFormatter DATE = DateTimeFormatter.ofPattern("yyyy/MM/dd");

	public static final Color GAINSBORO = new Color(0xDDDDDD);

	public static String[] southboundColumns = {
			"車次", "南港", "台北", "板橋", "桃園", "新竹", "苗栗", "台中", "彰化", "雲林", "嘉義", "台南", "左營"
	};
	public static String[] northboundColumns = {
			"車次", "左營", "台南", "嘉義", "雲林", "彰化", "台中", "苗栗", "新竹", "桃園", "板橋", "台北", "南港"
	};

	private final DatePicker datePicker;
	private final JLabel dateLabel;
	private final JButton searchButton;
	private final DefaultTableModel southboundTableModel;
	private final JTable southboundTable;
	private final JScrollPane southboundScrollPane;
	private final DefaultTableModel northboundTableModel;
	private final JTable northboundTable;
	private final JScrollPane northboundScrollPane;
	private final JTabbedPane tabs;

	DailyTimetablePanel() {
		super();

		setLayout(null);

		datePicker = new DatePicker();
		datePicker.getSettings().setSizeTextFieldMinimumWidth(0);
		datePicker.getSettings().setSizeTextFieldMinimumWidthDefaultOverride(false);
		datePicker.getSettings().setFormatForDatesCommonEra(DATE);
		datePicker.getSettings().setFormatForTodayButton(DATE);
		datePicker.getSettings().setVetoPolicy(this::isDateAllowed);
		datePicker.setDateToToday();
		datePicker.getComponentDateTextField().addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					searchButton.doClick();
				}
			}
		});
		add(datePicker);

		dateLabel = new JLabel("日期");
		add(dateLabel);

		searchButton = new JButton("查詢");
		searchButton.setMargin(new Insets(0, 0, 0, 0));
		searchButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				requestTable();
			}
		});
		add(searchButton);

		southboundTableModel = new ReadOnlyTableModel();
		southboundTableModel.setDataVector(new Object[][]{}, southboundColumns);

		northboundTableModel = new ReadOnlyTableModel();
		northboundTableModel.setDataVector(new Object[][]{}, northboundColumns);

		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer.UIResource() {
			@Override
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
				setBackground(column == 0 ? GAINSBORO : Color.WHITE);
				return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			}
		};
		centerRenderer.setHorizontalAlignment(SwingConstants.CENTER);

		southboundTable = new JTable(southboundTableModel);
		southboundTable.setDefaultRenderer(Object.class, centerRenderer);
		southboundTable.getTableHeader().setReorderingAllowed(false);
		southboundTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		northboundTable = new JTable(northboundTableModel);
		northboundTable.setDefaultRenderer(Object.class, centerRenderer);
		northboundTable.getTableHeader().setReorderingAllowed(false);
		northboundTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		southboundScrollPane = new JScrollPane(southboundTable);
		northboundScrollPane = new JScrollPane(northboundTable);

		tabs = new JTabbedPane();
		tabs.addTab("南下", southboundScrollPane);
		tabs.addTab("北上", northboundScrollPane);
		add(tabs);
	}

	@Override
	public void doLayout() {
		int s = getWidth()/2-100;
		datePicker.setBounds(s, 20, 105, 20);
		dateLabel.setBounds(s, 0, 105, 20);
		searchButton.setBounds(s+125, 20, 75, 20);
		tabs.setBounds(0, 40, getWidth(), getHeight()-40);
	}

	private void requestTable() {
		searchButton.setEnabled(false);
		new SwingWorker<List<DailyTimetableEntry>, Void>() {
			@Override
			protected List<DailyTimetableEntry> doInBackground() throws Exception {
				return DailyTimetableRequest.getTimetableEntries(datePicker.getDate());
			}
			@Override
			protected void done() {
				try {
					updateTable(get());
				}
				catch(Exception e) {
					HsruWindow.showErrorMessage(e);
				}
				searchButton.setEnabled(true);
			}
		}.execute();
	}

	private void updateTable(List<DailyTimetableEntry> timetable) {
		String[][] data;

		data = timetable.stream().
				filter(entry->entry.getDailyTrainInfo().getDirection() == 0).
				sorted().
				map(entry->getRow(entry, Station.SOUTHBOUND)).
				toArray(String[][]::new);
		southboundTableModel.setDataVector(data, southboundColumns);

		data = timetable.stream().
				filter(entry->entry.getDailyTrainInfo().getDirection() == 1).
				sorted().
				map(entry->getRow(entry, Station.NORTHBOUND)).
				toArray(String[][]::new);
		northboundTableModel.setDataVector(data, northboundColumns);
	}

	private String[] getRow(DailyTimetableEntry timetableEntry, Station[] stations) {
		String[] arr = new String[13];
		Arrays.fill(arr, "");
		TrainInfo trainInfo = timetableEntry.getDailyTrainInfo();
		arr[0] = trainInfo.getTrainNo();
		List<StopTime> stopTimes = timetableEntry.getStopTimes();
		int i = 0;
		boolean started = false;
		for(StopTime stopTime : stopTimes) {
			while(i < 12) {
				Station station = stations[i];
				if(station.getStationID().equals(stopTime.getStationID())) {
					arr[i+1] = stopTime.getDepartureTime();
					started = true;
					++i;
					break;
				}
				else if(started) {
					arr[i+1] = "─";
				}
				++i;
			}
		}
		return arr;
	}

	private boolean isDateAllowed(LocalDate date) {
		return date.toEpochDay()-LocalDate.now().toEpochDay() <= 28;
	}
}
