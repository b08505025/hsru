package tw.edu.ntu.esoe.hsru.web.data.ptx;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.security.SignatureException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.Base64;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.zip.GZIPInputStream;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.jsoup.Connection;
import org.jsoup.Jsoup;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

public class PTXRequest {

	private static final DateTimeFormatter RFC_1123_DATE_TIME = DateTimeFormatter.ofPattern("EEE, dd MMM yyyy HH:mm:ss z", Locale.US).withZone(ZoneId.of("GMT"));

	/**
	 * Makes a request to PTX.
	 * @param requestPath The path to the requested API method.
	 * @param lastModifiedTime The time used to set the If-Modified-Since header. Can be null.
	 * @return The response in JSON if the response is present.
	 * @throws IOException
	 */
	public static Optional<JsonElement> getJsonElement(String requestPath, Map<String, String> data, TemporalAccessor lastModifiedTime) throws IOException {
		String apiUrl = "https://ptx.transportdata.tw/MOTC/"+requestPath;

		String appID = "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF";
		String appKey = "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF";

		String xDate = getRFC1123GMTTime(Instant.now());
		String signDate = "x-date: "+xDate;

		String signature;
		try {
			signature = getSHA1Signature(signDate, appKey);
		}
		catch(SignatureException e) {
			throw new IOException(e);
		}
		String sAuth = "hmac username=\""+appID+"\", algorithm=\"hmac-sha1\", headers=\"x-date\", signature=\""+signature+"\"";

		System.out.println(apiUrl);

		Connection.Response response = Jsoup.connect(apiUrl).
				method(Connection.Method.GET).ignoreContentType(true).
				followRedirects(true).
				header("Authorization", sAuth).header("x-date", xDate).
				header("Accept-Encoding", "gzip").header("If-Modified-Since", getRFC1123GMTTime(lastModifiedTime)).
				data(data).data("$format", "JSON").
				execute();

		System.out.println(response.statusCode()+" "+response.statusMessage());
		System.out.println(response.headers());

		if(response.statusCode() == 304) {
			return Optional.empty();
		}
		else if(response.statusCode() != 200) {
			throw new IOException("Connection responded "+response.statusCode()+" "+response.statusMessage());
		}

		BufferedReader in;
		if("gzip".equals(response.contentType())) {
			InputStreamReader reader = new InputStreamReader(new GZIPInputStream(response.bodyStream()), StandardCharsets.UTF_8);
			in = new BufferedReader(reader);
		}
		else {
			InputStreamReader reader = new InputStreamReader(response.bodyStream(), StandardCharsets.UTF_8);
			in = new BufferedReader(reader);
		}
		return Optional.of(JsonParser.parseReader(in));
	}

	public static String getRFC1123GMTTime(TemporalAccessor time) {
		if(time == null) {
			time = Instant.EPOCH;
		}
		return RFC_1123_DATE_TIME.format(time);
	}

	public static String getSHA1Signature(String xData, String appKey) throws SignatureException {
		try {
			Base64.Encoder encoder = Base64.getEncoder();
			//get an hmac_sha1 key from the raw key bytes
			SecretKeySpec signingKey = new SecretKeySpec(appKey.getBytes("UTF-8"),"HmacSHA1");
			// get an hmac_sha1 mac instance and initialize with the signing key
			Mac mac = Mac.getInstance("HmacSHA1");
			mac.init(signingKey);
			// compute the hmac on input data bytes
			byte[] rawHmac = mac.doFinal(xData.getBytes("UTF-8"));
			String result = encoder.encodeToString(rawHmac);
			return result;
		}
		catch(Exception e) {
			throw new SignatureException("Failed to generate HMAC", e);
		}
	}
}
