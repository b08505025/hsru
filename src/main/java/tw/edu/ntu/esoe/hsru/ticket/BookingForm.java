package tw.edu.ntu.esoe.hsru.ticket;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import tw.edu.ntu.esoe.hsru.data.Station;

public class BookingForm {

	public final Station fromStation;
	public final Station toStation;
	public CarType carType = CarType.STANDARD;
	public Seat.Alignment seatPreference = null;
	public boolean specTrainNo = false;
	public LocalDate outboundDate = null;
	public LocalTime outboundTime = null;
	public String outboundTrainNo = null;
	public boolean roundTrip = false;
	public LocalDate inboundDate = null;
	public LocalTime inboundTime = null;
	public String inboundTrainNo = null;
	public int adultCount = 1;
	public int childCount = 0;
	public int disabledCount = 0;
	public int seniorCount = 0;
	public int studentCount = 0;
	public boolean earlyBirdOnly = false;

	public BookingForm(Station fromStation, Station toStation) {
		this.fromStation = fromStation;
		this.toStation = toStation;
	}

	public BookingForm setCarType(CarType carType) {
		this.carType = carType;
		return this;
	}

	public BookingForm setSeatPreference(Seat.Alignment seatPreference) {
		this.seatPreference = seatPreference;
		return this;
	}

	public BookingForm setSpecTrainNo(boolean specTrainNo) {
		this.specTrainNo = specTrainNo;
		return this;
	}

	public BookingForm setOutboundDate(LocalDate outboundDate) {
		this.outboundDate = outboundDate;
		return this;
	}

	public BookingForm setOutboundTime(LocalTime outboundTime) {
		this.outboundTime = outboundTime;
		return this;
	}

	public BookingForm setOutboundTrainNo(String outboundTrainNo) {
		this.outboundTrainNo = outboundTrainNo;
		return this;
	}

	public BookingForm setRoundTrip(boolean roundTrip) {
		this.roundTrip = roundTrip;
		return this;
	}

	public BookingForm setInboundDate(LocalDate inboundDate) {
		this.inboundDate = inboundDate;
		return this;
	}

	public BookingForm setInboundTime(LocalTime inboundTime) {
		this.inboundTime = inboundTime;
		return this;
	}

	public BookingForm setInboundTrainNo(String inboundTrainNo) {
		this.inboundTrainNo = inboundTrainNo;
		return this;
	}

	public BookingForm setAdultCount(int adultCount) {
		this.adultCount = adultCount;
		return this;
	}

	public BookingForm setChildCount(int childCount) {
		this.childCount = childCount;
		return this;
	}

	public BookingForm setDisabledCount(int disabledCount) {
		this.disabledCount = disabledCount;
		return this;
	}

	public BookingForm setSeniorCount(int seniorCount) {
		this.seniorCount = seniorCount;
		return this;
	}

	public BookingForm setStudentCount(int studentCount) {
		this.studentCount = studentCount;
		return this;
	}

	public BookingForm setEarlyBirdOnly(boolean earlyBirdOnly) {
		this.earlyBirdOnly = earlyBirdOnly;
		return this;
	}

	public Station getFromStation() {
		return fromStation;
	}

	public Station getToStation() {
		return toStation;
	}

	public CarType getCarType() {
		return carType;
	}

	public Seat.Alignment getSeatPreference() {
		return seatPreference;
	}

	public boolean isSpecTrainNo() {
		return specTrainNo;
	}

	public LocalDate getOutboundDate() {
		return outboundDate;
	}

	public LocalTime getOutboundTime() {
		return outboundTime;
	}

	public String getOutboundTrainNo() {
		return outboundTrainNo;
	}

	public boolean isRoundTrip() {
		return roundTrip;
	}

	public LocalDate getInboundDate() {
		return inboundDate;
	}

	public LocalTime getInboundTime() {
		return inboundTime;
	}

	public String getInboundTrainNo() {
		return inboundTrainNo;
	}

	public int getAdultCount() {
		return adultCount;
	}

	public int getChildCount() {
		return childCount;
	}

	public int getDisabledCount() {
		return disabledCount;
	}

	public int getSeniorCount() {
		return seniorCount;
	}

	public int getConcessionCount() {
		return childCount+disabledCount+seniorCount;
	}

	public int getStudentCount() {
		return studentCount;
	}

	public int getTotalCount() {
		return adultCount+childCount+disabledCount+seniorCount+studentCount;
	}

	public boolean isEarlyBirdOnly() {
		return earlyBirdOnly;
	}

	public static final DateTimeFormatter FLOW_DATE = DateTimeFormatter.ofPattern("yyyy/MM/dd");
	public static final DateTimeFormatter FLOW_TIME = DateTimeFormatter.ofPattern("hmm");

	public Map<String, String> getFlowData(String veriCode) {
		Map<String, String> data = new LinkedHashMap<>();
		data.put("BookingS1Form:hf:0", "");
		data.put("selectStartStation", String.valueOf(fromStation.ordinal()+1));
		data.put("selectDestinationStation", String.valueOf(toStation.ordinal()+1));
		data.put("trainCon:trainRadioGroup", String.valueOf(carType.ordinal()));
		data.put("seatCon:seatRadioGroup", getFlowSeatPreference());
		data.put("bookingMethod", specTrainNo ? "1" : "0");
		data.put("toTimeInputField", FLOW_DATE.format(outboundDate));
		data.put("toTimeTable", formatFlowTime(outboundTime));
		data.put("toTrainIDInputField", outboundTrainNo);
		data.put("showbackCheckBox:backTimeCheckBox", roundTrip ? "on" : "");
		data.put("backTimeInputField", FLOW_DATE.format(inboundDate));
		data.put("backTimeTable", formatFlowTime(inboundTime));
		data.put("backTrainIDInputField", inboundTrainNo);
		data.put("ticketPanel:rows:0:ticketAmount", adultCount+"F");
		data.put("ticketPanel:rows:1:ticketAmount", childCount+"H");
		data.put("ticketPanel:rows:2:ticketAmount", disabledCount+"W");
		data.put("ticketPanel:rows:3:ticketAmount", seniorCount+"E");
		data.put("ticketPanel:rows:4:ticketAmount", studentCount+"P");
		data.put("offPeakTrainSearchContainer:onlyQueryOffPeak", earlyBirdOnly ? "on" : "");
		data.put("homeCaptcha:securityCode", veriCode);
		return data;
	}

	public static String formatFlowTime(LocalTime time) {
		return new StringBuilder().
				append(FLOW_TIME.format(time)).
				append(time.getHour() == 12 && time.getMinute() == 0 ? 'N' :
					time.getHour() < 12 ? 'A' : 'P').
				toString();
	}

	public String getFlowSeatPreference() {
		switch(seatPreference) {
		default: return "radio17";
		case WINDOW: return "radio19";
		case AISLE: return "radio21";
		}
	}

	public static final DateTimeFormatter DESC_DATE = DateTimeFormatter.ofPattern("MM/dd (EEEE)");

	public String getOutboundDesc() {
		return "去程: "+fromStation.getNameZh()+" - "+toStation.getNameZh()+" "+
				DESC_DATE.withLocale(Locale.TAIWAN).format(outboundDate);
	}

	public String getInboundDesc() {
		return "回程: "+fromStation.getNameZh()+" - "+toStation.getNameZh()+" "+
				DESC_DATE.withLocale(Locale.TAIWAN).format(inboundDate);
	}

	public String getTicketDesc() {
		StringBuilder sb = new StringBuilder();
		sb.append("車廂: ");
		switch(carType) {
		case STANDARD:
			sb.append("標準車廂");
			break;
		case BUSINESS:
			sb.append("商務車廂");
			break;
		}
		sb.append("　　");
		sb.append("票數: ");
		int m = roundTrip ? 2 : 1;
		List<String> tickets = new ArrayList<>();
		if(adultCount > 0) {
			tickets.add("全票 "+adultCount*m+" 張");
		}
		if(childCount > 0) {
			tickets.add("孩童票 "+childCount*m+" 張");
		}
		if(disabledCount > 0) {
			tickets.add("愛心票 "+disabledCount*m+" 張");
		}
		if(seniorCount > 0) {
			tickets.add("敬老票 "+seniorCount*m+" 張");
		}
		if(studentCount > 0) {
			tickets.add("大學生優惠票 "+studentCount*m+" 張");
		}
		sb.append(String.join(" | ", tickets));
		return sb.toString();
	}
}
