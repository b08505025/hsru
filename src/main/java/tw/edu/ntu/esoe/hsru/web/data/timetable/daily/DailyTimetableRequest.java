package tw.edu.ntu.esoe.hsru.web.data.timetable.daily;

import java.io.IOException;
import java.lang.reflect.Type;
import java.time.Instant;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

import tw.edu.ntu.esoe.hsru.web.data.DataUtils;
import tw.edu.ntu.esoe.hsru.web.data.JsonUtils;
import tw.edu.ntu.esoe.hsru.web.data.ptx.PTXRequest;

public class DailyTimetableRequest {

	public static final Type DAILY_TIMETABLE_ENTRY_LIST_TYPE = new TypeToken<List<DailyTimetableEntry>>(){}.getType();

	/**
	 * Requests the timetable of the specified date.
	 * @param date The date of the requested timetable, max 28 days into the future.
	 * @return The list containing entries of the requested timetable.
	 * @throws IOException
	 */
	public static List<DailyTimetableEntry> getTimetableEntries(LocalDate date) throws IOException {
		if(date.toEpochDay()-LocalDate.now().toEpochDay() > 28) {
			throw new IOException("Date out of range: "+DateTimeFormatter.ISO_LOCAL_DATE.format(date));
		}
		String fileName = "dailyTimetable-"+DateTimeFormatter.ISO_LOCAL_DATE.format(date)+".json";
		String apiPath = "v2/Rail/THSR/DailyTimetable/TrainDate/"+DateTimeFormatter.ISO_LOCAL_DATE.format(date);
		Instant lastModified = DataUtils.getLastModified(fileName);
		Optional<JsonElement> optional = Optional.empty();
		if(lastModified.toEpochMilli() == 0) {
			optional = PTXRequest.getJsonElement(apiPath, Collections.emptyMap(), null);
		}
		JsonElement jsonElement = DataUtils.getData(fileName, optional);
		return JsonUtils.GSON.fromJson(jsonElement, DAILY_TIMETABLE_ENTRY_LIST_TYPE);
	}
}
