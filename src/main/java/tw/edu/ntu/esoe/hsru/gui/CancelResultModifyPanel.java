package tw.edu.ntu.esoe.hsru.gui;

import java.awt.Color;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.Border;

import tw.edu.ntu.esoe.hsru.userdata.Settings;

public class CancelResultModifyPanel extends JPanel {

	public static final Settings SETTINGS = HsruWindow.SETTINGS;

	private final StackedPanel stack;

	private final JLabel messageLabel;
	private final JButton backButton;

	CancelResultModifyPanel(StackedPanel stack) {
		super();
		this.stack = stack;

		setLayout(null);

		Border border = BorderFactory.createEtchedBorder();

		messageLabel = new JLabel("取消訂位成功!");
		messageLabel.setHorizontalAlignment(SwingConstants.CENTER);
		messageLabel.setBorder(border);
		messageLabel.setForeground(Color.RED);
		add(messageLabel);

		backButton = new JButton("回到首頁");
		backButton.setMargin(new Insets(0, 0, 0, 0));
		backButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				stack.closeToRoot();
			}
		});
		add(backButton);
	}

	@Override
	public void doLayout() {
		int s = getWidth()/2;
		messageLabel.setBounds(s-150, 20, 300, 20);
		backButton.setBounds(s-40, 80, 80, 20);
	}
}
