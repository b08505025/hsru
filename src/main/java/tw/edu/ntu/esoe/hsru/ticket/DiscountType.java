package tw.edu.ntu.esoe.hsru.ticket;

import java.util.function.Predicate;

public enum DiscountType {

	NONE("", "", 1, s->false),
	EARLY_BIRD_65("早鳥65折", "Early Bird 35% Off", 0.65, s->s.contains("irs_ind_ebX")),
	EARLY_BIRD_8("早鳥8折", "Early Bird 20% Off", 0.8, s->s.contains("irs_ind_ebH")),
	EARLY_BIRD_9("早鳥9折", "Early Bird 10% Off", 0.9, s->s.contains("irs_ind_eb2")),
	COLLEGE_STUDENT_5("5折", "50% Off", 0.5, s->s.contains("50off")),
	COLLEGE_STUDENT_75("75折", "25% Off", 0.75, s->s.contains("25off")),
	COLLEGE_STUDENT_88("88折", "12% Off", 0.88, s->s.contains("12off")),
	;

	public final String nameZh;
	public final String nameEn;
	public final double multiplier;
	public final Predicate<String> fileNameMatcher;

	DiscountType(String nameZh, String nameEn, double multiplier, Predicate<String> fileNameMatcher) {
		this.nameZh = nameZh;
		this.nameEn = nameEn;
		this.multiplier = multiplier;
		this.fileNameMatcher = fileNameMatcher;
	}

	public String getNameZh() {
		return nameZh;
	}

	public String getNameEn() {
		return nameEn;
	}

	public double getMultiplier() {
		return multiplier;
	}

	public Predicate<String> getFileNameMatcher() {
		return fileNameMatcher;
	}

	public boolean isEarlyBird() {
		return this == EARLY_BIRD_65 || this == EARLY_BIRD_8 || this == EARLY_BIRD_9;
	}

	public boolean isCollegeStudent() {
		return this == COLLEGE_STUDENT_5 || this == COLLEGE_STUDENT_75 || this == COLLEGE_STUDENT_88;
	}

	public static DiscountType fromFileName(String fileName) {
		if(fileName != null && !fileName.isEmpty()) {
			for(DiscountType type : values()) {
				if(type.fileNameMatcher.test(fileName)) {
					return type;
				}
			}
		}
		return NONE;
	}
}
