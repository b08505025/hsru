package tw.edu.ntu.esoe.hsru.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import tw.edu.ntu.esoe.hsru.ticket.Booking;
import tw.edu.ntu.esoe.hsru.ticket.BookingManager;
import tw.edu.ntu.esoe.hsru.ticket.Trip;
import tw.edu.ntu.esoe.hsru.userdata.Settings;

public class CancelModifyPanel extends JPanel {

	public static final Settings SETTINGS = HsruWindow.SETTINGS;

	public static final Color GAINSBORO = new Color(0xDDDDDD);

	public static String[] detailsColumns = {
			"行程", "日期", "車次", "起程站", "到達站", "出發時間", "到達時間", "小計", "座位"
	};

	private final StackedPanel stack;
	private final DetailsModifyPanel details;
	private final CancelResultModifyPanel result;

	private final JLabel messageLabel;
	private final JTextField idField;
	private final JLabel idDescLabel;
	private final DefaultTableModel detailsTableModel;
	private final JTable detailsTable;
	private final JScrollPane detailsScrollPane;
	private final JLabel detailsLabel;
	private final JLabel detailsPriceLabel;
	private final JPanel detailsDescPane;
	private final JPanel detailsPane;
	private final JCheckBox confirmCheckBox;
	private final JButton confirmButton;
	private final JButton backButton;

	private Booking booking;
	private boolean roundTrip;
	private boolean processing;

	CancelModifyPanel(StackedPanel stack, DetailsModifyPanel details) {
		super();
		this.stack = stack;
		this.details = details;

		setLayout(null);

		result = new CancelResultModifyPanel(stack);

		Border border = BorderFactory.createEtchedBorder();

		messageLabel = new JLabel("您確定要取消本筆訂位嗎?");
		messageLabel.setHorizontalAlignment(SwingConstants.CENTER);
		messageLabel.setBorder(border);
		messageLabel.setForeground(Color.RED);
		add(messageLabel);

		idField = new JTextField();
		idField.setEditable(false);
		idField.setHorizontalAlignment(SwingConstants.CENTER);
		idField.setBorder(border);
		idField.setForeground(Color.RED);
		add(idField);

		idDescLabel = new JLabel("訂位代號");
		idDescLabel.setHorizontalAlignment(SwingConstants.CENTER);
		idDescLabel.setBorder(border);
		add(idDescLabel);DefaultTableCellRenderer detailsTableRenderer = new DefaultTableCellRenderer.UIResource()  {
			@Override
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
				setBackground(column == 0 ? GAINSBORO : Color.WHITE);
				return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			}
		};
		detailsTableRenderer.setHorizontalAlignment(SwingConstants.CENTER);

		detailsTableModel = new ReadOnlyTableModel();

		detailsTable = new JTable(detailsTableModel);
		detailsTable.setDefaultRenderer(Object.class, detailsTableRenderer);
		detailsTable.getTableHeader().setReorderingAllowed(false);
		detailsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		detailsScrollPane = new JScrollPane(detailsTable);
		detailsScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		detailsScrollPane.setBorder(BorderFactory.createEmptyBorder());
		add(detailsScrollPane);

		detailsLabel = new JLabel();

		detailsPriceLabel = new JLabel();
		detailsPriceLabel.setHorizontalAlignment(SwingConstants.TRAILING);

		detailsDescPane = new JPanel(new GridLayout(1, 2));
		detailsDescPane.add(detailsLabel);
		detailsDescPane.add(detailsPriceLabel);

		detailsPane = new JPanel(new BorderLayout());
		detailsPane.add(detailsScrollPane, BorderLayout.CENTER);
		detailsPane.add(detailsDescPane, BorderLayout.SOUTH);
		detailsPane.setBorder(BorderFactory.createTitledBorder(
				BorderFactory.createEtchedBorder(),
				"訂位明細", TitledBorder.LEADING, TitledBorder.TOP));
		add(detailsPane);

		confirmCheckBox = new JCheckBox();
		confirmCheckBox.setHorizontalAlignment(SwingConstants.CENTER);
		confirmCheckBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				confirmButton.setEnabled(canConfirm());
			}
		});
		add(confirmCheckBox);

		confirmButton = new JButton("下一步");
		confirmButton.setMargin(new Insets(0, 0, 0, 0));
		confirmButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				confirm();
			}
		});
		confirmButton.setEnabled(false);
		add(confirmButton);

		backButton = new JButton("回上一頁");
		backButton.setMargin(new Insets(0, 0, 0, 0));
		backButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				stack.closePane();
			}
		});
		add(backButton);
	}

	@Override
	public void doLayout() {
		int s = getWidth()/2-150;
		messageLabel.setBounds(s, 20, 300, 20);
		idField.setBounds(s+100, 60, 200, 20);
		idDescLabel.setBounds(s, 60, 100, 20);
		detailsPane.setBounds(0, 90, getWidth(), roundTrip ? 100 : 80);
		int t = roundTrip ? 200 : 180;
		confirmCheckBox.setBounds(0, t, getWidth(), 20);
		confirmButton.setBounds(s+220, t+30, 80, 20);
		backButton.setBounds(s, t+30, 80, 20);
	}

	void setData(Booking booking) {
		processing = true;
		this.booking = booking;

		String id = String.format("%08d", booking.getId());
		idField.setText(id);

		Trip outbound = booking.getOutbound();
		Trip inbound = booking.getInbound();

		List<String[]> data = new ArrayList<>();
		data.add(outbound.toDetailsRow());
		if(roundTrip) {
			data.add(inbound.toDetailsRow());
		}

		detailsTableModel.setDataVector(data.toArray(new String[data.size()][]), detailsColumns);

		detailsLabel.setText(booking.getTicketDesc());
		detailsPriceLabel.setText(booking.getTotalPriceDesc());

		confirmCheckBox.setText("我確定要取消本筆訂位記錄 (訂位代號 "+id+")");

		confirmCheckBox.setSelected(false);
		confirmButton.setEnabled(false);

		processing = false;
	}

	private void confirm() {
		processing = true;
		confirmButton.setEnabled(false);
		backButton.setEnabled(false);

		new SwingWorker<Void, Void>() {
			@Override
			protected Void doInBackground() throws Exception {
				BookingManager.INSTANCE.removeBooking(booking);
				return null;
			}
			@Override
			protected void done() {
				try {
					get();
					stack.openPane(result);
				}
				catch(Exception e) {
					HsruWindow.showErrorMessage(e);
					stack.closeToRoot();
				}
				processing = false;
				confirmButton.setEnabled(canConfirm());
				backButton.setEnabled(true);
			}
		}.execute();
	}

	private boolean canConfirm() {
		return !processing && confirmCheckBox.isSelected() && booking.getState().cancellable;
	}
}
