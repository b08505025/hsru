package tw.edu.ntu.esoe.hsru.web.data.timetable;

import tw.edu.ntu.esoe.hsru.web.data.ptx.NameType;

public class StopTime implements Comparable<StopTime> {

	private int stopSequence = 0;
	private String stationID = "";
	private NameType stationName = new NameType();
	private String arrivalTime = "";
	private String departureTime = "";

	public int getStopSequence() {
		return stopSequence;
	}

	public String getStationID() {
		return stationID;
	}

	public NameType getStationName() {
		return stationName;
	}

	public String getArrivalTime() {
		return arrivalTime.isEmpty() ? departureTime : arrivalTime;
	}

	public String getDepartureTime() {
		return departureTime.isEmpty() ? arrivalTime : departureTime;
	}

	@Override
	public int compareTo(StopTime o) {
		return departureTime.compareTo(o.departureTime);
	}

	@Override
	public String toString() {
		return "StopTime [stopSequence=" + stopSequence + ", stationID=" + stationID + ", stationName="
				+ stationName + ", arrivalTime=" + arrivalTime + ", departureTime=" + departureTime + "]";
	}
}
