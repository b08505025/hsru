package tw.edu.ntu.esoe.hsru.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import tw.edu.ntu.esoe.hsru.ticket.Booking;
import tw.edu.ntu.esoe.hsru.ticket.Trip;
import tw.edu.ntu.esoe.hsru.userdata.Settings;

public class DetailsModifyPanel extends JPanel {

	public static final Settings SETTINGS = HsruWindow.SETTINGS;

	public static final Color GAINSBORO = new Color(0xDDDDDD);

	public static String[] detailsColumns = {
			"行程", "日期", "車次", "起程站", "到達站", "出發時間", "到達時間", "小計", "座位"
	};

	private final StackedPanel stack;
	private final ReduceModifyPanel reduce;
	private final RemoveModifyPanel remove;
	private final CancelModifyPanel cancel;

	private final JLabel messageLabel;
	private final JTextField idField;
	private final JLabel idDescLabel;
	private final JLabel stateLabel;
	private final JLabel stateDescLabel;
	private final JLabel userIdLabel;
	private final JLabel userIdDescLabel;
	private final JLabel phoneLabel;
	private final JLabel phoneDescLabel;
	private final JLabel emailLabel;
	private final JLabel emailDescLabel;
	private final DefaultTableModel detailsTableModel;
	private final JTable detailsTable;
	private final JScrollPane detailsScrollPane;
	private final JLabel detailsLabel;
	private final JLabel detailsPriceLabel;
	private final JPanel detailsDescPane;
	private final JPanel detailsPane;
	private final JButton reduceButton;
	private final JButton removeButton;
	private final JButton cancelButton;
	private final JButton backButton;

	private Booking booking;
	private boolean roundTrip;

	DetailsModifyPanel(StackedPanel stack) {
		super();
		this.stack = stack;

		setLayout(null);

		reduce = new ReduceModifyPanel(stack, this);
		remove = new RemoveModifyPanel(stack, this);
		cancel = new CancelModifyPanel(stack, this);

		Border border = BorderFactory.createEtchedBorder();

		messageLabel = new JLabel();
		messageLabel.setHorizontalAlignment(SwingConstants.CENTER);
		messageLabel.setBorder(border);
		messageLabel.setForeground(Color.RED);
		add(messageLabel);

		idField = new JTextField();
		idField.setEditable(false);
		idField.setHorizontalAlignment(SwingConstants.CENTER);
		idField.setBorder(border);
		idField.setForeground(Color.RED);
		add(idField);

		idDescLabel = new JLabel("訂位代號");
		idDescLabel.setHorizontalAlignment(SwingConstants.CENTER);
		idDescLabel.setBorder(border);
		add(idDescLabel);

		stateLabel = new JLabel();
		stateLabel.setHorizontalAlignment(SwingConstants.CENTER);
		stateLabel.setForeground(Color.RED);
		stateLabel.setBorder(border);
		add(stateLabel);

		stateDescLabel = new JLabel("交易狀態");
		stateDescLabel.setHorizontalAlignment(SwingConstants.CENTER);
		stateDescLabel.setBorder(border);
		add(stateDescLabel);

		userIdLabel = new JLabel();
		userIdLabel.setHorizontalAlignment(SwingConstants.CENTER);
		userIdLabel.setBorder(border);
		add(userIdLabel);

		userIdDescLabel = new JLabel("取票識別碼");
		userIdDescLabel.setHorizontalAlignment(SwingConstants.CENTER);
		userIdDescLabel.setBorder(border);
		add(userIdDescLabel);

		phoneLabel = new JLabel();
		phoneLabel.setHorizontalAlignment(SwingConstants.CENTER);
		phoneLabel.setBorder(border);
		add(phoneLabel);

		phoneDescLabel = new JLabel("電話");
		phoneDescLabel.setHorizontalAlignment(SwingConstants.CENTER);
		phoneDescLabel.setBorder(border);
		add(phoneDescLabel);

		emailLabel = new JLabel();
		emailLabel.setHorizontalAlignment(SwingConstants.CENTER);
		emailLabel.setBorder(border);
		add(emailLabel);

		emailDescLabel = new JLabel("電子郵件");
		emailDescLabel.setHorizontalAlignment(SwingConstants.CENTER);
		emailDescLabel.setBorder(border);
		add(emailDescLabel);

		DefaultTableCellRenderer detailsTableRenderer = new DefaultTableCellRenderer.UIResource()  {
			@Override
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
				setBackground(column == 0 ? GAINSBORO : Color.WHITE);
				return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			}
		};
		detailsTableRenderer.setHorizontalAlignment(SwingConstants.CENTER);

		detailsTableModel = new ReadOnlyTableModel();

		detailsTable = new JTable(detailsTableModel);
		detailsTable.setDefaultRenderer(Object.class, detailsTableRenderer);
		detailsTable.getTableHeader().setReorderingAllowed(false);
		detailsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		detailsScrollPane = new JScrollPane(detailsTable);
		detailsScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		detailsScrollPane.setBorder(BorderFactory.createEmptyBorder());
		add(detailsScrollPane);

		detailsLabel = new JLabel();

		detailsPriceLabel = new JLabel();
		detailsPriceLabel.setHorizontalAlignment(SwingConstants.TRAILING);

		detailsDescPane = new JPanel(new GridLayout(1, 2));
		detailsDescPane.add(detailsLabel);
		detailsDescPane.add(detailsPriceLabel);

		detailsPane = new JPanel(new BorderLayout());
		detailsPane.add(detailsScrollPane, BorderLayout.CENTER);
		detailsPane.add(detailsDescPane, BorderLayout.SOUTH);
		detailsPane.setBorder(BorderFactory.createTitledBorder(
				BorderFactory.createEtchedBorder(),
				"訂位明細", TitledBorder.LEADING, TitledBorder.TOP));
		add(detailsPane);

		reduceButton = new JButton("減少人數");
		reduceButton.setMargin(new Insets(0, 0, 0, 0));
		reduceButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				reduce();
			}
		});
		add(reduceButton);

		removeButton = new JButton("刪減行程");
		removeButton.setMargin(new Insets(0, 0, 0, 0));
		removeButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				remove();
			}
		});
		add(removeButton);

		cancelButton = new JButton("取消訂位");
		cancelButton.setMargin(new Insets(0, 0, 0, 0));
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				cancel();
			}
		});
		add(cancelButton);

		backButton = new JButton("返回");
		backButton.setMargin(new Insets(0, 0, 0, 0));
		backButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				stack.closeToRoot();
			}
		});
		add(backButton);
	}

	@Override
	public void doLayout() {
		int s = getWidth()/2-200;
		messageLabel.setBounds(s, 20, 400, 20);
		idField.setBounds(s+150, 60, 250, 20);
		idDescLabel.setBounds(s, 60, 150, 20);
		stateLabel.setBounds(s+150, 80, 250, 20);
		stateDescLabel.setBounds(s, 80, 150, 20);
		userIdLabel.setBounds(s+150, 100, 250, 20);
		userIdDescLabel.setBounds(s, 100, 150, 20);
		phoneLabel.setBounds(s+150, 120, 250, 20);
		phoneDescLabel.setBounds(s, 120, 150, 20);
		emailLabel.setBounds(s+150, 140, 250, 20);
		emailDescLabel.setBounds(s, 140, 150, 20);
		detailsPane.setBounds(0, 170, getWidth(), roundTrip ? 100 : 80);
		int t = roundTrip ? 290 : 270;
		reduceButton.setBounds(s+140, t, 80, 20);
		removeButton.setBounds(s+230, t, 80, 20);
		cancelButton.setBounds(s+320, t, 80, 20);
		backButton.setBounds(s, t, 80, 20);
	}

	void setData(Booking booking) {
		this.booking = booking;
		roundTrip = booking.isRoundTrip();

		idField.setText(String.format("%08d", booking.getId()));
		stateLabel.setText(booking.getStateDesc());
		userIdLabel.setText(obscureUserId(booking.getUserId()));
		phoneLabel.setText(booking.getPhone());
		emailLabel.setText(booking.getEmail());

		Trip outbound = booking.getOutbound();
		Trip inbound = booking.getInbound();

		List<String[]> data = new ArrayList<>();
		data.add(outbound.toDetailsRow());
		if(roundTrip) {
			data.add(inbound.toDetailsRow());
		}

		detailsTableModel.setDataVector(data.toArray(new String[data.size()][]), detailsColumns);

		detailsLabel.setText(booking.getTicketDesc());
		detailsPriceLabel.setText(booking.getTotalPriceDesc());

		String message = "";
		switch(booking.getState()) {
		case UNPAID:
			message = "您尚未付款!";
			break;
		case PAID:
		case MODIFIED:
			message = "您尚未取票!";
			break;
		case PICKED_UP:
			message = "您已取票!";
			break;
		case INVALIDATED:
			message = "已過付款期限!";
			break;
		default:
			break;
		}
		messageLabel.setText(message);

		reduceButton.setEnabled(canReduce());
		removeButton.setEnabled(canRemove());
		cancelButton.setEnabled(canCancel());
	}

	void setMessage(String message) {
		messageLabel.setText(message);
	}

	private void reduce() {
		reduce.setData(booking);
		stack.openPane(reduce);
	}

	private void remove() {
		remove.setData(booking);
		stack.openPane(remove);
	}

	private void cancel() {
		cancel.setData(booking);
		stack.openPane(cancel);
	}

	private String obscureUserId(String userId) {
		StringBuilder sb = new StringBuilder();
		sb.append(userId.substring(0, Math.min(3, userId.length())));
		for(int i = 0; i < userId.length()-5; ++i) {
			sb.append('*');
		}
		sb.append(userId.substring(Math.max(userId.length()-2, 0)));
		return sb.toString();
	}

	private boolean canReduce() {
		return booking.getTotalCount() > 1 && booking.getState().reducible;
	}

	private boolean canRemove() {
		return booking.isRoundTrip() && booking.getState().reducible;
	}

	private boolean canCancel() {
		return booking.getState().cancellable;
	}
}
