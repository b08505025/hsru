package tw.edu.ntu.esoe.hsru.ticket;

import java.text.NumberFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import tw.edu.ntu.esoe.hsru.data.Station;

public class Booking {

	public static final DateTimeFormatter DATE_TIME = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm");

	private int id;
	private String userId;
	private String phone;
	private String email;
	private LocalDateTime payDeadline;
	private LocalDateTime modifyDeadline;
	private LocalDateTime departureTime;
	private Station fromStation;
	private Station toStation;
	private CarType carType;
	private boolean roundTrip;
	private Trip outbound;
	private Trip inbound;
	private int adultCount;
	private int childCount;
	private int disabledCount;
	private int seniorCount;
	private int studentCount;
	private State state = State.UNPAID;

	public enum State {
		UNPAID(false, true, true),
		PAID(true, true, true),
		MODIFIED(false, true, true),
		PICKED_UP(false, false, false),
		INVALIDATED(false, false, false),
		;
		public final boolean modifiable;
		public final boolean reducible;
		public final boolean cancellable;

		State(boolean modifiable, boolean reducible, boolean cancellable) {
			this.modifiable = modifiable;
			this.reducible = reducible;
			this.cancellable = cancellable;
		}
	}

	public Booking(int id, String userId, String phone, String email, LocalDateTime payDeadline,
			LocalDateTime modifyDeadline, LocalDateTime departureTime, Station fromStation, Station toStation,
			CarType carType, boolean roundTrip, Trip outbound, Trip inbound, int adultCount, int childCount,
			int disabledCount, int seniorCount, int studentCount) {
		this.id = id;
		this.userId = userId;
		this.phone = phone;
		this.email = email;
		this.payDeadline = payDeadline;
		this.modifyDeadline = modifyDeadline;
		this.departureTime = departureTime;
		this.fromStation = fromStation;
		this.toStation = toStation;
		this.carType = carType;
		this.roundTrip = roundTrip;
		this.outbound = outbound;
		this.inbound = inbound;
		this.adultCount = adultCount;
		this.childCount = childCount;
		this.disabledCount = disabledCount;
		this.seniorCount = seniorCount;
		this.studentCount = studentCount;
	}

	public int getId() {
		return id;
	}

	public String getUserId() {
		return userId;
	}

	public String getPhone() {
		return phone;
	}

	public String getEmail() {
		return email;
	}

	public LocalDateTime getPayDeadline() {
		return payDeadline;
	}

	public Station getFromStation() {
		return fromStation;
	}

	public Station getToStation() {
		return toStation;
	}

	public CarType getCarType() {
		return carType;
	}

	public boolean isRoundTrip() {
		return roundTrip;
	}

	public Trip getOutbound() {
		return outbound;
	}

	public Trip getInbound() {
		return inbound;
	}

	public int getAdultCount() {
		return adultCount;
	}

	public int getChildCount() {
		return childCount;
	}

	public int getDisabledCount() {
		return disabledCount;
	}

	public int getSeniorCount() {
		return seniorCount;
	}

	public int getStudentCount() {
		return studentCount;
	}

	public int getTotalCount() {
		return adultCount+childCount+disabledCount+seniorCount+studentCount;
	}

	public int getTotalPrice() {
		return outbound.getPrice() + (inbound != null ? inbound.getPrice() : 0);
	}

	public State getState() {
		if(state == State.UNPAID && LocalDateTime.now().isAfter(payDeadline)) {
			state = State.INVALIDATED;
		}
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public String getTicketDesc() {
		StringBuilder sb = new StringBuilder();
		sb.append("車廂: ");
		switch(carType) {
		case STANDARD:
			sb.append("標準車廂");
			break;
		case BUSINESS:
			sb.append("商務車廂");
			break;
		}
		sb.append("　　");
		sb.append("票數: ");
		int m = roundTrip ? 2 : 1;
		List<String> tickets = new ArrayList<>();
		if(adultCount > 0) {
			tickets.add("全票 "+adultCount*m+" 張");
		}
		if(childCount > 0) {
			tickets.add("孩童票 "+childCount*m+" 張");
		}
		if(disabledCount > 0) {
			tickets.add("愛心票 "+disabledCount*m+" 張");
		}
		if(seniorCount > 0) {
			tickets.add("敬老票 "+seniorCount*m+" 張");
		}
		if(studentCount > 0) {
			tickets.add("大學生優惠票 "+studentCount*m+" 張");
		}
		sb.append(String.join(" | ", tickets));
		return sb.toString();
	}

	public String getTotalPriceDesc() {
		return "總票價 TWD "+NumberFormat.getInstance().format(getTotalPrice());
	}

	public String getPayDeadlineDesc() {
		return DATE_TIME.format(payDeadline);
	}

	public String getStateDesc() {
		switch(getState()) {
		case UNPAID:
			return "未付款 (付款期限: "+getPayDeadlineDesc()+")";
		case PAID:
		case MODIFIED:
			return "已付款";
		case PICKED_UP:
			return "已取票";
		case INVALIDATED:
			return "已過期";
		default:
			return "不明";
		}
	}

	public Object[][] getTicketTable() {
		int count = getTotalCount();
		Object[][] data = new Object[count][];
		for(int i = 0; i < count; ++i) {
			data[i] = outbound.getTickets().get(i).toTicketsRow(roundTrip ? inbound.getTickets().get(i) : null);
		}
		return data;
	}

	public void removeTickets(int[] indexes) {
		Ticket.Type[] types = outbound.removeTickets(indexes);
		if(roundTrip) {
			inbound.removeTickets(indexes);
		}
		for(Ticket.Type type : types) {
			switch(type) {
			case ADULT:
				adultCount--;
				break;
			case CHILD:
				childCount--;
				break;
			case DISABLED:
				disabledCount--;
				break;
			case SENIOR:
				seniorCount--;
				break;
			case STUDENT:
				studentCount--;
				break;
			default:
				break;
			}
		}
		BookingManager.INSTANCE.save();
	}

	public void removeTrip(boolean rInbound) {
		Trip toRemove;
		roundTrip = false;
		if(rInbound) {
			toRemove = inbound;
			inbound = null;
		}
		else {
			toRemove = outbound;
			departureTime = inbound.getDate().atTime(inbound.getDepartureTime());
			modifyDeadline = departureTime.minusMinutes(30);
			Station temp = fromStation;
			fromStation = toStation;
			toStation = temp;
			outbound = inbound;
			inbound = null;
		}

		int[] indexes = IntStream.range(0, getTotalCount()).toArray();
		toRemove.removeTickets(indexes);

		BookingManager.INSTANCE.save();
	}

	void removeAllTickets() {
		int[] indexes = IntStream.range(0, getTotalCount()).toArray();
		outbound.removeTickets(indexes);
		if(roundTrip) {
			inbound.removeTickets(indexes);
		}
	}
}
