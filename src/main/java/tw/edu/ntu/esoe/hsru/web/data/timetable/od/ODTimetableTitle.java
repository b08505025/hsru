package tw.edu.ntu.esoe.hsru.web.data.timetable.od;

public class ODTimetableTitle {

	private String startStationName = null;
	private String endStationName = null;
	private String titleSplit1 = null;
	private String titleSplit2 = null;

	public String getStartStationName() {
		return startStationName;
	}

	public String getEndStationName() {
		return endStationName;
	}

	public String getTitleSplit1() {
		return titleSplit1;
	}

	public String getTitleSplit2() {
		return titleSplit2;
	}

	@Override
	public String toString() {
		return "ODTimetableTitle [startStationName=" + startStationName + ", endStationName=" + endStationName
				+ ", titleSplit1=" + titleSplit1 + ", titleSplit2=" + titleSplit2 + "]";
	}
}
