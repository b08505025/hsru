package tw.edu.ntu.esoe.hsru.web.data;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Contains JSON utility for web data.
 */
public class JsonUtils {

	/**
	 * The Gson to use when serializing and deserializing JSON.
	 */
	public static final Gson GSON = new GsonBuilder().
			setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).
			serializeNulls().
			setPrettyPrinting().
			create();
}
