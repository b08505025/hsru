package tw.edu.ntu.esoe.hsru.ticket;

import java.time.LocalDate;

import tw.edu.ntu.esoe.hsru.data.Station;

public class Ticket {

	private int bookingId;
	private String bookingUserId;
	private Type type;
	private Seat seat;
	private LocalDate date;
	private String trainNumber;
	private Station fromStation;
	private Station toStation;

	public Ticket(int bookingId, String bookingUserId, Type type, Seat seat, LocalDate date, String trainNumber,
			Station fromStation, Station toStation) {
		this.bookingId = bookingId;
		this.bookingUserId = bookingUserId;
		this.type = type;
		this.seat = seat;
		this.date = date;
		this.trainNumber = trainNumber;
		this.fromStation = fromStation;
		this.toStation = toStation;
	}

	public int getBookingId() {
		return bookingId;
	}

	public String getBookingUserId() {
		return bookingUserId;
	}

	public Type getType() {
		return type;
	}

	public Seat getSeat() {
		return seat;
	}

	public LocalDate getDate() {
		return date;
	}

	public String getTrainNumber() {
		return trainNumber;
	}

	public Station getFromStation() {
		return fromStation;
	}

	public Station getToStation() {
		return toStation;
	}

	public Object[] toTicketsRow(Ticket inbound) {
		Object[] ret = new Object[4];
		ret[0] = Boolean.FALSE;
		ret[1] = type.nameZh;
		ret[2] = bookingUserId;
		if(inbound == null) {
			ret[3] = seat.getNameZh();
		}
		else {
			ret[3] = "去:"+seat.getNameZh()+";回:"+inbound.seat.getNameZh();
		}
		return ret;
	}

	public enum Type {
		ADULT("全票"),
		CHILD("孩童票"),
		DISABLED("愛心票"),
		SENIOR("敬老票"),
		STUDENT("大學生優惠票"),
		;

		public final String nameZh;

		Type(String nameZh) {
			this.nameZh = nameZh;
		}
	}
}
