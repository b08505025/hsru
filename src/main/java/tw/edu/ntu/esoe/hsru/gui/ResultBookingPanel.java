package tw.edu.ntu.esoe.hsru.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import tw.edu.ntu.esoe.hsru.ticket.Booking;
import tw.edu.ntu.esoe.hsru.ticket.Trip;
import tw.edu.ntu.esoe.hsru.userdata.Settings;

public class ResultBookingPanel extends JPanel {

	public static final Settings SETTINGS = HsruWindow.SETTINGS;

	public static final Color GAINSBORO = new Color(0xDDDDDD);

	public static String[] detailsColumns = {
			"行程", "日期", "車次", "起程站", "到達站", "出發時間", "到達時間", "小計", "座位"
	};

	private final StackedPanel stack;

	private final JLabel messageLabel;
	private final JTextField idField;
	private final JLabel idDescLabel;
	private final JLabel deadlineLabel;
	private final JLabel deadlineDescLabel;
	private final JLabel userIdLabel;
	private final JLabel userIdDescLabel;
	private final JLabel phoneLabel;
	private final JLabel phoneDescLabel;
	private final JLabel emailLabel;
	private final JLabel emailDescLabel;
	private final DefaultTableModel detailsTableModel;
	private final JTable detailsTable;
	private final JScrollPane detailsScrollPane;
	private final JLabel detailsLabel;
	private final JLabel detailsPriceLabel;
	private final JPanel detailsDescPane;
	private final JPanel detailsPane;
	private final JButton backButton;

	private Booking booking;
	private boolean roundTrip;

	ResultBookingPanel(StackedPanel stack) {
		super();
		this.stack = stack;

		setLayout(null);

		Border border = BorderFactory.createEtchedBorder();

		messageLabel = new JLabel("您已完成訂位!");
		messageLabel.setHorizontalAlignment(SwingConstants.CENTER);
		messageLabel.setBorder(border);
		messageLabel.setForeground(Color.RED);
		add(messageLabel);

		idField = new JTextField();
		idField.setEditable(false);
		idField.setHorizontalAlignment(SwingConstants.CENTER);
		idField.setBorder(border);
		idField.setForeground(Color.RED);
		add(idField);

		idDescLabel = new JLabel("訂位代號");
		idDescLabel.setHorizontalAlignment(SwingConstants.CENTER);
		idDescLabel.setBorder(border);
		add(idDescLabel);

		deadlineLabel = new JLabel();
		deadlineLabel.setHorizontalAlignment(SwingConstants.CENTER);
		deadlineLabel.setForeground(Color.RED);
		deadlineLabel.setBorder(border);
		add(deadlineLabel);

		deadlineDescLabel = new JLabel("付款期限");
		deadlineDescLabel.setHorizontalAlignment(SwingConstants.CENTER);
		deadlineDescLabel.setBorder(border);
		add(deadlineDescLabel);

		userIdLabel = new JLabel();
		userIdLabel.setHorizontalAlignment(SwingConstants.CENTER);
		userIdLabel.setBorder(border);
		add(userIdLabel);

		userIdDescLabel = new JLabel("取票識別碼");
		userIdDescLabel.setHorizontalAlignment(SwingConstants.CENTER);
		userIdDescLabel.setBorder(border);
		add(userIdDescLabel);

		phoneLabel = new JLabel();
		phoneLabel.setHorizontalAlignment(SwingConstants.CENTER);
		phoneLabel.setBorder(border);
		add(phoneLabel);

		phoneDescLabel = new JLabel("電話");
		phoneDescLabel.setHorizontalAlignment(SwingConstants.CENTER);
		phoneDescLabel.setBorder(border);
		add(phoneDescLabel);

		emailLabel = new JLabel();
		emailLabel.setHorizontalAlignment(SwingConstants.CENTER);
		emailLabel.setBorder(border);
		add(emailLabel);

		emailDescLabel = new JLabel("電子郵件");
		emailDescLabel.setHorizontalAlignment(SwingConstants.CENTER);
		emailDescLabel.setBorder(border);
		add(emailDescLabel);

		DefaultTableCellRenderer detailsTableRenderer = new DefaultTableCellRenderer.UIResource()  {
			@Override
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
				setBackground(column == 0 ? GAINSBORO : Color.WHITE);
				return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			}
		};
		detailsTableRenderer.setHorizontalAlignment(SwingConstants.CENTER);

		detailsTableModel = new ReadOnlyTableModel();

		detailsTable = new JTable(detailsTableModel);
		detailsTable.setDefaultRenderer(Object.class, detailsTableRenderer);
		detailsTable.getTableHeader().setReorderingAllowed(false);
		detailsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		detailsScrollPane = new JScrollPane(detailsTable);
		detailsScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		detailsScrollPane.setBorder(BorderFactory.createEmptyBorder());
		add(detailsScrollPane);

		detailsLabel = new JLabel();

		detailsPriceLabel = new JLabel();
		detailsPriceLabel.setHorizontalAlignment(SwingConstants.TRAILING);

		detailsDescPane = new JPanel(new GridLayout(1, 2));
		detailsDescPane.add(detailsLabel);
		detailsDescPane.add(detailsPriceLabel);

		detailsPane = new JPanel(new BorderLayout());
		detailsPane.add(detailsScrollPane, BorderLayout.CENTER);
		detailsPane.add(detailsDescPane, BorderLayout.SOUTH);
		detailsPane.setBorder(BorderFactory.createTitledBorder(
				BorderFactory.createEtchedBorder(),
				"訂位明細", TitledBorder.LEADING, TitledBorder.TOP));
		add(detailsPane);

		backButton = new JButton("返回");
		backButton.setMargin(new Insets(0, 0, 0, 0));
		backButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				stack.closeToRoot();
			}
		});
		add(backButton);
	}

	@Override
	public void doLayout() {
		int s = getWidth()/2-150;
		messageLabel.setBounds(s, 20, 300, 20);
		idField.setBounds(s+100, 60, 200, 20);
		idDescLabel.setBounds(s, 60, 100, 20);
		deadlineLabel.setBounds(s+100, 80, 200, 20);
		deadlineDescLabel.setBounds(s, 80, 100, 20);
		userIdLabel.setBounds(s+100, 100, 200, 20);
		userIdDescLabel.setBounds(s, 100, 100, 20);
		phoneLabel.setBounds(s+100, 120, 200, 20);
		phoneDescLabel.setBounds(s, 120, 100, 20);
		emailLabel.setBounds(s+100, 140, 200, 20);
		emailDescLabel.setBounds(s, 140, 100, 20);
		detailsPane.setBounds(0, 170, getWidth(), roundTrip ? 100 : 80);
		backButton.setBounds(getWidth()/2-40, roundTrip ? 290 : 270, 80, 20);
	}

	void setData(Booking booking) {
		this.booking = booking;
		roundTrip = booking.isRoundTrip();

		idField.setText(String.format("%08d", booking.getId()));
		deadlineLabel.setText(booking.getPayDeadlineDesc());
		userIdLabel.setText(obscureUserId(booking.getUserId()));
		phoneLabel.setText(booking.getPhone());
		emailLabel.setText(booking.getEmail());

		Trip outbound = booking.getOutbound();
		Trip inbound = booking.getInbound();

		List<String[]> data = new ArrayList<>();
		data.add(outbound.toDetailsRow());
		if(roundTrip) {
			data.add(inbound.toDetailsRow());
		}

		detailsTableModel.setDataVector(data.toArray(new String[data.size()][]), detailsColumns);

		detailsLabel.setText(booking.getTicketDesc());
		detailsPriceLabel.setText(booking.getTotalPriceDesc());
	}

	private String obscureUserId(String userId) {
		StringBuilder sb = new StringBuilder();
		sb.append(userId.substring(0, Math.min(3, userId.length())));
		for(int i = 0; i < userId.length()-5; ++i) {
			sb.append('*');
		}
		sb.append(userId.substring(Math.max(userId.length()-2, 0)));
		return sb.toString();
	}
}
