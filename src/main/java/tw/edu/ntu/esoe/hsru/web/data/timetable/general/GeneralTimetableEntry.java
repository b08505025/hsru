package tw.edu.ntu.esoe.hsru.web.data.timetable.general;

public class GeneralTimetableEntry implements Comparable<GeneralTimetableEntry> {

	private String updateTime = "";
	private String effectiveDate = "";
	private String expiringDate = "";
	private int versionID = -1;
	private GeneralTimetableInfo generalTimetable = new GeneralTimetableInfo();

	public String getUpdateTime() {
		return updateTime;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public String getExpiringDate() {
		return expiringDate;
	}

	public int getVersionID() {
		return versionID;
	}

	public GeneralTimetableInfo getGeneralTimetable() {
		return generalTimetable;
	}	

	@Override
	public int compareTo(GeneralTimetableEntry o) {
		return generalTimetable.compareTo(o.generalTimetable);
	}

	@Override
	public String toString() {
		return "GeneralTimetableEntry [updateTime=" + updateTime + ", effectiveDate=" + effectiveDate
				+ ", expiringDate=" + expiringDate + ", versionID=" + versionID + ", generalTimetable="
				+ generalTimetable + "]";
	}
}