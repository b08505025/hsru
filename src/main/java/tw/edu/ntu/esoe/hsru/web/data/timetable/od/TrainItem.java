package tw.edu.ntu.esoe.hsru.web.data.timetable.od;

import java.util.Collections;
import java.util.List;

public class TrainItem implements Comparable<TrainItem> {

	private String trainNumber = "";
	private String departureTime = "";
	private String destinationTime = "";
	private String duration = "";
	private String nonReservedCar = "";
	private List<Discount> discount = Collections.emptyList();
	private String note = "";
	private int sequence = 0;
	private List<StationInfo> stationInfo = Collections.emptyList();
	private boolean isCrossNight = false;
	private String runDate = "";
	private String departureTime_Order = "";
	private String destinationTime_Order = "";

	public String getTrainNumber() {
		return trainNumber;
	}

	public String getDepartureTime() {
		return departureTime;
	}

	public String getDestinationTime() {
		return destinationTime;
	}

	public String getDuration() {
		return duration;
	}

	public String getNonReservedCar() {
		return nonReservedCar;
	}

	public List<Discount> getDiscount() {
		return discount;
	}

	public String getNote() {
		return note;
	}

	public int getSequence() {
		return sequence;
	}

	public List<StationInfo> getStationInfo() {
		return stationInfo;
	}

	public boolean isCrossNight() {
		return isCrossNight;
	}

	public String getRunDate() {
		return runDate;
	}

	public String getDepartureTime_Order() {
		return departureTime_Order;
	}

	public String getDestinationTime_Order() {
		return destinationTime_Order;
	}

	@Override
	public int compareTo(TrainItem o) {
		return departureTime.compareTo(o.departureTime);
	}

	@Override
	public String toString() {
		return "TrainItem [trainNumber=" + trainNumber + ", departureTime=" + departureTime + ", destinationTime="
				+ destinationTime + ", duration=" + duration + ", nonReservedCar=" + nonReservedCar + ", discount="
				+ discount + ", note=" + note + ", sequence=" + sequence + ", stationInfo=" + stationInfo
				+ ", isCrossNight=" + isCrossNight + ", runDate=" + runDate + ", departureTime_Order="
				+ departureTime_Order + ", destinationTime_Order=" + destinationTime_Order + "]";
	}
}
