package tw.edu.ntu.esoe.hsru.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import tw.edu.ntu.esoe.hsru.ticket.Booking;
import tw.edu.ntu.esoe.hsru.ticket.BookingForm;
import tw.edu.ntu.esoe.hsru.ticket.BookingManager;
import tw.edu.ntu.esoe.hsru.ticket.TripSelected;
import tw.edu.ntu.esoe.hsru.ticket.TripSelectedList;
import tw.edu.ntu.esoe.hsru.userdata.Settings;
import tw.edu.ntu.esoe.hsru.web.irs.BookingFlow;

public class ConfirmBookingPanel extends JPanel {

	public static final Settings SETTINGS = HsruWindow.SETTINGS;

	public static final Color GAINSBORO = new Color(0xDDDDDD);

	private final StackedPanel stack;
	private final ResultBookingPanel result;

	private final DefaultTableModel detailsTableModel;
	private final JTable detailsTable;
	private final JScrollPane detailsScrollPane;
	private final JLabel detailsLabel;
	private final JLabel detailsPriceLabel;
	private final JPanel detailsDescPane;
	private final JPanel detailsPane;
	private final JTextField userIdField;
	private final JLabel userIdDescLabel;
	private final JLabel userIdLabel;
	private final JTextField phoneField;
	private final JLabel phoneDescLabel;
	private final JLabel phoneLabel;
	private final JTextField emailField;
	private final JLabel emailLabel;
	private final JLabel userInfoLabel;
	private final JButton confirmButton;
	private final JButton backButton;

	private BookingForm bookingForm;
	private BookingFlow bookingFlow;
	private TripSelectedList tripSelectedList;
	private boolean roundTrip;
	private boolean processing;

	ConfirmBookingPanel(StackedPanel stack) {
		super();
		this.stack = stack;

		setLayout(null);

		result = new ResultBookingPanel(stack);

		DocumentListener canSearchDocumentListener = new DocumentListener() {
			@Override
			public void insertUpdate(DocumentEvent e) {
				confirmButton.setEnabled(canConfirm());
			}
			@Override
			public void removeUpdate(DocumentEvent e) {
				confirmButton.setEnabled(canConfirm());
			}
			@Override
			public void changedUpdate(DocumentEvent e) {}
		};

		DefaultTableCellRenderer detailsTableRenderer = new DefaultTableCellRenderer.UIResource()  {
			@Override
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
				setBackground(column == 0 ? GAINSBORO : Color.WHITE);
				return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			}
		};
		detailsTableRenderer.setHorizontalAlignment(SwingConstants.CENTER);

		detailsTableModel = new ReadOnlyTableModel();

		detailsTable = new JTable(detailsTableModel);
		detailsTable.setDefaultRenderer(Object.class, detailsTableRenderer);
		detailsTable.getTableHeader().setReorderingAllowed(false);
		detailsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		detailsScrollPane = new JScrollPane(detailsTable);
		detailsScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		detailsScrollPane.setBorder(BorderFactory.createEmptyBorder());
		add(detailsScrollPane);

		detailsLabel = new JLabel();

		detailsPriceLabel = new JLabel();
		detailsPriceLabel.setHorizontalAlignment(SwingConstants.TRAILING);

		detailsDescPane = new JPanel(new GridLayout(1, 2));
		detailsDescPane.add(detailsLabel);
		detailsDescPane.add(detailsPriceLabel);

		detailsPane = new JPanel(new BorderLayout());
		detailsPane.add(detailsScrollPane, BorderLayout.CENTER);
		detailsPane.add(detailsDescPane, BorderLayout.SOUTH);
		detailsPane.setBorder(BorderFactory.createTitledBorder(
				BorderFactory.createEtchedBorder(),
				"訂位明細", TitledBorder.LEADING, TitledBorder.TOP));
		add(detailsPane);

		userIdField = new JTextField();
		userIdField.getDocument().addDocumentListener(canSearchDocumentListener);
		add(userIdField);

		userIdDescLabel = new JLabel("身分證字號/護照號碼");
		add(userIdDescLabel);

		userIdLabel = new JLabel("取票識別碼");
		add(userIdLabel);

		phoneField = new JTextField();
		add(phoneField);

		phoneDescLabel = new JLabel("行動電話/市話");
		add(phoneDescLabel);

		phoneLabel = new JLabel("電話");
		add(phoneLabel);

		emailField = new JTextField();
		add(emailField);

		emailLabel = new JLabel("電子郵件");
		add(emailLabel);

		userInfoLabel = new JLabel("取票人資訊");
		userInfoLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(userInfoLabel);

		confirmButton = new JButton("完成訂位");
		confirmButton.setMargin(new Insets(0, 0, 0, 0));
		confirmButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				confirm();
			}
		});
		add(confirmButton);

		backButton = new JButton("重新查詢");
		backButton.setMargin(new Insets(0, 0, 0, 0));
		backButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				stack.closeToRoot();
			}
		});
		add(backButton);
	}

	@Override
	public void doLayout() {
		detailsPane.setBounds(0, 0, getWidth(), roundTrip ? 100 : 80);
		int s = getWidth()/2;
		int s0 = s-150;
		int s1 = s0+100;
		int t = roundTrip ? 110 : 90;
		userIdField.setBounds(s1, t+50, 200, 20);
		userIdDescLabel.setBounds(s1, t+30, 200, 20);
		userIdLabel.setBounds(s0, t+40, 100, 20);
		phoneField.setBounds(s1, t+90, 200, 20);
		phoneDescLabel.setBounds(s1, t+70, 200, 20);
		phoneLabel.setBounds(s0, t+80, 100, 20);
		emailField.setBounds(s1, t+120, 200, 20);
		emailLabel.setBounds(s0, t+120, 100, 20);
		userInfoLabel.setBounds(s-100, t, 200, 20);
		confirmButton.setBounds(s+20, t+160, 80, 20);
		backButton.setBounds(s-100, t+160, 80, 20);
	}

	void setData(BookingForm bookingForm, BookingFlow bookingFlow) {
		processing = true;
		this.bookingForm = bookingForm;
		this.bookingFlow = bookingFlow;
		tripSelectedList = bookingFlow.getTripSelectedList(bookingForm);
		roundTrip = bookingForm.isRoundTrip();

		String[] columns = tripSelectedList.getColumns();
		TripSelected outbound = tripSelectedList.getOutbound();
		TripSelected inbound = tripSelectedList.getInbound();

		List<String[]> data = new ArrayList<>();
		data.add(outbound.toDetailsRow());
		if(roundTrip) {
			data.add(inbound.toDetailsRow());
		}

		detailsTableModel.setDataVector(data.toArray(new String[data.size()][]), columns);

		detailsLabel.setText(bookingForm.getTicketDesc());
		detailsPriceLabel.setText(tripSelectedList.getTotalPriceDesc());

		processing = false;
		confirmButton.setEnabled(canConfirm());
	}

	private void confirm() {
		String userId = userIdField.getText().toUpperCase(Locale.US);
		String phone = phoneField.getText();
		String email = emailField.getText();
		new SwingWorker<Booking, Void>() {
			@Override
			protected Booking doInBackground() throws Exception {
				return BookingManager.INSTANCE.addBooking(bookingForm, tripSelectedList, userId, phone, email);
			}
			@Override
			protected void done() {
				try {
					result.setData(get());
					stack.openPane(result);
				}
				catch(Exception e) {
					HsruWindow.showErrorMessage(e);
					stack.closeToRoot();
				}
			}
		}.execute();
	}

	private boolean canConfirm() {
		return !processing && !userIdField.getText().isEmpty();
	}
}
