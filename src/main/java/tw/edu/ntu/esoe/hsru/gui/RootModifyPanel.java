package tw.edu.ntu.esoe.hsru.gui;

import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import javax.swing.border.Border;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import com.github.lgooddatepicker.components.DatePicker;

import tw.edu.ntu.esoe.hsru.data.Station;
import tw.edu.ntu.esoe.hsru.ticket.Booking;
import tw.edu.ntu.esoe.hsru.ticket.BookingManager;
import tw.edu.ntu.esoe.hsru.userdata.Settings;

/**
 * The root booking details modify and search panel, replicating the booking history tab on THSR's booking website.
 */
public class RootModifyPanel extends JPanel {

	public static final Settings SETTINGS = HsruWindow.SETTINGS;

	public static final DateTimeFormatter DATE = DateTimeFormatter.ofPattern("yyyy/MM/dd");

	public static String[] stationNames = {
			"南港", "台北", "板橋", "桃園", "新竹", "苗栗", "台中", "彰化", "雲林", "嘉義", "台南", "左營"
	};

	private final StackedPanel stack;
	private final DetailsModifyPanel details;

	private final JTextField userIdField0;
	private final JLabel userIdDescLabel0;
	private final JLabel userIdLabel0;
	private final JTextField bookingIdField;
	private final JLabel bookingIdLabel;
	private final JButton searchByIdButton;
	private final JLabel searchByIdLabel;
	private final JTextField userIdField1;
	private final JLabel userIdDescLabel1;
	private final JLabel userIdLabel1;
	private final JComboBox fromStationComboBox;
	private final JLabel fromStationLabel;
	private final JButton swapButton;
	private final JComboBox toStationComboBox;
	private final JLabel toStationLabel;
	private final JLabel stationLabel;
	private final DatePicker datePicker;
	private final JLabel dateLabel;
	private final JTextField trainNoField;
	private final JLabel trainNoLabel;
	private final JButton searchByTrainButton;
	private final JLabel searchByTrainLabel;

	private boolean searching = false;

	RootModifyPanel(StackedPanel stack) {
		super();
		this.stack = stack;

		setLayout(null);

		details = new DetailsModifyPanel(stack);

		Border border = BorderFactory.createEtchedBorder();

		DocumentListener canSearchByIdDocumentListener = new DocumentListener() {
			@Override
			public void insertUpdate(DocumentEvent e) {
				searchByIdButton.setEnabled(canSearchById());
			}
			@Override
			public void removeUpdate(DocumentEvent e) {
				searchByIdButton.setEnabled(canSearchById());
			}
			@Override
			public void changedUpdate(DocumentEvent e) {}
		};

		userIdField0 = new JTextField();
		userIdField0.getDocument().addDocumentListener(canSearchByIdDocumentListener);
		add(userIdField0);

		userIdDescLabel0 = new JLabel("身分證字號/護照號碼");
		add(userIdDescLabel0);

		userIdLabel0 = new JLabel("取票識別碼");
		add(userIdLabel0);

		bookingIdField = new JTextField();
		bookingIdField.getDocument().addDocumentListener(canSearchByIdDocumentListener);
		add(bookingIdField);

		bookingIdLabel = new JLabel("訂位代號");
		add(bookingIdLabel);

		searchByIdButton = new JButton("開始查詢");
		searchByIdButton.setMargin(new Insets(0, 0, 0, 0));
		searchByIdButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				searchById();
			}
		});
		searchByIdButton.setEnabled(false);
		add(searchByIdButton);

		searchByIdLabel = new JLabel("依訂位代號查詢訂位紀錄");
		searchByIdLabel.setHorizontalAlignment(SwingConstants.CENTER);
		searchByIdLabel.setBorder(border);
		add(searchByIdLabel);

		DocumentListener canSearchByTrainDocumentListener = new DocumentListener() {
			@Override
			public void insertUpdate(DocumentEvent e) {
				searchByTrainButton.setEnabled(canSearchByTrain());
			}
			@Override
			public void removeUpdate(DocumentEvent e) {
				searchByTrainButton.setEnabled(canSearchByTrain());
			}
			@Override
			public void changedUpdate(DocumentEvent e) {}
		};

		ActionListener canSearchByTrainListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				searchByTrainButton.setEnabled(canSearchByTrain());
			}
		};

		userIdField1 = new JTextField();
		userIdField1.getDocument().addDocumentListener(canSearchByTrainDocumentListener);
		add(userIdField1);

		userIdDescLabel1 = new JLabel("身分證字號/護照號碼");
		add(userIdDescLabel1);

		userIdLabel1 = new JLabel("取票識別碼");
		add(userIdLabel1);

		fromStationComboBox = new JComboBox(stationNames);
		fromStationComboBox.setEditable(false);
		fromStationComboBox.setSelectedIndex(0);
		fromStationComboBox.addActionListener(canSearchByTrainListener);
		add(fromStationComboBox);

		fromStationLabel = new JLabel("起程站");
		add(fromStationLabel);

		swapButton = new JButton("⇄");
		swapButton.setMargin(new Insets(0, 0, 0, 0));
		swapButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int fromIndex = fromStationComboBox.getSelectedIndex();
				int toIndex = toStationComboBox.getSelectedIndex();
				fromStationComboBox.setSelectedIndex(toIndex);
				toStationComboBox.setSelectedIndex(fromIndex);
			}
		});
		add(swapButton);

		toStationComboBox = new JComboBox(stationNames);
		toStationComboBox.setEditable(false);
		toStationComboBox.setSelectedIndex(11);
		toStationComboBox.addActionListener(canSearchByTrainListener);
		add(toStationComboBox);

		toStationLabel = new JLabel("到達站");
		add(toStationLabel);

		stationLabel = new JLabel("起訖站");
		add(stationLabel);

		datePicker = new DatePicker();
		datePicker.getSettings().setSizeTextFieldMinimumWidth(0);
		datePicker.getSettings().setSizeTextFieldMinimumWidthDefaultOverride(false);
		datePicker.getSettings().setFormatForDatesCommonEra(DATE);
		datePicker.getSettings().setFormatForTodayButton(DATE);
		datePicker.getSettings().setAllowEmptyDates(false);
		datePicker.setDateToToday();
		add(datePicker);

		dateLabel = new JLabel("去程日期");
		add(dateLabel);

		trainNoField = new JTextField();
		trainNoField.getDocument().addDocumentListener(canSearchByTrainDocumentListener);
		add(trainNoField);

		trainNoLabel = new JLabel("車次號碼");
		add(trainNoLabel);

		searchByTrainButton = new JButton("開始查詢");
		searchByTrainButton.setMargin(new Insets(0, 0, 0, 0));
		searchByTrainButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				searchByTrain();
			}
		});
		searchByTrainButton.setEnabled(false);
		add(searchByTrainButton);

		searchByTrainLabel = new JLabel("依車次查詢訂位紀錄");
		searchByTrainLabel.setHorizontalAlignment(SwingConstants.CENTER);
		searchByTrainLabel.setBorder(border);
		add(searchByTrainLabel);

		stack.openPane(this);
	}

	@Override
	public void doLayout() {
		int s = getWidth()/2;
		int s0 = s-150;
		int s1 = s0+100;
		userIdField0.setBounds(s1, 60, 200, 20);
		userIdDescLabel0.setBounds(s1, 40, 200, 20);
		userIdLabel0.setBounds(s0, 50, 100, 20);
		bookingIdField.setBounds(s1, 90, 200, 20);
		bookingIdLabel.setBounds(s0, 90, 100, 20);
		searchByIdButton.setBounds(s-40, 120, 80, 20);
		searchByIdLabel.setBounds(s0, 20, 300, 20);
		userIdField1.setBounds(s1, 200, 200, 20);
		userIdDescLabel1.setBounds(s1, 180, 200, 20);
		userIdLabel1.setBounds(s0, 190, 100, 20);
		fromStationComboBox.setBounds(s1, 250, 70, 20);
		fromStationLabel.setBounds(s1, 230, 70, 20);
		swapButton.setBounds(s1+75, 250, 20, 20);
		toStationComboBox.setBounds(s1+100, 250, 70, 20);
		toStationLabel.setBounds(s1+100, 230, 70, 20);
		stationLabel.setBounds(s0, 240, 100, 20);
		datePicker.setBounds(s1, 280, 105, 20);
		dateLabel.setBounds(s0, 280, 100, 20);
		trainNoField.setBounds(s1, 310, 70, 20);
		trainNoLabel.setBounds(s0, 310, 100, 20);
		searchByTrainButton.setBounds(s-40, 340, 80, 20);
		searchByTrainLabel.setBounds(s0, 160, 300, 20);
	}

	private void searchById() {
		searching = true;
		searchByIdButton.setEnabled(false);
		searchByTrainButton.setEnabled(false);

		String userId = userIdField0.getText();
		String id = bookingIdField.getText();

		new SwingWorker<Booking, Void>() {
			@Override
			protected Booking doInBackground() throws Exception {
				return BookingManager.INSTANCE.searchBooking(userId, id);
			}
			@Override
			protected void done() {
				try {
					Booking booking = get();
					if(booking != null) {
						details.setData(booking);
						stack.openPane(details);
					}
					else {
						HsruWindow.showErrorMessage("查無紀錄");
					}
				}
				catch(Exception e) {
					HsruWindow.showErrorMessage(e);
				}
				searching = false;
				searchByIdButton.setEnabled(canSearchById());
				searchByTrainButton.setEnabled(canSearchByTrain());
			}
		}.execute();
	}

	private void searchByTrain() {
		searching = true;
		searchByIdButton.setEnabled(false);
		searchByTrainButton.setEnabled(false);

		String userId = userIdField1.getText();
		Station fromStation = Station.values()[fromStationComboBox.getSelectedIndex()];
		Station toStation = Station.values()[toStationComboBox.getSelectedIndex()];
		LocalDate date = datePicker.getDate();
		String trainNumber = getTrainNo();

		new SwingWorker<Booking, Void>() {
			@Override
			protected Booking doInBackground() throws Exception {
				return BookingManager.INSTANCE.searchBooking(userId, fromStation, toStation, date, trainNumber);
			}
			@Override
			protected void done() {
				try {
					Booking booking = get();
					if(booking != null) {
						details.setData(booking);
						stack.openPane(details);
					}
					else {
						HsruWindow.showErrorMessage("查無紀錄");
					}
				}
				catch(Exception e) {
					HsruWindow.showErrorMessage(e);
				}
				searching = false;
				searchByIdButton.setEnabled(canSearchById());
				searchByTrainButton.setEnabled(canSearchByTrain());
			}
		}.execute();
	}

	private boolean canSearchById() {
		return !searching && !userIdField0.getText().isEmpty() && !bookingIdField.getText().isEmpty();
	}

	private boolean canSearchByTrain() {
		int fromIndex = fromStationComboBox.getSelectedIndex();
		int toIndex = toStationComboBox.getSelectedIndex();
		return !searching && !userIdField1.getText().isEmpty() && isTrainNoValid(trainNoField.getText(), fromIndex, toIndex);
	}

	private boolean isTrainNoValid(String trainNo, int fromIndex, int toIndex) {
		if(fromIndex < toIndex) {
			return trainNo.matches("(0|1(?=[2356]))?[123568]\\d[13579]");
		}
		else {
			return trainNo.matches("(0|1(?=[2356]))?[123568]\\d[02468]");
		}
	}

	private String getTrainNo() {
		String trainNumber = trainNoField.getText();
		trainNumber = (trainNumber.length() == 3 ? "0" : "") + trainNumber;
		return trainNumber;
	}
}
