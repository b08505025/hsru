package tw.edu.ntu.esoe.hsru.web.data.timetable.od;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.EnumSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.zip.GZIPInputStream;

import org.jsoup.Connection;
import org.jsoup.Jsoup;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import tw.edu.ntu.esoe.hsru.data.Station;
import tw.edu.ntu.esoe.hsru.web.data.JsonUtils;

public class ODTimetableRequest {

	public static final DateTimeFormatter DATE = DateTimeFormatter.ofPattern("yyyy/MM/dd");
	public static final DateTimeFormatter TIME = DateTimeFormatter.ofPattern("HH:mm");

	/**
	 * Requests the timetable data for the specified inputs.
	 * @param language The language the search results should be in. Can be "TW", "EN", or "JP".
	 * @param fromStation The start station.
	 * @param toStation The destination station.
	 * @param roundTrip Whether this query is round trip or not.
	 * @param outboundDateTime The outbound trip time.
	 * @param inboundDateTime The inbound trip time. Will be ignored if roundTrip is false.
	 * @param discountTypes The discount types.
	 * @return The timetable data.
	 * @throws IOException If the query did not work properly.
	 * @see <a href="https://en.thsrc.com.tw/ArticleContent/a3b630bb-1066-4352-a1ef-58c7b4e8ef7c">THSRC Timetable and Fare Search</a>
	 */
	public static ODTimetableData getTimetableData(String language, Station fromStation, Station toStation, boolean roundTrip, LocalDateTime outboundDateTime, LocalDateTime inboundDateTime, EnumSet<DiscountType> discountTypes) throws IOException {
		return getTimetableData(language, fromStation.getQueryName(), toStation.getQueryName(), roundTrip, outboundDateTime, inboundDateTime, discountTypes);
	}

	public static ODTimetableData getTimetableData(String language, String fromStation, String toStation, boolean roundTrip, LocalDateTime outboundDateTime, LocalDateTime inboundDateTime, EnumSet<DiscountType> discountTypes) throws IOException {
		String searchUrl = "https://www.thsrc.com.tw/TimeTable/Search";
		// The request site returns 405 if input is invalid
		Map<String, String> data = new LinkedHashMap<>();
		data.put("SearchType", roundTrip ? "R" : "S");
		data.put("Lang", language);
		data.put("StartStation", fromStation);
		data.put("EndStation", toStation);
		data.put("OutWardSearchDate", DATE.format(outboundDateTime));
		data.put("OutWardSearchTime", TIME.format(outboundDateTime));
		data.put("ReturnSearchDate", DATE.format(inboundDateTime));
		data.put("ReturnSearchTime", TIME.format(inboundDateTime));
		data.put("DiscountType", discountTypes.stream().map(DiscountType::getUUID).collect(Collectors.joining(",")));

		System.out.println(searchUrl);

		Connection.Response response = Jsoup.connect(searchUrl).
				method(Connection.Method.POST).ignoreContentType(true).
				followRedirects(true).header("Accept-Encoding", "gzip").
				data(data).execute();

		System.out.println(response.headers());

		if(response.statusCode() != 200) {
			throw new IOException("Connection responded "+response.statusCode()+" "+response.statusMessage());
		}

		BufferedReader in;
		if("gzip".equals(response.contentType())) {
			InputStreamReader reader = new InputStreamReader(new GZIPInputStream(response.bodyStream()));
			in = new BufferedReader(reader);
		}
		else {
			InputStreamReader reader = new InputStreamReader(response.bodyStream());
			in = new BufferedReader(reader);
		}
		JsonObject jsonObj = JsonParser.parseReader(in).getAsJsonObject();

		return JsonUtils.GSON.fromJson(jsonObj.get("data"), ODTimetableData.class);
	}
}
