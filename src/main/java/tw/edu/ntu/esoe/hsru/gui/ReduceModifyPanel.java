package tw.edu.ntu.esoe.hsru.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.stream.IntStream;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import tw.edu.ntu.esoe.hsru.ticket.Booking;
import tw.edu.ntu.esoe.hsru.userdata.Settings;

public class ReduceModifyPanel extends JPanel {

	public static final Settings SETTINGS = HsruWindow.SETTINGS;

	public static String[] ticketColumns = {
			"取消訂位", "票數", "取票識別碼", "座位"
	};

	private final StackedPanel stack;
	private final DetailsModifyPanel details;

	private final JTextField idField;
	private final JLabel idDescLabel;
	private final DefaultTableModel ticketsTableModel;
	private final JTable ticketsTable;
	private final JScrollPane ticketsScrollPane;
	private final JLabel ticketsLabel;
	private final JPanel ticketsPane;
	private final JCheckBox confirmCheckBox;
	private final JButton confirmButton;
	private final JButton backButton;

	private Booking booking;
	private boolean processing;

	ReduceModifyPanel(StackedPanel stack, DetailsModifyPanel details) {
		super();
		this.stack = stack;
		this.details = details;

		setLayout(null);

		Border border = BorderFactory.createEtchedBorder();

		idField = new JTextField();
		idField.setEditable(false);
		idField.setHorizontalAlignment(SwingConstants.CENTER);
		idField.setBorder(border);
		idField.setForeground(Color.RED);
		add(idField);

		idDescLabel = new JLabel("訂位代號");
		idDescLabel.setHorizontalAlignment(SwingConstants.CENTER);
		idDescLabel.setBorder(border);
		add(idDescLabel);

		DefaultTableCellRenderer ticketsTableRenderer = new DefaultTableCellRenderer.UIResource();
		ticketsTableRenderer.setHorizontalAlignment(SwingConstants.CENTER);

		ticketsTableModel = new ReadOnlyTableModel() {
			@Override
			public Class<?> getColumnClass(int columnIndex) {
				return columnIndex == 0 ? Boolean.class : Object.class;
			}
		};
		ticketsTableModel.addTableModelListener(new TableModelListener() {
			@Override
			public void tableChanged(TableModelEvent e) {
				confirmButton.setEnabled(canConfirm());
			}
		});

		ticketsTable = new JTable(ticketsTableModel);
		ticketsTable.setDefaultRenderer(Object.class, ticketsTableRenderer);
		ticketsTable.getTableHeader().setReorderingAllowed(false);
		ticketsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		ticketsScrollPane = new JScrollPane(ticketsTable);
		ticketsScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		ticketsScrollPane.setBorder(BorderFactory.createEmptyBorder());
		add(ticketsScrollPane);

		ticketsLabel = new JLabel();

		ticketsPane = new JPanel(new BorderLayout());
		ticketsPane.add(ticketsScrollPane, BorderLayout.CENTER);
		ticketsPane.add(ticketsLabel, BorderLayout.NORTH);
		ticketsPane.setBorder(BorderFactory.createTitledBorder(
				BorderFactory.createEtchedBorder(),
				"乘客資訊", TitledBorder.LEADING, TitledBorder.TOP));
		add(ticketsPane);

		confirmCheckBox = new JCheckBox();
		confirmCheckBox.setHorizontalAlignment(SwingConstants.CENTER);
		confirmCheckBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				confirmButton.setEnabled(canConfirm());
			}
		});
		add(confirmCheckBox);

		confirmButton = new JButton("下一步");
		confirmButton.setMargin(new Insets(0, 0, 0, 0));
		confirmButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				confirm();
			}
		});
		confirmButton.setEnabled(false);
		add(confirmButton);

		backButton = new JButton("回上一頁");
		backButton.setMargin(new Insets(0, 0, 0, 0));
		backButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				stack.closePane();
			}
		});
		add(backButton);
	}

	@Override
	public void doLayout() {
		int s = getWidth()/2-150;
		idField.setBounds(s+100, 20, 200, 20);
		idDescLabel.setBounds(s, 20, 100, 20);
		ticketsPane.setBounds(0, 50, getWidth(), 220);
		confirmCheckBox.setBounds(0, 280, getWidth(), 20);
		confirmButton.setBounds(s+220, 310, 80, 20);
		backButton.setBounds(s, 310, 80, 20);
	}

	void setData(Booking booking) {
		processing = true;
		this.booking = booking;

		String id = String.format("%08d", booking.getId());
		idField.setText(id);
		ticketsLabel.setText("以下為訂位代號 "+id+" 之乘客資訊，請勾選欲取消之乘客後點選確定。");
		ticketsTableModel.setDataVector(booking.getTicketTable(), ticketColumns);
		confirmCheckBox.setText("我確定取消以上所勾選之訂位 (訂位代號 "+id+")");

		confirmCheckBox.setSelected(false);
		confirmButton.setEnabled(false);

		processing = false;
	}

	private void confirm() {
		processing = true;
		confirmButton.setEnabled(false);
		backButton.setEnabled(false);

		int[] indexes = getIndexes();

		new SwingWorker<Void, Void>() {
			@Override
			protected Void doInBackground() throws Exception {
				booking.removeTickets(indexes);
				return null;
			}
			@Override
			protected void done() {
				try {
					get();
					details.setData(booking);
					details.setMessage("您已成功減少人數!");
					stack.closePane();
				}
				catch(Exception e) {
					HsruWindow.showErrorMessage(e);
					stack.closeToRoot();
				}
				processing = false;
				confirmButton.setEnabled(canConfirm());
				backButton.setEnabled(true);
			}
		}.execute();
	}

	private boolean canConfirm() {
		return !processing && confirmCheckBox.isSelected() && booking.getState().reducible && isTicketSelectionValid();
	}

	private boolean isTicketSelectionValid() {
		boolean allFalse = true;
		boolean allTrue = true;
		for(int i = 0; i < booking.getTotalCount(); i++) {
			if((Boolean)ticketsTableModel.getValueAt(i, 0)) {
				allFalse = false;
			}
			else {
				allTrue = false;
			}
		}
		return !allFalse && !allTrue;
	}

	private int[] getIndexes() {
		return IntStream.range(0, booking.getTotalCount()).
				filter(i->(Boolean)ticketsTableModel.getValueAt(i, 0)).toArray();
	}
}
