package tw.edu.ntu.esoe.hsru.data;

import java.util.List;

import tw.edu.ntu.esoe.hsru.web.data.station.RailStation;

/**
 * An enumeration of all current HSR stations.
 */
public enum Station {

	NANGANG ("0990", "南港", "Nangang" , "NanGang" ),
	TAIPEI  ("1000", "台北", "Taipei"  , "TaiPei"  ),
	BANQIAO ("1010", "板橋", "Banqiao" , "BanQiao" ),
	TAOYUAN ("1020", "桃園", "Taoyuan" , "TaoYuan" ),
	HSINCHU ("1030", "新竹", "Hsinchu" , "XinZhu"  ),
	MIAOLI  ("1035", "苗栗", "Miaoli"  , "MiaoLi"  ),
	TAICHUNG("1040", "台中", "Taichung", "TaiZhong"),
	CHANGHUA("1043", "彰化", "Changhua", "ZhangHua"),
	YUNLIN  ("1047", "雲林", "Yunlin"  , "YunLin"  ),
	CHIAYI  ("1050", "嘉義", "Chiayi"  , "JiaYi"   ),
	TAINAN  ("1060", "台南", "Tainan"  , "TaiNan"  ),
	ZUOYING ("1070", "左營", "Zuoying" , "ZuoYing" ),
	;

	/**
	 * The array of stations in southbound (natural) order.
	 */
	public static final Station[] SOUTHBOUND = {
			NANGANG, TAIPEI, BANQIAO, TAOYUAN, HSINCHU, MIAOLI,
			TAICHUNG, CHANGHUA, YUNLIN, CHIAYI, TAINAN, ZUOYING,
	};
	/**
	 * The array of stations in northbound (reverse) order.
	 */
	public static final Station[] NORTHBOUND = {
			ZUOYING, TAINAN, CHIAYI, YUNLIN, CHANGHUA, TAICHUNG,
			MIAOLI, HSINCHU, TAOYUAN, BANQIAO, TAIPEI, NANGANG,
	};

	public final String stationID;
	public final String nameZh;
	public final String nameEn;
	public final String queryName;
	private RailStation stationInfo = new RailStation();

	Station(String stationID, String nameZh, String nameEn, String queryName) {
		this.stationID = stationID;
		this.nameZh = nameZh;
		this.nameEn = nameEn;
		this.queryName = queryName;
	}

	/**
	 * @return The station ID of this station.
	 */
	public String getStationID() {
		return stationID;
	}

	/**
	 * @return The Chinese name of this station.
	 */
	public String getNameZh() {
		return nameZh;
	}

	/**
	 * @return The English name of this station.
	 */
	public String getNameEn() {
		return nameEn;
	}

	/**
	 * @return The name of this station used for query in
	 * {@link tw.edu.ntu.esoe.hsru.web.data.timetable.od.ODTimetableRequest ODTimetableRequest}.
	 */
	public String getQueryName() {
		return queryName;
	}

	public RailStation getStationInfo() {
		return stationInfo;
	}

	/**
	 * Finds the station with the specified name.
	 * @param name The English or Chinese name of the station.
	 * @return The station with the specified name, or NANGANG if no station has the name.
	 */
	public static Station fromName(String name) {
		for(Station station : values()) {
			if(station.nameEn.equals(name) || station.nameZh.equals(name)) {
				return station;
			}
		}
		return Station.NANGANG;
	}

	public static void setStationInfo(List<RailStation> stations) {
		for(RailStation railStation : stations) {
			String id = railStation.getStationID();
			for(Station station : values()) {
				if(station.stationID.equals(id)) {
					station.stationInfo = railStation;
					break;
				}
			}
		}
	}
}
