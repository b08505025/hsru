package tw.edu.ntu.esoe.hsru.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import tw.edu.ntu.esoe.hsru.ticket.BookingForm;
import tw.edu.ntu.esoe.hsru.ticket.TripCandidate;
import tw.edu.ntu.esoe.hsru.ticket.TripCandidateList;
import tw.edu.ntu.esoe.hsru.userdata.Settings;
import tw.edu.ntu.esoe.hsru.web.irs.BookingFlow;

public class SelectBookingPanel extends JPanel {

	public static final Settings SETTINGS = HsruWindow.SETTINGS;

	public static final DateTimeFormatter DATE = DateTimeFormatter.ofPattern("MM/dd");

	public static final Color GAINSBORO = new Color(0xDDDDDD);

	public static String[] detailsColumns = {
			"行程", "日期", "車次", "起程站", "到達站", "出發時間", "到達時間"
	};

	private final StackedPanel stack;
	private final ConfirmBookingPanel confirm;

	private final DefaultTableModel outboundTableModel;
	private final JTable outboundTable;
	private final JScrollPane outboundScrollPane;
	private final DefaultTableModel inboundTableModel;
	private final JTable inboundTable;
	private final JScrollPane inboundScrollPane;
	private final JPanel tablePane;
	private final DefaultTableModel detailsTableModel;
	private final JTable detailsTable;
	private final JScrollPane detailsScrollPane;
	private final JLabel detailsLabel;
	private final JPanel detailsPane;
	private final JButton selectButton;
	private final JButton backButton;

	private BookingForm bookingForm;
	private BookingFlow bookingFlow;
	private TripCandidateList tripCandidateList;
	private boolean roundTrip;
	private boolean processing;

	SelectBookingPanel(StackedPanel stack, ConfirmBookingPanel confirm) {
		super();
		this.stack = stack;

		setLayout(null);

		this.confirm = confirm;

		outboundTableModel = new ReadOnlyTableModel();
		inboundTableModel = new ReadOnlyTableModel();

		DefaultTableCellRenderer tripTableRenderer = new DefaultTableCellRenderer.UIResource() {
			@Override
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
				setBackground(column == 0 ? GAINSBORO : Color.WHITE);
				return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			}
		};
		tripTableRenderer.setHorizontalAlignment(SwingConstants.CENTER);

		ListSelectionListener updateSelectionListener = new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				updateSelection();
			}
		};

		outboundTable = new JTable(outboundTableModel);
		outboundTable.setDefaultRenderer(Object.class, tripTableRenderer);
		outboundTable.getTableHeader().setReorderingAllowed(false);
		outboundTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		outboundTable.getSelectionModel().addListSelectionListener(updateSelectionListener);

		inboundTable = new JTable(inboundTableModel);
		inboundTable.setDefaultRenderer(Object.class, tripTableRenderer);
		inboundTable.getTableHeader().setReorderingAllowed(false);
		inboundTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		inboundTable.getSelectionModel().addListSelectionListener(updateSelectionListener);

		outboundScrollPane = new JScrollPane(outboundTable);
		outboundScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		inboundScrollPane = new JScrollPane(inboundTable);
		inboundScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

		tablePane = new JPanel();
		tablePane.setLayout(new GridLayout(1, 0));
		tablePane.add(outboundScrollPane);
		add(tablePane);

		DefaultTableCellRenderer detailsTableRenderer = new DefaultTableCellRenderer.UIResource();
		detailsTableRenderer.setHorizontalAlignment(SwingConstants.CENTER);

		detailsTableModel = new ReadOnlyTableModel();
		detailsTableModel.setColumnIdentifiers(detailsColumns);

		detailsTable = new JTable(detailsTableModel);
		detailsTable.setDefaultRenderer(Object.class, detailsTableRenderer);
		detailsTable.getTableHeader().setReorderingAllowed(false);
		detailsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		detailsScrollPane = new JScrollPane(detailsTable);
		detailsScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		detailsScrollPane.setBorder(BorderFactory.createEmptyBorder());

		detailsLabel = new JLabel();

		detailsPane = new JPanel(new BorderLayout());
		detailsPane.add(detailsScrollPane, BorderLayout.CENTER);
		detailsPane.add(detailsLabel, BorderLayout.SOUTH);
		detailsPane.setBorder(BorderFactory.createTitledBorder(
				BorderFactory.createEtchedBorder(),
				"訂位明細", TitledBorder.LEADING, TitledBorder.TOP));
		add(detailsPane);

		selectButton = new JButton("確認車次");
		selectButton.setMargin(new Insets(0, 0, 0, 0));
		selectButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				select();
			}
		});
		add(selectButton);

		backButton = new JButton("重新查詢");
		backButton.setMargin(new Insets(0, 0, 0, 0));
		backButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				stack.closeToRoot();
			}
		});
		add(backButton);
	}

	@Override
	public void doLayout() {
		tablePane.setBounds(0, 0, getWidth(), 205);
		detailsPane.setBounds(0, 205, getWidth(), roundTrip ? 100 : 80);
		int s = getWidth()/2;
		int t = roundTrip ? 315 : 295;
		selectButton.setBounds(s+20, t, 80, 20);
		backButton.setBounds(s-100, t, 80, 20);
	}

	void setData(BookingForm bookingForm, BookingFlow bookingFlow) {
		processing = true;
		this.bookingForm = bookingForm;
		this.bookingFlow = bookingFlow;
		tripCandidateList = bookingFlow.getTripCandidateList(bookingForm);
		roundTrip = bookingForm.isRoundTrip();

		String[] outboundColumns = tripCandidateList.getOutboundColumns();
		List<TripCandidate> outbound = tripCandidateList.getOutbound();
		String[] inboundColumns = tripCandidateList.getInboundColumns();
		List<TripCandidate> inbound = tripCandidateList.getInbound();

		String[][] data;

		data = outbound.stream().map(TripCandidate::toCandidateRow).toArray(String[][]::new);
		outboundTableModel.setDataVector(data, outboundColumns);
		outboundTable.setRowSelectionInterval(0, 0);
		outboundScrollPane.setBorder(BorderFactory.createTitledBorder(
				BorderFactory.createEtchedBorder(),
				bookingForm.getOutboundDesc(),
				TitledBorder.LEADING, TitledBorder.TOP));

		if(roundTrip) {
			data = inbound.stream().map(TripCandidate::toCandidateRow).toArray(String[][]::new);
			inboundTableModel.setDataVector(data, inboundColumns);
			inboundTable.setRowSelectionInterval(0, 0);
			inboundScrollPane.setBorder(BorderFactory.createTitledBorder(
					BorderFactory.createEtchedBorder(),
					bookingForm.getInboundDesc(),
					TitledBorder.LEADING, TitledBorder.TOP));
			tablePane.add(inboundScrollPane);
		}
		else {
			inboundTable.clearSelection();
			tablePane.remove(inboundScrollPane);
		}

		detailsLabel.setText(bookingForm.getTicketDesc());

		processing = false;
		updateSelection();
	}

	private void select() {
		if(bookingFlow != null && bookingFlow.getState() == BookingFlow.State.SELECT) {
			processing = true;
			selectButton.setEnabled(false);
			backButton.setEnabled(false);
			TripCandidate outboundCandidate = tripCandidateList.getOutbound().get(outboundTable.getSelectedRow());
			TripCandidate inboundCandidate = roundTrip ? tripCandidateList.getInbound().get(inboundTable.getSelectedRow()) : null;
			new SwingWorker<Void, Void>() {
				@Override
				protected Void doInBackground() throws Exception {
					bookingFlow.select(outboundCandidate, inboundCandidate);
					return null;
				}
				@Override
				protected void done() {
					try {
						get();
						if(bookingFlow.getState() == BookingFlow.State.CONFIRM) {
							confirm.setData(bookingForm, bookingFlow);
							stack.openPane(confirm);
						}
						else if(bookingFlow.getState() == BookingFlow.State.SELECT) {
							HsruWindow.showErrorMessage(bookingFlow.getErrorMessage());
						}
						else {
							HsruWindow.showErrorMessage(bookingFlow.getErrorMessage());
							stack.closeToRoot();
						}
					}
					catch(Exception e) {
						HsruWindow.showErrorMessage(e);
						stack.closeToRoot();
					}
					processing = false;
					selectButton.setEnabled(canSelect());
					backButton.setEnabled(true);
					updateSelection();
				}
			}.execute();
		}
		else {
			stack.closeToRoot();
			HsruWindow.showErrorMessage("Unexpected error: flow in invalid state");
		}
	}

	private void updateSelection() {
		if(!processing && bookingForm != null && tripCandidateList != null) {
			detailsTableModel.setRowCount(0);
			int outboundIndex = outboundTable.getSelectedRow();
			if(outboundIndex != -1) {
				String[] row = new String[7];
				TripCandidate outboundCandidate = tripCandidateList.getOutbound().get(outboundIndex);
				row[0] = "去程";
				row[1] = DATE.format(bookingForm.outboundDate);
				row[2] = outboundCandidate.getTrainNumber();
				row[3] = bookingForm.getFromStation().getNameZh();
				row[4] = bookingForm.getToStation().getNameZh();
				row[5] = outboundCandidate.getDepartureTime();
				row[6] = outboundCandidate.getDestinationTime();
				detailsTableModel.addRow(row);
			}
			int inboundIndex = inboundTable.getSelectedRow();
			if(inboundIndex != -1) {
				String[] row = new String[7];
				TripCandidate inboundCandidate = tripCandidateList.getInbound().get(inboundIndex);
				row[0] = "回程";
				row[1] = DATE.format(bookingForm.inboundDate);
				row[2] = inboundCandidate.getTrainNumber();
				row[3] = bookingForm.getFromStation().getNameZh();
				row[4] = bookingForm.getToStation().getNameZh();
				row[5] = inboundCandidate.getDepartureTime();
				row[6] = inboundCandidate.getDestinationTime();
				detailsTableModel.addRow(row);
			}
		}
		selectButton.setEnabled(canSelect());
	}

	private boolean canSelect() {
		return !processing && outboundTable.getSelectedRow() != -1 && (!roundTrip || inboundTable.getSelectedRow() != -1);
	}
}
