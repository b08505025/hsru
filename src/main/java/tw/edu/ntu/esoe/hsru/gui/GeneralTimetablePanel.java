package tw.edu.ntu.esoe.hsru.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridLayout;
import java.util.Arrays;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import tw.edu.ntu.esoe.hsru.data.Station;
import tw.edu.ntu.esoe.hsru.userdata.Settings;
import tw.edu.ntu.esoe.hsru.web.data.timetable.StopTime;
import tw.edu.ntu.esoe.hsru.web.data.timetable.TrainInfo;
import tw.edu.ntu.esoe.hsru.web.data.timetable.general.GeneralTimetableEntry;
import tw.edu.ntu.esoe.hsru.web.data.timetable.general.GeneralTimetableInfo;
import tw.edu.ntu.esoe.hsru.web.data.timetable.general.GeneralTimetableRequest;

/**
 * The general timetable panel, replicating the official THSR timetable format.
 */
public class GeneralTimetablePanel extends JPanel {

	public static final Settings SETTINGS = HsruWindow.SETTINGS;

	public static final Color GAINSBORO = new Color(0xDDDDDD);
	public static final Color ANTI_FLASH_WHITE = new Color(0xF2F3F4);

	public static String[] southboundColumns = {
			"車次", "行駛日", "南港", "台北", "板橋", "桃園", "新竹", "苗栗", "台中", "彰化", "雲林", "嘉義", "台南", "左營"
	};
	public static String[] northboundColumns = {
			"車次", "行駛日", "左營", "台南", "嘉義", "雲林", "彰化", "台中", "苗栗", "新竹", "桃園", "板橋", "台北", "南港"
	};

	private final DefaultTableModel southboundTableModel;
	private final JTable southboundTable;
	private final JScrollPane southboundScrollPane;
	private final DefaultTableModel northboundTableModel;
	private final JTable northboundTable;
	private final JScrollPane northboundScrollPane;
	private final JTabbedPane tabs;

	GeneralTimetablePanel() {
		super();

		setLayout(new GridLayout(1, 1));

		southboundTableModel = new ReadOnlyTableModel();
		southboundTableModel.setDataVector(new Object[][]{}, southboundColumns);

		northboundTableModel = new ReadOnlyTableModel();
		northboundTableModel.setDataVector(new Object[][]{}, northboundColumns);

		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer.UIResource() {
			@Override
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
				setBackground(column == 0 ? GAINSBORO : !table.getModel().getValueAt(row, 1).equals("全") ? ANTI_FLASH_WHITE : Color.WHITE);
				return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			}
		};
		centerRenderer.setHorizontalAlignment(SwingConstants.CENTER);

		southboundTable = new JTable(southboundTableModel);
		southboundTable.setDefaultRenderer(Object.class, centerRenderer);
		southboundTable.getTableHeader().setReorderingAllowed(false);
		southboundTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		northboundTable = new JTable(northboundTableModel);
		northboundTable.setDefaultRenderer(Object.class, centerRenderer);
		northboundTable.getTableHeader().setReorderingAllowed(false);
		northboundTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		southboundScrollPane = new JScrollPane(southboundTable);
		northboundScrollPane = new JScrollPane(northboundTable);

		tabs = new JTabbedPane();
		tabs.addTab("南下", southboundScrollPane);
		tabs.addTab("北上", northboundScrollPane);
		add(tabs);

		new SwingWorker<List<GeneralTimetableEntry>, Void>() {
			@Override
			protected List<GeneralTimetableEntry> doInBackground() throws Exception {
				return GeneralTimetableRequest.getTimetableEntries();
			}
			@Override
			protected void done() {
				try {
					updateTable(get());
				}
				catch(Exception e) {
					HsruWindow.showErrorMessage(e);
				}
			}
		}.execute();
	}

	private void updateTable(List<GeneralTimetableEntry> timetable) {
		String[][] data;

		data = timetable.stream().
				filter(entry->entry.getGeneralTimetable().getGeneralTrainInfo().getDirection() == 0).
				sorted().
				map(this::getSouthboundRow).
				toArray(String[][]::new);
		southboundTableModel.setDataVector(data, southboundColumns);

		data = timetable.stream().
				filter(entry->entry.getGeneralTimetable().getGeneralTrainInfo().getDirection() == 1).
				sorted().
				map(this::getNorthboundRow).
				toArray(String[][]::new);
		northboundTableModel.setDataVector(data, northboundColumns);
	}

	private String[] getSouthboundRow(GeneralTimetableEntry timetableEntry) {
		String[] arr = new String[14];
		Arrays.fill(arr, "");
		GeneralTimetableInfo ttInfo = timetableEntry.getGeneralTimetable();
		TrainInfo trainInfo = ttInfo.getGeneralTrainInfo();
		arr[0] = trainInfo.getTrainNo();
		arr[1] = ttInfo.getServiceDay().getDesc();
		List<StopTime> stopTimes = ttInfo.getStopTimes();
		int i = 0;
		boolean started = false;
		for(StopTime stopTime : stopTimes) {
			while(i < 12) {
				Station station = Station.SOUTHBOUND[i];
				if(station.getStationID().equals(stopTime.getStationID())) {
					arr[i+2] = stopTime.getDepartureTime();
					started = true;
					++i;
					break;
				}
				else if(started) {
					arr[i+2] = "─";
				}
				++i;
			}
		}
		return arr;
	}

	private String[] getNorthboundRow(GeneralTimetableEntry timetableEntry) {
		String[] arr = new String[14];
		Arrays.fill(arr, "");
		GeneralTimetableInfo ttInfo = timetableEntry.getGeneralTimetable();
		TrainInfo trainInfo = ttInfo.getGeneralTrainInfo();
		arr[0] = trainInfo.getTrainNo();
		arr[1] = ttInfo.getServiceDay().getDesc();
		List<StopTime> stopTimes = ttInfo.getStopTimes();
		int i = 0;
		boolean started = false;
		for(StopTime stopTime : stopTimes) {
			while(i < 12) {
				Station station = Station.NORTHBOUND[i];
				if(station.getStationID().equals(stopTime.getStationID())) {
					arr[i+2] = stopTime.getDepartureTime();
					started = true;
					++i;
					break;
				}
				else if(started) {
					arr[i+2] = "─";
				}
				++i;
			}
		}
		return arr;
	}
}
