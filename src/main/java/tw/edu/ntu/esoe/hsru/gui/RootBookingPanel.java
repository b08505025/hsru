package tw.edu.ntu.esoe.hsru.gui;

import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.image.BufferedImage;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingWorker;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import com.github.lgooddatepicker.components.DatePicker;
import com.github.lgooddatepicker.components.TimePicker;
import com.github.lgooddatepicker.optionalusertools.DateChangeListener;
import com.github.lgooddatepicker.optionalusertools.TimeChangeListener;
import com.github.lgooddatepicker.zinternaltools.DateChangeEvent;
import com.github.lgooddatepicker.zinternaltools.TimeChangeEvent;

import tw.edu.ntu.esoe.hsru.data.Station;
import tw.edu.ntu.esoe.hsru.ticket.BookingForm;
import tw.edu.ntu.esoe.hsru.ticket.CarType;
import tw.edu.ntu.esoe.hsru.ticket.Seat;
import tw.edu.ntu.esoe.hsru.userdata.Settings;
import tw.edu.ntu.esoe.hsru.web.irs.BookingFlow;

/**
 * The root ticket booking panel, replicating the booking tab on THSR's booking website.
 */
public class RootBookingPanel extends JPanel {

	public static final Settings SETTINGS = HsruWindow.SETTINGS;

	public static final DateTimeFormatter DATE = DateTimeFormatter.ofPattern("yyyy/MM/dd");
	public static final DateTimeFormatter TIME = DateTimeFormatter.ofPattern("HH:mm");

	public static String[] stationNames = {
			"南港", "台北", "板橋", "桃園", "新竹", "苗栗", "台中", "彰化", "雲林", "嘉義", "台南", "左營"
	};
	public static String[] ticketCounts = {
			"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"
	};

	private final StackedPanel stack;
	private final VerifyBookingPanel verify;

	private final JComboBox fromStationComboBox;
	private final JLabel fromStationLabel;
	private final JButton swapButton;
	private final JComboBox toStationComboBox;
	private final JLabel toStationLabel;
	private final JLabel stationLabel;
	private final JRadioButton standardRadioButton;
	private final JRadioButton businessRadioButton;
	private final ButtonGroup carTypeButtonGroup;
	private final JLabel carTypeLabel;
	private final JRadioButton noneRadioButton;
	private final JRadioButton windowRadioButton;
	private final JRadioButton aisleRadioButton;
	private final ButtonGroup seatTypeButtonGroup;
	private final JLabel seatTypeLabel;
	private final JRadioButton timeRadioButton;
	private final JRadioButton trainNoRadioButton;
	private final ButtonGroup searchTypeButtonGroup;
	private final JLabel searchTypeLabel;
	private final DatePicker outboundDatePicker;
	private final JLabel outboundDateLabel;
	private final TimePicker outboundTimePicker;
	private final JLabel outboundTimeLabel;
	private final JTextField outboundTrainNoField;
	private final JLabel outboundTrainNoLabel;
	private final JCheckBox roundTripCheckBox;
	private final DatePicker inboundDatePicker;
	private final JLabel inboundDateLabel;
	private final TimePicker inboundTimePicker;
	private final JLabel inboundTimeLabel;
	private final JTextField inboundTrainNoField;
	private final JLabel inboundTrainNoLabel;
	private final JLabel timeLabel;
	private final JComboBox adultComboBox;
	private final JLabel adultLabel;
	private final JComboBox childComboBox;
	private final JLabel childLabel;
	private final JComboBox disabledComboBox;
	private final JLabel disabledLabel;
	private final JComboBox seniorComboBox;
	private final JLabel seniorLabel;
	private final JComboBox studentComboBox;
	private final JLabel studentLabel;
	private final JLabel ticketLabel;
	private final JCheckBox earlyBirdCheckBox;
	private final JLabel discountLabel;
	private final JButton searchButton;

	private boolean searching = false;

	RootBookingPanel(StackedPanel stack) {
		super();
		this.stack = stack;

		setLayout(null);

		verify = new VerifyBookingPanel(stack);

		ActionListener canSetSeatListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				boolean enable = getTicketCount() <= 1;
				noneRadioButton.setEnabled(enable);
				windowRadioButton.setEnabled(enable);
				aisleRadioButton.setEnabled(enable);
			}
		};

		ActionListener canSearchListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				searchButton.setEnabled(canSearch());
			}
		};

		DocumentListener canSearchDocumentListener = new DocumentListener() {
			@Override
			public void insertUpdate(DocumentEvent e) {
				searchButton.setEnabled(canSearch());
			}
			@Override
			public void removeUpdate(DocumentEvent e) {
				searchButton.setEnabled(canSearch());
			}
			@Override
			public void changedUpdate(DocumentEvent e) {}
		};

		fromStationComboBox = new JComboBox(stationNames);
		fromStationComboBox.setEditable(false);
		fromStationComboBox.setSelectedIndex(0);
		fromStationComboBox.addActionListener(canSearchListener);
		add(fromStationComboBox);

		fromStationLabel = new JLabel("起程站");
		add(fromStationLabel);

		swapButton = new JButton("⇄");
		swapButton.setMargin(new Insets(0, 0, 0, 0));
		swapButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int fromIndex = fromStationComboBox.getSelectedIndex();
				int toIndex = toStationComboBox.getSelectedIndex();
				fromStationComboBox.setSelectedIndex(toIndex);
				toStationComboBox.setSelectedIndex(fromIndex);
			}
		});
		add(swapButton);

		toStationComboBox = new JComboBox(stationNames);
		toStationComboBox.setEditable(false);
		toStationComboBox.setSelectedIndex(11);
		toStationComboBox.addActionListener(canSearchListener);
		add(toStationComboBox);

		toStationLabel = new JLabel("到達站");
		add(toStationLabel);

		stationLabel = new JLabel("起訖站");
		add(stationLabel);

		standardRadioButton = new JRadioButton("標準車廂", true);
		add(standardRadioButton);

		businessRadioButton = new JRadioButton("商務車廂");
		businessRadioButton.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				studentComboBox.setEnabled(!businessRadioButton.isSelected());
				earlyBirdCheckBox.setEnabled(isEarlyBirdAvailable());
			}
		});
		add(businessRadioButton);

		carTypeButtonGroup = new ButtonGroup();
		carTypeButtonGroup.add(standardRadioButton);
		carTypeButtonGroup.add(businessRadioButton);

		carTypeLabel = new JLabel("車廂種類");
		add(carTypeLabel);

		noneRadioButton = new JRadioButton("無", true);
		add(noneRadioButton);

		windowRadioButton = new JRadioButton("靠窗優先");
		add(windowRadioButton);

		aisleRadioButton = new JRadioButton("走道優先");
		add(aisleRadioButton);

		seatTypeButtonGroup = new ButtonGroup();
		seatTypeButtonGroup.add(noneRadioButton);
		seatTypeButtonGroup.add(windowRadioButton);
		seatTypeButtonGroup.add(aisleRadioButton);

		seatTypeLabel = new JLabel("座位喜好");
		add(seatTypeLabel);

		timeRadioButton = new JRadioButton("依時間搜尋合適車次", true);
		add(timeRadioButton);

		trainNoRadioButton = new JRadioButton("直接輸入車次號碼");
		trainNoRadioButton.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if(trainNoRadioButton.isSelected()) {
					outboundTimePicker.setVisible(false);
					outboundTimeLabel.setVisible(false);
					outboundTrainNoField.setVisible(true);
					outboundTrainNoLabel.setVisible(true);
					if(roundTripCheckBox.isSelected()) {
						inboundTimePicker.setVisible(false);
						inboundTimeLabel.setVisible(false);
						inboundTrainNoField.setVisible(true);
						inboundTrainNoLabel.setVisible(true);
					}
				}
				else {
					outboundTimePicker.setVisible(true);
					outboundTimeLabel.setVisible(true);
					outboundTrainNoField.setVisible(false);
					outboundTrainNoLabel.setVisible(false);
					if(roundTripCheckBox.isSelected()) {
						inboundTimePicker.setVisible(true);
						inboundTimeLabel.setVisible(true);
						inboundTrainNoField.setVisible(false);
						inboundTrainNoLabel.setVisible(false);
					}
				}
				earlyBirdCheckBox.setEnabled(isEarlyBirdAvailable());
				searchButton.setEnabled(canSearch());
			}
		});
		add(trainNoRadioButton);

		searchTypeButtonGroup = new ButtonGroup();
		searchTypeButtonGroup.add(timeRadioButton);
		searchTypeButtonGroup.add(trainNoRadioButton);

		searchTypeLabel = new JLabel("訂位方式");
		add(searchTypeLabel);

		outboundDatePicker = new DatePicker();
		outboundDatePicker.getSettings().setSizeTextFieldMinimumWidth(0);
		outboundDatePicker.getSettings().setSizeTextFieldMinimumWidthDefaultOverride(false);
		outboundDatePicker.getSettings().setFormatForDatesCommonEra(DATE);
		outboundDatePicker.getSettings().setFormatForTodayButton(DATE);
		outboundDatePicker.getSettings().setAllowEmptyDates(false);
		outboundDatePicker.getSettings().setVetoPolicy(this::isOutboundDateAllowed);
		outboundDatePicker.setDateToToday();
		outboundDatePicker.addDateChangeListener(new DateChangeListener() {
			@Override
			public void dateChanged(DateChangeEvent e) {
				if(!isInboundDateAllowed(inboundDatePicker.getDate())) {
					inboundDatePicker.setDate(outboundDatePicker.getDate());
				}
				if(!isInboundTimeAllowed(inboundTimePicker.getTime())) {
					inboundTimePicker.setTime(nextTwoHalfHours(outboundTimePicker.getTime().minusMinutes(30)));
				}
				earlyBirdCheckBox.setEnabled(isEarlyBirdAvailable());
			}
		});
		add(outboundDatePicker);

		outboundDateLabel = new JLabel("去程日期");
		add(outboundDateLabel);

		outboundTimePicker = new TimePicker();
		outboundTimePicker.getSettings().setSizeTextFieldMinimumWidth(0);
		outboundTimePicker.getSettings().setSizeTextFieldMinimumWidthDefaultOverride(false);
		outboundTimePicker.getSettings().setFormatForDisplayTime(TIME);
		outboundTimePicker.getSettings().setFormatForMenuTimes(TIME);
		outboundTimePicker.getSettings().setAllowEmptyTimes(false);
		outboundTimePicker.getSettings().setVetoPolicy(this::isOutboundTimeAllowed);
		outboundTimePicker.setTime(nextHalfHour(LocalTime.now()));
		outboundTimePicker.addTimeChangeListener(new TimeChangeListener() {
			@Override
			public void timeChanged(TimeChangeEvent e) {
				if(!isInboundTimeAllowed(inboundTimePicker.getTime())) {
					inboundTimePicker.setTime(nextTwoHalfHours(outboundTimePicker.getTime().minusMinutes(30)));
				}
			}
		});
		add(outboundTimePicker);

		outboundTimeLabel = new JLabel("去程時刻");
		add(outboundTimeLabel);

		outboundTrainNoField = new JTextField();
		outboundTrainNoField.getDocument().addDocumentListener(canSearchDocumentListener);
		add(outboundTrainNoField);
		outboundTrainNoField.setVisible(false);

		outboundTrainNoLabel = new JLabel("去程車次");
		add(outboundTrainNoLabel);
		outboundTrainNoLabel.setVisible(false);

		roundTripCheckBox = new JCheckBox("訂購回程");
		roundTripCheckBox.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if(roundTripCheckBox.isSelected()) {
					inboundDatePicker.setVisible(true);
					inboundDateLabel.setVisible(true);
					if(trainNoRadioButton.isSelected()) {
						inboundTrainNoField.setVisible(true);
						inboundTrainNoLabel.setVisible(true);
					}
					else {
						inboundTimePicker.setVisible(true);
						inboundTimeLabel.setVisible(true);
					}
				}
				else {
					inboundDatePicker.setVisible(false);
					inboundDateLabel.setVisible(false);
					inboundTimePicker.setVisible(false);
					inboundTimeLabel.setVisible(false);
					inboundTrainNoField.setVisible(false);
					inboundTrainNoLabel.setVisible(false);
				}
				searchButton.setEnabled(canSearch());
				validate();
			}
		});
		add(roundTripCheckBox);

		inboundDatePicker = new DatePicker();
		inboundDatePicker.getSettings().setSizeTextFieldMinimumWidth(0);
		inboundDatePicker.getSettings().setSizeTextFieldMinimumWidthDefaultOverride(false);
		inboundDatePicker.getSettings().setFormatForDatesCommonEra(DATE);
		inboundDatePicker.getSettings().setFormatForTodayButton(DATE);
		inboundDatePicker.getSettings().setAllowEmptyDates(false);
		inboundDatePicker.getSettings().setVetoPolicy(this::isInboundDateAllowed);
		inboundDatePicker.setDateToToday();
		add(inboundDatePicker);
		inboundDatePicker.setVisible(false);

		inboundDateLabel = new JLabel("回程日期");
		add(inboundDateLabel);
		inboundDateLabel.setVisible(false);

		inboundTimePicker = new TimePicker();
		inboundTimePicker.getSettings().setSizeTextFieldMinimumWidth(0);
		inboundTimePicker.getSettings().setSizeTextFieldMinimumWidthDefaultOverride(false);
		inboundTimePicker.getSettings().setFormatForDisplayTime(TIME);
		inboundTimePicker.getSettings().setFormatForMenuTimes(TIME);
		inboundTimePicker.getSettings().setAllowEmptyTimes(false);
		inboundTimePicker.getSettings().setVetoPolicy(this::isInboundTimeAllowed);
		inboundTimePicker.setTime(nextTwoHalfHours(LocalTime.now()));
		add(inboundTimePicker);
		inboundTimePicker.setVisible(false);

		inboundTimeLabel = new JLabel("回程時刻");
		add(inboundTimeLabel);
		inboundTimeLabel.setVisible(false);

		inboundTrainNoField = new JTextField();
		inboundTrainNoField.getDocument().addDocumentListener(canSearchDocumentListener);
		add(inboundTrainNoField);
		inboundTrainNoField.setVisible(false);

		inboundTrainNoLabel = new JLabel("回程車次");
		add(inboundTrainNoLabel);
		inboundTrainNoLabel.setVisible(false);

		timeLabel = new JLabel("時間");
		add(timeLabel);

		adultComboBox = new JComboBox(ticketCounts);
		adultComboBox.setEditable(false);
		adultComboBox.setSelectedIndex(1);
		adultComboBox.addActionListener(canSearchListener);
		adultComboBox.addActionListener(canSetSeatListener);
		add(adultComboBox);

		adultLabel = new JLabel("全票");
		add(adultLabel);

		childComboBox = new JComboBox(ticketCounts);
		childComboBox.setEditable(false);
		childComboBox.setSelectedIndex(0);
		childComboBox.addActionListener(canSearchListener);
		childComboBox.addActionListener(canSetSeatListener);
		add(childComboBox);

		childLabel = new JLabel("孩童票");
		add(childLabel);

		disabledComboBox = new JComboBox(ticketCounts);
		disabledComboBox.setEditable(false);
		disabledComboBox.setSelectedIndex(0);
		disabledComboBox.addActionListener(canSearchListener);
		disabledComboBox.addActionListener(canSetSeatListener);
		add(disabledComboBox);

		disabledLabel = new JLabel("愛心票");
		add(disabledLabel);

		seniorComboBox = new JComboBox(ticketCounts);
		seniorComboBox.setEditable(false);
		seniorComboBox.setSelectedIndex(0);
		seniorComboBox.addActionListener(canSearchListener);
		seniorComboBox.addActionListener(canSetSeatListener);
		add(seniorComboBox);

		seniorLabel = new JLabel("敬老票");
		add(seniorLabel);

		studentComboBox = new JComboBox(ticketCounts);
		studentComboBox.setEditable(false);
		studentComboBox.setSelectedIndex(0);
		studentComboBox.addActionListener(canSearchListener);
		studentComboBox.addActionListener(canSetSeatListener);
		add(studentComboBox);

		studentLabel = new JLabel("大學生優惠票");
		add(studentLabel);

		ticketLabel = new JLabel("票數");
		add(ticketLabel);

		earlyBirdCheckBox = new JCheckBox("僅顯示尚有早鳥優惠之車次");
		add(earlyBirdCheckBox);
		earlyBirdCheckBox.setEnabled(false);

		discountLabel = new JLabel("查詢早鳥優惠");
		add(discountLabel);

		searchButton = new JButton("開始查詢");
		searchButton.setMargin(new Insets(0, 0, 0, 0));
		searchButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				search();
			}
		});
		add(searchButton);

		stack.openPane(this);
	}

	@Override
	public void doLayout() {
		int s0 = getWidth()/2-200;
		int s1 = s0+100;
		int t0 = 20;
		fromStationComboBox.setBounds(s1, t0+20, 70, 20);
		fromStationLabel.setBounds(s1, t0, 70, 20);
		swapButton.setBounds(s1+75, t0+20, 20, 20);
		toStationComboBox.setBounds(s1+100, t0+20, 70, 20);
		toStationLabel.setBounds(s1+100, t0, 70, 20);
		stationLabel.setBounds(s0, t0+10, 100, 20);
		standardRadioButton.setBounds(s1, t0+50, 80, 20);
		businessRadioButton.setBounds(s1+80, t0+50, 80, 20);
		carTypeLabel.setBounds(s0, t0+50, 100, 20);
		noneRadioButton.setBounds(s1, t0+80, 40, 20);
		windowRadioButton.setBounds(s1+40, t0+80, 80, 20);
		aisleRadioButton.setBounds(s1+120, t0+80, 80, 20);
		seatTypeLabel.setBounds(s0, t0+80, 100, 20);
		timeRadioButton.setBounds(s1, t0+110, 150, 20);
		trainNoRadioButton.setBounds(s1+150, t0+110, 130, 20);
		searchTypeLabel.setBounds(s0, t0+110, 100, 20);
		outboundDatePicker.setBounds(s1, t0+160, 105, 20);
		outboundDateLabel.setBounds(s1, t0+140, 105, 20);
		outboundTimePicker.setBounds(s1+110, t0+160, 70, 20);
		outboundTimeLabel.setBounds(s1+110, t0+140, 70, 20);
		outboundTrainNoField.setBounds(s1+110, t0+160, 70, 20);
		outboundTrainNoLabel.setBounds(s1+110, t0+140, 70, 20);
		roundTripCheckBox.setBounds(s1+185, t0+160, 80, 20);
		inboundDatePicker.setBounds(s1, t0+200, 105, 20);
		inboundDateLabel.setBounds(s1, t0+180, 105, 20);
		inboundTimePicker.setBounds(s1+110, t0+200, 70, 20);
		inboundTimeLabel.setBounds(s1+110, t0+180, 70, 20);
		inboundTrainNoField.setBounds(s1+110, t0+200, 70, 20);
		inboundTrainNoLabel.setBounds(s1+110, t0+180, 70, 20);
		timeLabel.setBounds(s0, t0+150, 100, 20);
		int t1 = t0+190+(roundTripCheckBox.isSelected() ? 40 : 0);
		adultComboBox.setBounds(s1, t1+20, 50, 20);
		adultLabel.setBounds(s1, t1, 50, 20);
		childComboBox.setBounds(s1+55, t1+20, 50, 20);
		childLabel.setBounds(s1+55, t1, 50, 20);
		disabledComboBox.setBounds(s1+110, t1+20, 50, 20);
		disabledLabel.setBounds(s1+110, t1, 50, 20);
		seniorComboBox.setBounds(s1+165, t1+20, 50, 20);
		seniorLabel.setBounds(s1+165, t1, 50, 20);
		studentComboBox.setBounds(s1+220, t1+20, 50, 20);
		studentLabel.setBounds(s1+220, t1, 80, 20);
		ticketLabel.setBounds(s0, t1+10, 100, 20);
		earlyBirdCheckBox.setBounds(s1, t1+50, 200, 20);
		discountLabel.setBounds(s0, t1+50, 100, 20);
		searchButton.setBounds(getWidth()/2-40, t1+80, 80, 20);
	}

	private void search() {
		searching = true;
		searchButton.setEnabled(false);

		BookingForm bookingForm = new BookingForm(
				Station.values()[fromStationComboBox.getSelectedIndex()],
				Station.values()[toStationComboBox.getSelectedIndex()]).
				setCarType(standardRadioButton.isSelected() ? CarType.STANDARD : CarType.BUSINESS).
				setSeatPreference(getTicketCount() <= 1 ? getSeatPreference() : Seat.Alignment.NONE).
				setSpecTrainNo(trainNoRadioButton.isSelected()).
				setOutboundDate(outboundDatePicker.getDate()).
				setOutboundTime(outboundTimePicker.getTime()).
				setOutboundTrainNo(outboundTrainNoField.getText()).
				setRoundTrip(roundTripCheckBox.isSelected()).
				setInboundDate(inboundDatePicker.getDate()).
				setInboundTime(inboundTimePicker.getTime()).
				setInboundTrainNo(inboundTrainNoField.getText()).
				setAdultCount(adultComboBox.getSelectedIndex()).
				setChildCount(childComboBox.getSelectedIndex()).
				setDisabledCount(disabledComboBox.getSelectedIndex()).
				setSeniorCount(seniorComboBox.getSelectedIndex()).
				setStudentCount(standardRadioButton.isSelected() ? studentComboBox.getSelectedIndex() : 0).
				setEarlyBirdOnly(isEarlyBirdAvailable() && earlyBirdCheckBox.isSelected());
		BookingFlow bookingFlow = new BookingFlow();

		new SwingWorker<BufferedImage, Void>() {
			@Override
			protected BufferedImage doInBackground() throws Exception {
				return bookingFlow.connect().getVeriCodeImage();
			}
			@Override
			protected void done() {
				try {
					BufferedImage veriCodeImage = get();
					verify.setData(bookingForm, bookingFlow, veriCodeImage);
					stack.openPane(verify);
				}
				catch(Exception e) {
					HsruWindow.showErrorMessage(e);
				}
				searching = false;
				searchButton.setEnabled(canSearch());
			}
		}.execute();
	}

	private boolean canSearch() {
		int fromIndex = fromStationComboBox.getSelectedIndex();
		int toIndex = toStationComboBox.getSelectedIndex();
		int ticketCount = getTicketCount();
		if(roundTripCheckBox.isSelected()) {
			ticketCount *= 2;
		}
		return !searching &&
				fromIndex != toIndex &&
				ticketCount > 0 && ticketCount <= 10 &&
				(!outboundTrainNoField.isVisible() ||
						isTrainNoValid(outboundTrainNoField.getText(), fromIndex, toIndex)) &&
				(!inboundTrainNoField.isVisible() ||
						isTrainNoValid(inboundTrainNoField.getText(), toIndex, fromIndex));
	}

	private boolean isEarlyBirdAvailable() {
		return !businessRadioButton.isSelected() &&
				!trainNoRadioButton.isSelected() &&
				adultComboBox.getSelectedIndex() > 0 &&
				outboundDatePicker.getDate().toEpochDay()-LocalDate.now().toEpochDay() >= 4;
	}

	private int getTicketCount() {
		return adultComboBox.getSelectedIndex() +
				childComboBox.getSelectedIndex() +
				disabledComboBox.getSelectedIndex() +
				seniorComboBox.getSelectedIndex() +
				(standardRadioButton.isSelected() ? studentComboBox.getSelectedIndex() : 0);
	}

	private boolean isTrainNoValid(String trainNo, int fromIndex, int toIndex) {
		if(fromIndex < toIndex) {
			return trainNo.matches("(0|1(?=[2356]))?[123568]\\d[13579]");
		}
		else {
			return trainNo.matches("(0|1(?=[2356]))?[123568]\\d[02468]");
		}
	}

	private boolean isOutboundDateAllowed(LocalDate date) {
		long diff = date.toEpochDay()-LocalDate.now().toEpochDay();
		return diff >= 0 && diff <= 28;
	}

	private boolean isOutboundTimeAllowed(LocalTime time) {
		return time.getHour() >= 5 && time.getMinute() % 30 == 0;
	}

	private boolean isInboundDateAllowed(LocalDate date) {
		long diff = date.toEpochDay()-LocalDate.now().toEpochDay();
		long diff1 = outboundDatePicker.getDate().toEpochDay()-LocalDate.now().toEpochDay();
		return diff >= diff1 && diff <= 28;
	}

	private boolean isInboundTimeAllowed(LocalTime time) {
		if(outboundDatePicker.getDate().isEqual(inboundDatePicker.getDate())) {
			return !time.isBefore(outboundTimePicker.getTime());
		}
		return time.getHour() >= 5 && time.getMinute() % 30 == 0;
	}

	private LocalTime nextHalfHour(LocalTime time) {
		if(time.getHour() < 5) {
			return LocalTime.of(5, 0);
		}
		time = time.truncatedTo(ChronoUnit.HOURS).
				plusMinutes(time.getMinute() < 30 ? 30 : 60);
		if(time.getHour() < 5) {
			return LocalTime.of(23, 00);
		}
		return time;
	}

	private LocalTime nextTwoHalfHours(LocalTime time) {
		if(time.getHour() < 5) {
			return LocalTime.of(5, 30);
		}
		time = time.truncatedTo(ChronoUnit.HOURS).
				plusMinutes(time.getMinute() < 30 ? 60 : 90);
		if(time.getHour() < 5) {
			return LocalTime.of(23, 30);
		}
		return time;
	}

	private Seat.Alignment getSeatPreference() {
		return windowRadioButton.isSelected() ? Seat.Alignment.WINDOW :
			aisleRadioButton.isSelected() ? Seat.Alignment.AISLE : Seat.Alignment.NONE;
	}
}
