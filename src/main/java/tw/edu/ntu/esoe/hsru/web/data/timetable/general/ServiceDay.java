package tw.edu.ntu.esoe.hsru.web.data.timetable.general;

import java.util.ArrayList;
import java.util.List;

public class ServiceDay {

	private byte monday = 0;
	private byte tuesday = 0;
	private byte wednesday = 0;
	private byte thursday = 0;
	private byte friday = 0;
	private byte saturday = 0;
	private byte sunday = 0;

	public boolean monday() {
		return monday == 1;
	}

	public boolean tuesday() {
		return tuesday == 1;
	}

	public boolean wednesday() {
		return wednesday == 1;
	}

	public boolean thursday() {
		return thursday == 1;
	}

	public boolean friday() {
		return friday == 1;
	}

	public boolean saturday() {
		return saturday == 1;
	}

	public boolean sunday() {
		return sunday == 1;
	}

	public String getDesc() {
		boolean[] arr = {monday(), tuesday(), wednesday(), thursday(), friday(), saturday(), sunday()};
		boolean allTrue = true;
		for(boolean b : arr) {
			if(!b) {
				allTrue = false;
				break;
			}
		}
		if(allTrue) {
			return "全";
		}
		List<String> strings = new ArrayList<>();
		String temp = "";
		int startIndex = -1;
		for(int i = 0; i <= arr.length; ++i) {
			boolean b = i == arr.length ? false : arr[i];
			if(b && startIndex == -1) {
				startIndex = i;
				switch(startIndex) {
				case 0: temp = "一"; break;
				case 1: temp = "二"; break;
				case 2: temp = "三"; break;
				case 3: temp = "四"; break;
				case 4: temp = "五"; break;
				case 5: temp = "六"; break;
				case 6: temp = "日"; break;
				}
			}
			else if(!b && startIndex != -1) {
				int endIndex = i-1;
				if(startIndex != endIndex) {
					switch(endIndex) {
					case 1: temp += "~二"; break;
					case 2: temp += "~三"; break;
					case 3: temp += "~四"; break;
					case 4: temp += "~五"; break;
					case 5: temp += "~六"; break;
					case 6: temp += "~日"; break;
					}
				}
				startIndex = -1;
				strings.add(temp);
			}
		}	
		return String.join("、", strings);
	}

	@Override
	public String toString() {
		return "ServiceDay [monday=" + monday + ", tuesday=" + tuesday + ", wednesday=" + wednesday + ", thursday="
				+ thursday + ", friday=" + friday + ", saturday=" + saturday + ", sunday=" + sunday + "]";
	}
}
