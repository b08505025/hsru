package tw.edu.ntu.esoe.hsru.web.data.timetable.daily;

import java.util.Collections;
import java.util.List;

import tw.edu.ntu.esoe.hsru.web.data.timetable.StopTime;
import tw.edu.ntu.esoe.hsru.web.data.timetable.TrainInfo;

public class DailyTimetableEntry implements Comparable<DailyTimetableEntry> {

	private String trainDate = "";
	private TrainInfo dailyTrainInfo = new TrainInfo();
	private List<StopTime> stopTimes = Collections.singletonList(new StopTime());
	private String updateTime = "";
	private int versionID = -1;

	public String getTrainDate() {
		return trainDate;
	}

	public TrainInfo getDailyTrainInfo() {
		return dailyTrainInfo;
	}

	public List<StopTime> getStopTimes() {
		return stopTimes;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public int getVersionID() {
		return versionID;
	}

	@Override
	public int compareTo(DailyTimetableEntry o) {
		return stopTimes.get(0).compareTo(o.stopTimes.get(0));
	}

	@Override
	public String toString() {
		return "DailyTimetableEntry [trainDate=" + trainDate + ", dailyTrainInfo=" + dailyTrainInfo + ", stopTimes="
				+ stopTimes + ", updateTime=" + updateTime + ", versionID=" + versionID + "]";
	}
}
