package tw.edu.ntu.esoe.hsru.web.data.timetable.od;

public enum DiscountType {

	EARLY_BIRD     ("e1b4c4d9-98d7-4c8c-9834-e1d2528750f1"),
	COLLEGE_STUDENT("68d9fc7b-7330-44c2-962a-74bc47d2ee8a"),
	FIELD_TRIP     ("40863ff1-a16c-4da1-8af7-c1f8991627f3"),
	GROUP          ("9973b559-8279-4bf4-90be-601f7973a39f"),
	;

	public final String uuid;

	DiscountType(String uuid) {
		this.uuid = uuid;
	}

	public String getUUID() {
		return uuid;
	}

	public boolean isTypeOf(Discount discount) {
		return uuid.equalsIgnoreCase(discount.getId());
	}
}
