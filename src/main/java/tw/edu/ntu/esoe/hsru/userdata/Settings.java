package tw.edu.ntu.esoe.hsru.userdata;

import java.awt.Rectangle;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.PrintStream;
import java.lang.reflect.Type;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.InstanceCreator;

/**
 * The settings for the application.
 */
public class Settings {

	private final transient File settingsFile;
	private final transient InstanceCreator<Settings> creator;
	private final transient Gson gson;

	private Rectangle bounds = new Rectangle(100, 100, 800, 600);

	/**
	 * Constructs the settings from the specified file.
	 * @param settingsFile The file the settings should be saved to.
	 */
	public Settings(File settingsFile) {
		this.settingsFile = settingsFile;
		creator = new InstanceCreator<Settings>() {
			@Override
			public Settings createInstance(Type type) {
				return Settings.this;
			}
		};
		gson = new GsonBuilder().
				registerTypeAdapter(Settings.class, creator).
				setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).
				serializeNulls().
				setLenient().
				setPrettyPrinting().
				create();
	}

	/**
	 * Loads the settings from file.
	 * @return The settings for chaining.
	 */
	public Settings load() {
		try {
			if(settingsFile.exists()) {
				gson.fromJson(new BufferedReader(new FileReader(settingsFile)), Settings.class);
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return this;
	}

	/**
	 * Saves the settings to file.
	 */
	public void save() {
		try(PrintStream out = new PrintStream(new FileOutputStream(settingsFile))) {
			gson.toJson(this, out);
		}
		catch(FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @return The bounds the application should be in when starting up.
	 */
	public Rectangle getBounds() {
		return bounds;
	}

	/**
	 * Sets the bounds the application should be in on next startup.
	 * @param bounds The bounds
	 * @return The settings for chaining.
	 */
	public Settings setBounds(Rectangle bounds) {
		this.bounds = bounds;
		return this;
	}
}
