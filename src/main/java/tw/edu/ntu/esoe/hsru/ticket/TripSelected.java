package tw.edu.ntu.esoe.hsru.ticket;

import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import tw.edu.ntu.esoe.hsru.data.Station;

public class TripSelected {

	public static final DateTimeFormatter DATE = DateTimeFormatter.ofPattern("MM/dd");

	private boolean inbound = false;
	private LocalDate date = LocalDate.now();
	private String trainNumber = "";
	private Station fromStation = Station.NANGANG;
	private Station toStation = Station.ZUOYING;
	private String departureTime = "";
	private String destinationTime = "";
	private CarType carType = CarType.STANDARD;
	private DiscountType earlyBirdDiscount = DiscountType.NONE;
	private int adultCount = 0;
	private int concessionCount = 0;
	private DiscountType studentDiscount = DiscountType.NONE;
	private int studentCount = 0;

	public TripSelected() {}

	public TripSelected(boolean inbound, LocalDate date, String trainNumber, Station fromStation, Station toStation, String departureTime, String destinationTime,
			CarType carType, DiscountType earlyBirdDiscount, int adultCount, int concessionCount, DiscountType studentDiscount, int studentCount) {
		this.inbound = inbound;
		this.date = date;
		this.trainNumber = (trainNumber.length() == 3 ? "0" : "") + trainNumber;
		this.fromStation = fromStation;
		this.toStation = toStation;
		this.departureTime = departureTime;
		this.destinationTime = destinationTime;
		this.carType = carType;
		this.earlyBirdDiscount = earlyBirdDiscount;
		this.adultCount = adultCount;
		this.concessionCount = concessionCount;
		this.studentDiscount = studentDiscount;
		this.studentCount = studentCount;
	}

	public LocalDate getDate() {
		return date;
	}

	public String getTrainNumber() {
		return trainNumber;
	}

	public Station getFromStation() {
		return fromStation;
	}

	public Station getToStation() {
		return toStation;
	}

	public String getDepartureTime() {
		return departureTime;
	}

	public String getDestinationTime() {
		return destinationTime;
	}

	public DiscountType getEarlyBirdDiscount() {
		return earlyBirdDiscount;
	}

	public int getAdultCount() {
		return adultCount;
	}

	public int getAdultPrice() {
		if(earlyBirdDiscount != DiscountType.NONE) {
			return Prices.getDiscount(fromStation, toStation, earlyBirdDiscount);
		}
		else {
			return Prices.getAdult(fromStation, toStation, carType);
		}
	}

	public int getConcessionCount() {
		return concessionCount;
	}

	public int getConcessionPrice() {
		return Prices.getConcession(fromStation, toStation, carType);
	}

	public DiscountType getStudentDiscount() {
		return studentDiscount;
	}

	public int getStudentCount() {
		return studentCount;
	}

	public int getStudentPrice() {
		return Prices.getDiscount(fromStation, toStation, studentDiscount);
	}

	public int getTotalPrice() {
		return adultCount*getAdultPrice()+concessionCount*getConcessionPrice()+studentCount*getStudentPrice();
	}

	public String[] toDetailsRow() {
		NumberFormat format = NumberFormat.getInstance();
		List<String> ret = new ArrayList<>();
		ret.add(inbound ? "回程" : "去程");
		ret.add(DATE.format(date));
		ret.add(trainNumber);
		ret.add(fromStation.getNameZh());
		ret.add(toStation.getNameZh());
		ret.add(departureTime);
		ret.add(destinationTime);
		if(earlyBirdDiscount != DiscountType.NONE) {
			ret.add(earlyBirdDiscount.getNameZh()+" "+format.format(getAdultPrice())+"*"+adultCount);
		}
		else if(adultCount > 0) {
			ret.add(format.format(getAdultPrice())+"*"+adultCount);
		}
		else {
			ret.add("-");
		}
		if(concessionCount > 0) {
			ret.add(format.format(getConcessionPrice())+"*"+concessionCount);
		}
		else {
			ret.add("-");
		}
		if(studentCount > 0) {
			ret.add(studentDiscount.getNameZh()+" "+format.format(getStudentPrice())+"*"+studentCount);
		}
		ret.add("TWD "+format.format(getTotalPrice()));
		return ret.toArray(new String[ret.size()]);
	}
}
