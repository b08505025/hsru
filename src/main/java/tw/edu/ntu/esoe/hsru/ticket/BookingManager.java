package tw.edu.ntu.esoe.hsru.ticket;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.PrintStream;
import java.lang.reflect.Type;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.fatboyindustrial.gsonjavatime.Converters;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import tw.edu.ntu.esoe.hsru.data.Station;

public class BookingManager {

	public static final DateTimeFormatter DATE = DateTimeFormatter.ofPattern("yyyy/MM/dd");
	public static final DateTimeFormatter TIME = DateTimeFormatter.ofPattern("HH:mm");
	public static final Gson GSON = Converters.registerAll(new GsonBuilder()).
			registerTypeAdapter(Seat.class, Seat.Serializer.INSTANCE).
			setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).
			serializeNulls().
			setLenient().
			setPrettyPrinting().
			create();

	public static final Type BOOKING_LIST_TYPE = new TypeToken<List<Booking>>(){}.getType();
	public static final Type TICKET_SEGMENT_TYPE = new TypeToken<Map<String, Set<Integer>>>(){}.getType();

	public static final BookingManager INSTANCE = new BookingManager();

	private final Random random = new Random();

	// you know what i'll just use json here
	private List<Booking> bookings = new ArrayList<>();
	private Map<String, Set<Integer>> ticketSegments = new HashMap<>();

	private File bookingsFile = new File("bookings.json");
	private File ticketsFile = new File("tickets.json");

	private BookingManager() {
		try {
			if(bookingsFile.exists()) {
				bookings = GSON.fromJson(new BufferedReader(new FileReader(bookingsFile)), BOOKING_LIST_TYPE);
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		try {
			if(ticketsFile.exists()) {
				ticketSegments = GSON.fromJson(new BufferedReader(new FileReader(ticketsFile)), TICKET_SEGMENT_TYPE);
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}

	public Booking addBooking(BookingForm bookingForm, TripSelectedList tripSelectedList, String userId, String phone, String email) {
		int id = random.nextInt(100000000);
		Station fromStation = bookingForm.getFromStation();	
		Station toStation = bookingForm.getToStation();	
		CarType carType = bookingForm.getCarType();

		TripSelected outboundSel = tripSelectedList.getOutbound();
		Trip outbound = new Trip(id, userId, false, outboundSel.getDate(), outboundSel.getTrainNumber(), fromStation, toStation, carType,
				TIME.parse(outboundSel.getDepartureTime(), LocalTime::from), TIME.parse(outboundSel.getDestinationTime(), LocalTime::from),
				outboundSel.getEarlyBirdDiscount(), outboundSel.getStudentDiscount(), outboundSel.getTotalPrice(),
				getTickets(id, userId, bookingForm, fromStation, toStation, outboundSel.getDate(), outboundSel.getTrainNumber()));

		Trip inbound = null;

		if(bookingForm.isRoundTrip()) {
			TripSelected inboundSel = tripSelectedList.getInbound();
			inbound = new Trip(id, userId, true, inboundSel.getDate(), inboundSel.getTrainNumber(), toStation, fromStation, carType,
					TIME.parse(inboundSel.getDepartureTime(), LocalTime::from), TIME.parse(inboundSel.getDestinationTime(), LocalTime::from),
					inboundSel.getEarlyBirdDiscount(), inboundSel.getStudentDiscount(), inboundSel.getTotalPrice(),
					getTickets(id, userId, bookingForm, toStation, fromStation, inboundSel.getDate(), inboundSel.getTrainNumber()));
		}

		LocalDateTime departureTime = outbound.getDate().atTime(outbound.getDepartureTime());
		LocalDateTime payDeadline = getPayDeadline(departureTime);
		LocalDateTime modifyDeadline = getModifyDeadline(departureTime);

		Booking booking = new Booking(id, userId, phone, email, payDeadline, modifyDeadline, departureTime,
				fromStation, toStation, bookingForm.getCarType(), bookingForm.isRoundTrip(), outbound, inbound,
				bookingForm.getAdultCount(), bookingForm.getChildCount(), bookingForm.getDisabledCount(),
				bookingForm.getSeniorCount(), bookingForm.getStudentCount());

		registerBooking(booking);

		return booking;
	}

	public Booking searchBooking(String userId, String idString) {
		if(!idString.matches("\\d+")) {
			return null;
		}
		int id = Integer.parseInt(idString);
		return bookings.stream().filter(
				b->
				b.getUserId().equalsIgnoreCase(userId) &&
				b.getId() == id
				).findAny().orElse(null);
	}

	public Booking searchBooking(String userId, Station fromStation, Station toStation, LocalDate date, String trainNumber) {
		return bookings.stream().filter(
				b->
				b.getUserId().equalsIgnoreCase(userId) &&
				b.getOutbound().getDate().isEqual(date) &&
				b.getOutbound().getTrainNumber().equals(trainNumber) &&
				b.getFromStation() == fromStation && b.getToStation() == toStation
				).findAny().orElse(null);
	}

	public synchronized void removeBooking(Booking booking) {
		booking.removeAllTickets();
		bookings.remove(booking);
		save();
	}

	void removeTicket(Ticket ticket) {
		String trainId = DATE.format(ticket.getDate())+' '+ticket.getTrainNumber();
		Set<Integer> segments = getTripSegments(ticket.getFromStation(), ticket.getToStation()).
				map(s->getSegment(s, ticket.getSeat())).boxed().collect(Collectors.toSet());
		ticketSegments.computeIfPresent(trainId, (key, original)->{
			original.removeAll(segments);
			return original.isEmpty() ? null : original;
		});
	}

	void save() {
		try(PrintStream out = new PrintStream(new FileOutputStream(bookingsFile))) {
			GSON.toJson(bookings, out);
		}
		catch(FileNotFoundException e) {
			e.printStackTrace();
		}
		try(PrintStream out = new PrintStream(new FileOutputStream(ticketsFile))) {
			GSON.toJson(ticketSegments, out);
		}
		catch(FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	private List<Ticket> getTickets(int id, String userId, BookingForm bookingForm, Station from, Station to, LocalDate date, String trainNo) {
		List<Seat> seats = getSeats(bookingForm.getTotalCount(), bookingForm.getCarType(),
				bookingForm.getSeatPreference(), from, to, date, trainNo);
		List<Ticket> tickets = new ArrayList<>();
		int seatIndex = 0;
		for(int i = 0; i < bookingForm.getAdultCount(); ++i) {
			tickets.add(new Ticket(id, userId, Ticket.Type.ADULT, seats.get(seatIndex++), date, trainNo, from, to));
		}
		for(int i = 0; i < bookingForm.getChildCount(); ++i) {
			tickets.add(new Ticket(id, userId, Ticket.Type.CHILD, seats.get(seatIndex++), date, trainNo, from, to));
		}
		for(int i = 0; i < bookingForm.getDisabledCount(); ++i) {
			tickets.add(new Ticket(id, userId, Ticket.Type.DISABLED, seats.get(seatIndex++), date, trainNo, from, to));
		}
		for(int i = 0; i < bookingForm.getSeniorCount(); ++i) {
			tickets.add(new Ticket(id, userId, Ticket.Type.SENIOR, seats.get(seatIndex++), date, trainNo, from, to));
		}
		for(int i = 0; i < bookingForm.getStudentCount(); ++i) {
			tickets.add(new Ticket(id, userId, Ticket.Type.STUDENT, seats.get(seatIndex++), date, trainNo, from, to));
		}
		return tickets;
	}

	private List<Seat> getSeats(int count, CarType carType, Seat.Alignment alignment, Station from, Station to, LocalDate date, String trainNo) {
		String trainId = DATE.format(date)+' '+trainNo;
		Set<Integer> segments = ticketSegments.getOrDefault(trainId, Collections.emptySet());
		List<Seat> seats = Seat.getMatchingSeats(carType, alignment).
				filter(s->getTripSegments(from, to).map(ts->getSegment(ts, s)).
						noneMatch(segments::contains)).collect(Collectors.toList());
		// whatever
		return seats.subList(0, count);
	}

	private synchronized void registerBooking(Booking booking) {
		bookings.add(booking);
		Station from = booking.getFromStation();
		Station to = booking.getToStation();
		Trip trip = booking.getOutbound();
		String trainId = DATE.format(trip.getDate())+' '+trip.getTrainNumber();
		Set<Integer> segments = trip.getTickets().stream().flatMap(t->{
			return getTripSegments(from, to).map(s->getSegment(s, t.getSeat())).boxed();
		}).collect(Collectors.toSet());
		ticketSegments.merge(trainId, segments, (a, b)->{
			a.addAll(b);
			return a;
		});
		if(booking.isRoundTrip()) {
			trip = booking.getInbound();
			trainId = DATE.format(trip.getDate())+' '+trip.getTrainNumber();
			segments = trip.getTickets().stream().flatMap(t->{
				return getTripSegments(to, from).map(s->getSegment(s, t.getSeat())).boxed();
			}).collect(Collectors.toSet());
			ticketSegments.merge(trainId, segments, (a, b)->{
				a.addAll(b);
				return a;
			});
		}
		save();
	}

	private IntStream getTripSegments(Station from, Station to) {
		int fromId = from.ordinal();
		int toId = to.ordinal();
		return IntStream.range(Math.min(fromId, toId), Math.max(fromId, toId));
	}

	private int getSegment(int tripSegment, Seat seat) {
		return tripSegment * 10000 + seat.getId();
	}

	private LocalDateTime getPayDeadline(LocalDateTime outboundDepartureTime) {
		LocalDateTime now = LocalDateTime.now();
		long nowDay = now.getLong(ChronoField.EPOCH_DAY);
		long depDay = outboundDepartureTime.getLong(ChronoField.EPOCH_DAY);
		if(depDay == nowDay) {
			return outboundDepartureTime.minusMinutes(30);
		}
		else if(depDay - nowDay < 3) {
			return outboundDepartureTime.truncatedTo(ChronoUnit.DAYS);
		}
		else {
			return now.withHour(23).withMinute(59).withSecond(59).plusDays(2);
		}
	}

	private LocalDateTime getModifyDeadline(LocalDateTime outboundDepartureTime) {
		return outboundDepartureTime.minusMinutes(30);
	}
}
