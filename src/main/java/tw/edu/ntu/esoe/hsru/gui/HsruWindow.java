package tw.edu.ntu.esoe.hsru.gui;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;

import tw.edu.ntu.esoe.hsru.userdata.Settings;

/**
 * The HSRU main application window.
 */
public class HsruWindow extends JFrame {

	public static final int WINDOW_MIN_WIDTH = 600;
	public static final int WINDOW_MIN_HEIGHT = 450;

	public static final Settings SETTINGS = new Settings(new File("settings.json")).load();

	public static HsruWindow window;

	public static void createAndShowGui() {
		if(window != null) {
			return;
		}
		window = new HsruWindow();
		window.setVisible(true);
	}

	public final StationPanel station;
	public final ODTimetablePanel odTimetable;
	public final DailyTimetablePanel dailyTimetable;
	public final GeneralTimetablePanel generalTimetable;
	public final RootBookingPanel booking;
	public final RootModifyPanel modify;

	private final JTabbedPane tabs;
	private final JTabbedPane timetableTabs;
	private final StackedPanel bookingStack;
	private final StackedPanel modifyStack;

	private HsruWindow() {
		super();

		setTitle("HSRU");
		setMinimumSize(new Dimension(WINDOW_MIN_WIDTH, WINDOW_MIN_HEIGHT));
		setBounds(SETTINGS.getBounds());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		setLayout(new GridLayout(1, 1));
		addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				SETTINGS.setBounds(getBounds());
			}
			@Override
			public void componentMoved(ComponentEvent e) {
				SETTINGS.setBounds(getBounds());
			}
		});
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				SETTINGS.save();
			}
		});

		tabs = new JTabbedPane();
		tabs.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);

		timetableTabs = new JTabbedPane();
		timetableTabs.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);

		bookingStack = new StackedPanel();
		modifyStack = new StackedPanel();

		station = new StationPanel();

		generalTimetable = new GeneralTimetablePanel();
		odTimetable = new ODTimetablePanel();
		dailyTimetable = new DailyTimetablePanel();

		booking = new RootBookingPanel(bookingStack);
		modify = new RootModifyPanel(modifyStack);

		timetableTabs.addTab("時刻表與票價查詢", odTimetable);
		timetableTabs.addTab("全日時刻表查詢", dailyTimetable);
		timetableTabs.addTab("定期時刻表", generalTimetable);

		//tabs.addTab("車站資訊", station);
		tabs.addTab("時刻表", timetableTabs);
		tabs.addTab("訂票", bookingStack);
		tabs.addTab("訂位紀錄", modifyStack);

		add(tabs);
	}

	public static void showErrorMessage(Object e) {
		if(e instanceof Throwable) {
			((Throwable)e).printStackTrace();
		}
		JOptionPane.showMessageDialog(window, e, "Error", JOptionPane.ERROR_MESSAGE);
	}
}
