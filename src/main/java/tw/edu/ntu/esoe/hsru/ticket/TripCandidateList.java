package tw.edu.ntu.esoe.hsru.ticket;

import java.util.Collections;
import java.util.List;

public class TripCandidateList {

	private String[] outboundColumns = {};
	private List<TripCandidate> outbound = Collections.emptyList();
	private String[] inboundColumns = {};
	private List<TripCandidate> inbound = Collections.emptyList();

	public TripCandidateList() {}

	public TripCandidateList(String[] outboundColumns, List<TripCandidate> outbound, String[] inboundColumns, List<TripCandidate> inbound) {
		this.outboundColumns = outboundColumns.clone();
		this.outbound = Collections.unmodifiableList(outbound);
		this.inboundColumns = inboundColumns.clone();
		this.inbound = Collections.unmodifiableList(inbound);
	}

	public String[] getOutboundColumns() {
		return outboundColumns;
	}

	public List<TripCandidate> getOutbound() {
		return outbound;
	}

	public String[] getInboundColumns() {
		return inboundColumns;
	}

	public List<TripCandidate> getInbound() {
		return inbound;
	}
}
