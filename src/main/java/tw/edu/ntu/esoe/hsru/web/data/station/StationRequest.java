package tw.edu.ntu.esoe.hsru.web.data.station;

import java.io.IOException;
import java.lang.reflect.Type;
import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

import tw.edu.ntu.esoe.hsru.web.data.DataUtils;
import tw.edu.ntu.esoe.hsru.web.data.JsonUtils;
import tw.edu.ntu.esoe.hsru.web.data.ptx.PTXRequest;

public class StationRequest {

	public static final Type RAIL_STATION_LIST_TYPE = new TypeToken<List<RailStation>>(){}.getType();
	/**
	 * 10 days in milliseconds.
	 */
	public static final long UPDATE_INTERVAL = 10*24*60*60*1000;

	/**
	 * 
	 * @return The list of stations.
	 * @throws IOException
	 */
	public static List<RailStation> getStations() throws IOException {
		String fileName = "station.json";
		Instant lastModified = DataUtils.getLastModified(fileName);
		Optional<JsonElement> optional = Optional.empty();
		if(System.currentTimeMillis()-lastModified.toEpochMilli() > UPDATE_INTERVAL) {
			optional = PTXRequest.getJsonElement("v2/Rail/THSR/Station", Collections.emptyMap(), lastModified);
		}
		JsonElement jsonElement = DataUtils.getData(fileName, optional);
		return JsonUtils.GSON.fromJson(jsonElement, RAIL_STATION_LIST_TYPE);
	}
}
