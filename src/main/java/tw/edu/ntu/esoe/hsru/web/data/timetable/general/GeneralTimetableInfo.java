package tw.edu.ntu.esoe.hsru.web.data.timetable.general;

import java.util.Collections;
import java.util.List;

import tw.edu.ntu.esoe.hsru.web.data.timetable.StopTime;
import tw.edu.ntu.esoe.hsru.web.data.timetable.TrainInfo;

//Would merge this into GeneralTimetableEntry but I don't want to make a custom deserializer
public class GeneralTimetableInfo implements Comparable<GeneralTimetableInfo> {

	private TrainInfo generalTrainInfo = new TrainInfo();
	private List<StopTime> stopTimes = Collections.singletonList(new StopTime());
	private ServiceDay serviceDay = new ServiceDay();
	private String srcUpdateTime = "";

	public TrainInfo getGeneralTrainInfo() {
		return generalTrainInfo;
	}

	public List<StopTime> getStopTimes() {
		return stopTimes;
	}

	public ServiceDay getServiceDay() {
		return serviceDay;
	}

	public String getSrcUpdateTime() {
		return srcUpdateTime;
	}

	@Override
	public int compareTo(GeneralTimetableInfo o) {
		return stopTimes.get(0).compareTo(o.stopTimes.get(0));
	}

	@Override
	public String toString() {
		return "GeneralTimetableInfo [generalTrainInfo=" + generalTrainInfo + ", stopTimes=" + stopTimes
				+ ", serviceDay=" + serviceDay + ", srcUpdateTime=" + srcUpdateTime + "]";
	}
}
