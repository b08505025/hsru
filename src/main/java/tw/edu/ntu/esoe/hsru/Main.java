package tw.edu.ntu.esoe.hsru;

import java.io.IOException;
import java.security.GeneralSecurityException;

import javax.swing.SwingUtilities;

import tw.edu.ntu.esoe.hsru.gui.HsruWindow;
import tw.edu.ntu.esoe.hsru.web.SslUtils;

public class Main {

	public static void main(String[] args) {
		try {
			SslUtils.setDefaultSSLSocketFactory();
		}
		catch(IOException | GeneralSecurityException e) {
			throw new RuntimeException(e);
		}
		SwingUtilities.invokeLater(()->{
			HsruWindow.createAndShowGui();
		});
	}
}
