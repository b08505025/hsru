package tw.edu.ntu.esoe.hsru.web.data.timetable.od;

public class ODTimetableData {

	private ODTimetable departureTable = new ODTimetable();
	private ODTimetable destinationTable = new ODTimetable();
	private PriceTable priceTable = new PriceTable();

	public ODTimetable getDepartureTable() {
		return departureTable;
	}

	public ODTimetable getDestinationTable() {
		return destinationTable;
	}

	public PriceTable getPriceTable() {
		return priceTable;
	}

	@Override
	public String toString() {
		return "ODTimetableData [departureTable=" + departureTable + ", destinationTable=" + destinationTable
				+ ", priceTable=" + priceTable + "]";
	}
}
