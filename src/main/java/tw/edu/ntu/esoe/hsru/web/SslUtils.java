package tw.edu.ntu.esoe.hsru.web;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import tw.edu.ntu.esoe.hsru.Main;

/**
 * From https://stackoverflow.com/a/58556946
 */
public class SslUtils {

	/**
	 * Sets the default SSL socket factory to contain the required certificates, and sets the context to TLSv1.2.
	 * @throws IOException
	 * @throws GeneralSecurityException
	 */
	public static void setDefaultSSLSocketFactory() throws IOException, GeneralSecurityException {
		KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
		trustStore.load(null, null);

		trustStore.setCertificateEntry("TWCA", loadCertificateFromResource("twca.cer"));	
		addDefaultRootCaCertificates(trustStore);

		SSLSocketFactory sslSocketFactory = createSslSocketFactory(trustStore);
		HttpsURLConnection.setDefaultSSLSocketFactory(sslSocketFactory);
	}

	private static SSLSocketFactory createSslSocketFactory(KeyStore trustStore) throws GeneralSecurityException {
		TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
		tmf.init(trustStore);
		TrustManager[] trustManagers = tmf.getTrustManagers();
		SSLContext sslContext = SSLContext.getInstance("TLSV1.2");
		sslContext.init(null, trustManagers, null);
		return sslContext.getSocketFactory();
	}

	private static X509Certificate loadCertificateFromResource(String resource) throws CertificateException {
		InputStream certIs = Main.class.getClassLoader().getResourceAsStream(resource);
		return (X509Certificate)CertificateFactory.getInstance("X.509").generateCertificate(new BufferedInputStream(certIs));
	}

	private static void addDefaultRootCaCertificates(KeyStore trustStore) throws GeneralSecurityException {
		TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
		trustManagerFactory.init((KeyStore)null);
		for(TrustManager trustManager : trustManagerFactory.getTrustManagers()) {
			if(trustManager instanceof X509TrustManager) {
				for(X509Certificate acceptedIssuer : ((X509TrustManager)trustManager).getAcceptedIssuers()) {
					trustStore.setCertificateEntry(acceptedIssuer.getSubjectDN().getName(), acceptedIssuer);
				}
			}
		}
	}
}