package tw.edu.ntu.esoe.hsru.ticket;

import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

import tw.edu.ntu.esoe.hsru.data.Station;

public class Trip {

	public static final DateTimeFormatter DATE = DateTimeFormatter.ofPattern("MM/dd");
	public static final DateTimeFormatter TIME = DateTimeFormatter.ofPattern("HH:mm");

	private int bookingId;
	private String bookingUserId;
	private boolean inbound;
	private LocalDate date;
	private String trainNumber;
	private Station fromStation;
	private Station toStation;
	private CarType carType;
	private LocalTime departureTime;
	private LocalTime destinationTime;
	private DiscountType earlyBirdDiscount = DiscountType.NONE;
	private DiscountType studentDiscount = DiscountType.NONE;
	private int price;
	private List<Ticket> tickets;

	public Trip(int bookingId, String bookingUserId, boolean inbound, LocalDate date, String trainNumber,
			Station fromStation, Station toStation, CarType carType, LocalTime departureTime, LocalTime destinationTime,
			DiscountType earlyBirdDiscount, DiscountType studentDiscount, int price, List<Ticket> tickets) {
		this.bookingId = bookingId;
		this.bookingUserId = bookingUserId;
		this.inbound = inbound;
		this.date = date;
		this.trainNumber = trainNumber;
		this.fromStation = fromStation;
		this.toStation = toStation;
		this.carType = carType;
		this.departureTime = departureTime;
		this.destinationTime = destinationTime;
		this.earlyBirdDiscount = earlyBirdDiscount;
		this.studentDiscount = studentDiscount;
		this.price = price;
		this.tickets = tickets;
	}

	public int getBookingId() {
		return bookingId;
	}

	public String getBookingUserId() {
		return bookingUserId;
	}

	public boolean isInbound() {
		return inbound;
	}

	public LocalDate getDate() {
		return date;
	}

	public String getTrainNumber() {
		return trainNumber;
	}

	public Station getFromStation() {
		return fromStation;
	}

	public Station getToStation() {
		return toStation;
	}

	public CarType getCarType() {
		return carType;
	}

	public LocalTime getDepartureTime() {
		return departureTime;
	}

	public LocalTime getDestinationTime() {
		return destinationTime;
	}

	public DiscountType getEarlyBirdDiscount() {
		return earlyBirdDiscount;
	}

	public DiscountType getStudentDiscount() {
		return studentDiscount;
	}

	public int getPrice() {
		return price;
	}

	public List<Ticket> getTickets() {
		return tickets;
	}

	public String[] toDetailsRow() {
		String[] ret = new String[9];
		ret[0] = inbound ? "回程" : "去程";
		ret[1] = DATE.format(date);
		ret[2] = trainNumber;
		ret[3] = fromStation.getNameZh();
		ret[4] = toStation.getNameZh();
		ret[5] = TIME.format(departureTime);
		ret[6] = TIME.format(destinationTime);
		ret[7] = "TWD "+NumberFormat.getInstance().format(price);
		ret[8] = tickets.stream().map(Ticket::getSeat).map(Seat::getNameZh).collect(Collectors.joining(", "));
		return ret;
	}

	public String[] toRemoveRow() {
		String[] ret = new String[8];
		ret[0] = inbound ? "回程" : "去程";
		ret[1] = DATE.format(date);
		ret[2] = trainNumber;
		ret[3] = fromStation.getNameZh();
		ret[4] = toStation.getNameZh();
		ret[5] = TIME.format(departureTime);
		ret[6] = TIME.format(destinationTime);
		ret[7] = "TWD "+NumberFormat.getInstance().format(price);
		return ret;
	}

	Ticket.Type[] removeTickets(int[] indexes) {
		Ticket.Type[] types = new Ticket.Type[indexes.length];
		for(int i = 0; i < indexes.length; ++i) {
			int index = indexes[i];
			if(index < 0 || index >= tickets.size()) {
				continue;
			}
			Ticket ticket = tickets.get(index);
			Ticket.Type type = ticket.getType();
			switch(type) {
			default:
			case ADULT:
				if(earlyBirdDiscount == DiscountType.NONE) {
					price -= Prices.getAdult(fromStation, toStation, carType);
				}
				else {
					price -= Prices.getDiscount(fromStation, toStation, earlyBirdDiscount);
				}
				break;
			case CHILD:
			case DISABLED:
			case SENIOR:
				price -= Prices.getConcession(fromStation, toStation, carType);
				break;
			case STUDENT:
				price -= Prices.getDiscount(fromStation, toStation, studentDiscount);
				break;
			}
			BookingManager.INSTANCE.removeTicket(ticket);
			types[i] = type;
			tickets.set(index, null);
		}
		tickets.removeIf(t->t == null);
		return types;
	}
}
