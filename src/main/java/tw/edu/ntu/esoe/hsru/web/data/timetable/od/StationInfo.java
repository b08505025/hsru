package tw.edu.ntu.esoe.hsru.web.data.timetable.od;

public class StationInfo {

	private String stationNo = "";
	private String stationName = "";
	private String departureTime = "";
	private boolean show = true;
	private String colorClass = "";

	public String getStationNo() {
		return stationNo;
	}

	public String getStationName() {
		return stationName;
	}

	public String getDepartureTime() {
		return departureTime;
	}

	public boolean isShow() {
		return show;
	}

	public String getColorClass() {
		return colorClass;
	}

	@Override
	public String toString() {
		return "StationInfo [stationNo=" + stationNo + ", stationName=" + stationName + ", departureTime="
				+ departureTime + ", show=" + show + ", colorClass=" + colorClass + "]";
	}
}
