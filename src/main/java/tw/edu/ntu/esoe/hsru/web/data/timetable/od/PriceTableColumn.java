package tw.edu.ntu.esoe.hsru.web.data.timetable.od;

public class PriceTableColumn {

	private String discount = "";
	private String columnName = "";
	private double double_CoachPrice = 0;
	private String coachPrice = "";
	private String businessPrice = "";
	private String unreserved = "";

	public String getDiscount() {
		return discount;
	}

	public String getColumnName() {
		return columnName;
	}

	public double getDouble_CoachPrice() {
		return double_CoachPrice;
	}

	public String getCoachPrice() {
		return coachPrice;
	}

	public String getBusinessPrice() {
		return businessPrice;
	}

	public String getUnreserved() {
		return unreserved;
	}

	@Override
	public String toString() {
		return "PriceTableColumn [discount=" + discount + ", columnName=" + columnName + ", double_CoachPrice="
				+ double_CoachPrice + ", coachPrice=" + coachPrice + ", businessPrice=" + businessPrice
				+ ", unreserved=" + unreserved + "]";
	}
}
