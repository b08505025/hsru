package tw.edu.ntu.esoe.hsru.gui;

import javax.swing.table.DefaultTableModel;

/**
 * A TableModel where only Booleans can be modified.
 */
public class ReadOnlyTableModel extends DefaultTableModel {

	/**
	 * Constructs a default <code>ReadOnlyTableModel</code>
	 * which is a table of zero columns and zero rows.
	 */
	public ReadOnlyTableModel() {
		super();
	}

	@Override
	public boolean isCellEditable(int row, int column) {
		return getValueAt(row, column) instanceof Boolean;
	}
}
