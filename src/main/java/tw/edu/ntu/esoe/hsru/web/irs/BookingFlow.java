package tw.edu.ntu.esoe.hsru.web.irs;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;

import org.jsoup.Connection;
import org.jsoup.helper.HttpConnection;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import tw.edu.ntu.esoe.hsru.ticket.BookingForm;
import tw.edu.ntu.esoe.hsru.ticket.DiscountType;
import tw.edu.ntu.esoe.hsru.ticket.TripCandidate;
import tw.edu.ntu.esoe.hsru.ticket.TripCandidateList;
import tw.edu.ntu.esoe.hsru.ticket.TripSelected;
import tw.edu.ntu.esoe.hsru.ticket.TripSelectedList;

/**
 * 
 */
public class BookingFlow {

	public enum State {
		DISCONNECTED,
		INQUIRE,
		SELECT,
		CONFIRM,
		COMPLETE,
		TIMEOUT,
		;
	}

	public static final String BOOKING_URL = "https://irs.thsrc.com.tw";
	public static final String LOCALE_PATH = "/IMINT/?locale=tw";

	public static String[] selectColumns = {
			"車次", "全票優惠*", "出發時間", "到達時間", "行車時間"
	};
	public static String[] selectStudentColumns = {
			"車次", "大學生優惠*", "全票優惠*", "出發時間", "到達時間", "行車時間"
	};

	private State state = State.DISCONNECTED;
	private Connection connection;
	private Map<String, String> cookies = new LinkedHashMap<>();
	private Document inquireDoc;
	private Document selectDoc;
	private Document confirmDoc;
	private Document currentDoc;

	private TripCandidateList tripCandidateList;

	public BookingFlow connect() throws IOException {
		if(state == State.DISCONNECTED) {
			connection = new HttpConnection();
			reconnect();
		}
		return this;
	}

	public BookingFlow reconnect() throws IOException {
		if(connection != null) {
			System.out.println(BOOKING_URL+LOCALE_PATH);
			Connection.Response response = connection.url(BOOKING_URL+LOCALE_PATH).
					method(Connection.Method.GET).cookies(cookies).
					followRedirects(true).execute();
			System.out.println(response.statusCode()+" "+response.statusMessage());
			System.out.println(response.headers());
			cookies = response.cookies();
			currentDoc = response.parse();
			state = State.INQUIRE;
			setCurrentDoc();
		}
		return this;
	}

	public BufferedImage getVeriCodeImage() throws IOException {
		if(state == State.INQUIRE) {
			// I have no idea why this is the only method that worked, other methods hang
			String path = inquireDoc.selectFirst("img#BookingS1Form_homeCaptcha_passCode").attr("src");
			System.out.println(BOOKING_URL+path);
			Connection.Response response = connection.url(BOOKING_URL+path).
					method(Connection.Method.GET).cookies(cookies).
					ignoreContentType(true).followRedirects(true).
					execute();
			System.out.println(response.statusCode()+" "+response.statusMessage());
			System.out.println(response.headers());
			ImageInputStream iis = ImageIO.createImageInputStream(response.bodyStream());
			ImageReader reader = ImageIO.getImageReadersBySuffix("jpg").next();
			reader.setInput(iis);
			return reader.read(0);
		}
		throw new IllegalStateException("Can only get verification code in inquiry state");
	}

	public BookingFlow inquire(BookingForm bookingForm, String veriCode) throws IOException {
		if(state == State.INQUIRE) {
			String path = inquireDoc.selectFirst("form").attr("action");
			System.out.println(BOOKING_URL+path);
			Connection.Response response = connection.url(BOOKING_URL+path).
					method(Connection.Method.GET).cookies(cookies).
					followRedirects(true).data(bookingForm.getFlowData(veriCode)).
					execute();
			System.out.println(response.statusCode()+" "+response.statusMessage());
			System.out.println(response.headers());
			cookies = response.cookies();
			currentDoc = response.parse();
			state = acquireState();
			setCurrentDoc();
			return this;
		}
		throw new IllegalStateException("Can only make train inquiry in inquire state");
	}

	public TripCandidateList getTripCandidateList(BookingForm bookingForm) {
		if(selectDoc != null) {
			Elements outboundElements = selectDoc.select("span#BookingS2Form_TrainQueryDataViewPanel tr");
			String[] outboundColumns = bookingForm.getStudentCount() > 0 ? selectStudentColumns : selectColumns;
			List<TripCandidate> outbound = new ArrayList<>();
			for(int i = 1; i < outboundElements.size(); ++i) {
				outbound.add(getTripCandidate(outboundElements.get(i)));
			}
			Elements inboundElements = selectDoc.select("span#BookingS2Form_TrainQueryDataViewPanel2 tr");
			String[] inboundColumns = new String[0];
			List<TripCandidate> inbound = new ArrayList<>();
			if(!inboundElements.isEmpty()) {
				inboundColumns = bookingForm.getStudentCount() > 0 ? selectStudentColumns : selectColumns;
				for(int i = 1; i < inboundElements.size(); ++i) {
					inbound.add(getTripCandidate(inboundElements.get(i)));
				}
			}
			return new TripCandidateList(outboundColumns, outbound, inboundColumns, inbound);
		}
		throw new IllegalStateException("Can only acquire trip candidate list in select state");
	}

	private TripCandidate getTripCandidate(Element row) {
		String formValue = row.selectFirst("input").attr("value");
		String trainNumber = row.selectFirst("span#QueryCode").text();
		DiscountType studentDiscount = DiscountType.NONE;
		DiscountType earlyBirdDiscount = DiscountType.NONE;
		String departureTime = row.selectFirst("span#QueryDeparture").text();
		String destinationTime = row.selectFirst("span#QueryArrival").text();
		String duration = row.selectFirst("td:has(span#QueryArrival)+td span").text();
		for(String fileName : row.select("img").eachAttr("src")) {
			DiscountType discount = DiscountType.fromFileName(fileName);
			if(discount.isCollegeStudent()) {
				studentDiscount = discount;
			}
			if(discount.isEarlyBird()) {
				earlyBirdDiscount = discount;
			}
		}
		return new TripCandidate(formValue, trainNumber, studentDiscount, earlyBirdDiscount, departureTime, destinationTime, duration);
	}

	public BookingFlow select(TripCandidate outboundCandidate, TripCandidate inboundCandidate) throws IOException {
		if(selectDoc != null) {
			Map<String, String> data = new LinkedHashMap<>();
			data.put("TrainQueryDataViewPanel:TrainGroup", outboundCandidate.getFormValue());
			if(inboundCandidate != null) {
				data.put("TrainQueryDataViewPanel2:TrainGroup", inboundCandidate.getFormValue());
			}
			String path = selectDoc.selectFirst("form").attr("action");
			System.out.println(BOOKING_URL+path);
			Connection.Response response = connection.url(BOOKING_URL+path).
					method(Connection.Method.GET).cookies(cookies).
					followRedirects(true).data(data).
					execute();
			System.out.println(response.statusCode()+" "+response.statusMessage());
			System.out.println(response.headers());
			cookies = response.cookies();
			currentDoc = response.parse();
			state = acquireState();
			setCurrentDoc();
			return this;
		}
		throw new IllegalStateException("Can only select trip candidate in select state");
	}

	public TripSelectedList getTripSelectedList(BookingForm bookingForm) {
		if(confirmDoc != null) {
			Elements elements = confirmDoc.selectFirst("div#content table").select("tr");
			List<String> columns = elements.get(0).children().eachText();
			Element row = elements.get(1);
			String trainNumber = row.select("span#InfoCode0").text();
			String departureTime = row.select("span#InfoDeparture0").text();
			String destinationTime = row.select("span#InfoArrival0").text();
			DiscountType earlyBirdDiscount = DiscountType.NONE;
			DiscountType studentDiscount = DiscountType.NONE;
			for(String fileName : row.select("img").eachAttr("src")) {
				DiscountType discount = DiscountType.fromFileName(fileName);
				if(discount.isCollegeStudent()) {
					studentDiscount = discount;
				}
				if(discount.isEarlyBird()) {
					earlyBirdDiscount = discount;
				}
			}
			TripSelected outbound = new TripSelected(false, bookingForm.getOutboundDate(),
					trainNumber, bookingForm.getFromStation(), bookingForm.getToStation(),
					departureTime, destinationTime,
					bookingForm.getCarType(),
					earlyBirdDiscount, bookingForm.getAdultCount(),
					bookingForm.getConcessionCount(),
					studentDiscount, bookingForm.getStudentCount());
			TripSelected inbound = null;
			if(bookingForm.isRoundTrip()) {
				row = elements.get(2);
				trainNumber = row.select("span#InfoCode1").text();
				departureTime = row.select("span#InfoDeparture1").text();
				destinationTime = row.select("span#InfoArrival1").text();
				earlyBirdDiscount = DiscountType.NONE;
				studentDiscount = DiscountType.NONE;
				for(String fileName : row.select("img").eachAttr("src")) {
					DiscountType discount = DiscountType.fromFileName(fileName);
					if(discount.isCollegeStudent()) {
						studentDiscount = discount;
					}
					if(discount.isEarlyBird()) {
						earlyBirdDiscount = discount;
					}
				}
				inbound = new TripSelected(true, bookingForm.getInboundDate(),
						trainNumber, bookingForm.getToStation(), bookingForm.getFromStation(),
						departureTime, destinationTime,
						bookingForm.getCarType(),
						earlyBirdDiscount, bookingForm.getAdultCount(),
						bookingForm.getConcessionCount(),
						studentDiscount, bookingForm.getStudentCount());
			}
			return new TripSelectedList(columns.toArray(new String[columns.size()]), outbound, inbound);
		}
		throw new IllegalStateException("Can only acquire trip details list in confirm state");
	}

	private State acquireState() throws IOException {
		if(state != State.DISCONNECTED && currentDoc != null) {
			Element element = currentDoc.selectFirst("div#steps");
			if(element == null) {
				return State.TIMEOUT;
			}
			for(int i = 1; i < element.children().size(); ++i) {
				Element e = element.children().get(i);
				if(e.outerHtml().contains("strong")) {
					switch(i) {
					case 1: return State.INQUIRE;
					case 2: return State.SELECT;
					case 3: return State.CONFIRM;
					case 4: return State.COMPLETE;
					}
				}
			}
			return State.TIMEOUT;
		}
		return State.DISCONNECTED;
	}

	private void setCurrentDoc() {
		switch(state) {
		case INQUIRE:
			inquireDoc = currentDoc;
			break;
		case SELECT:
			selectDoc = currentDoc;
			break;
		case CONFIRM:
			confirmDoc = currentDoc;
			break;
		default:
			break;
		}
	}

	public String getErrorMessage() throws IOException {
		if(state == State.TIMEOUT) {
			return "Timed out";
		}
		if(state != State.DISCONNECTED) {
			return currentDoc.selectFirst("div#error").text();
		}
		return "Not connected";
	}

	public State getState() {
		return state;
	}

	public Document getDocument() {
		return currentDoc;
	}
}
