package tw.edu.ntu.esoe.hsru.ticket;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class Seat {

	public static final List<Seat> ALL_SEATS;

	private int id;
	private int car;
	private int row;
	private char column;
	private boolean accessible;
	private String nameZh;
	private Alignment alignment;

	private Seat() {}

	private Seat(int id, int car, int row, char column) {
		this(id, car, row, column, false);
	}

	private Seat(int id, int car, int row, char column, boolean accessible) {
		this.id = id;
		this.car = car;
		this.row = row;
		this.column = column;
		this.accessible = accessible;
		nameZh = new StringBuilder().append(car).append('車').append(row).append(column).toString();
		switch(column) {
		case 'A':
		case 'E':
			alignment = Alignment.WINDOW;
			break;
		case 'C':
		case 'D':
			alignment = Alignment.AISLE;
			break;
		case 'B':
			alignment = Alignment.MIDDLE;
			break;
		}
	}

	public int getId() {
		return id;
	}

	public int getCar() {
		return car;
	}

	public int getRow() {
		return row;
	}

	public char getColumn() {
		return column;
	}

	public boolean isAccessible() {
		return accessible;
	}

	public String getNameZh() {
		return nameZh;
	}

	public Alignment getAlignment() {
		return alignment;
	}

	public boolean matchesCarType(CarType carType) {
		switch(carType) {
		// we assume all trains have non-reserved seats in cars 10-12
		case STANDARD: return car != 6 && car <= 9 && !accessible;
		case BUSINESS: return car == 6;
		default: return false;
		}
	}

	public boolean matchesAlignment(Alignment alignment) {
		return alignment == Alignment.NONE || alignment == this.alignment;
	}

	public static Stream<Seat> getMatchingSeats(CarType carType, Alignment alignment) {
		return ALL_SEATS.stream().filter(s->s.matchesCarType(carType) && s.matchesAlignment(alignment));
	}

	public enum Alignment {
		NONE,
		WINDOW,
		MIDDLE,
		AISLE,
		;
	}

	public enum Serializer implements JsonSerializer<Seat>, JsonDeserializer<Seat> {
		INSTANCE;

		@Override
		public JsonElement serialize(Seat seat, Type typeOfSrc, JsonSerializationContext context) {
			return new JsonPrimitive(seat.id);
		}

		@Override
		public Seat deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
			return ALL_SEATS.get(json.getAsInt());
		}
	}

	static {
		List<Seat> allSeats = new ArrayList<>(989);
		int id = 0;
		int car;
		int row;
		// Car 1
		car = 1;
		row = 1;
		allSeats.add(new Seat(id++, car, row, 'A'));
		allSeats.add(new Seat(id++, car, row, 'B'));
		allSeats.add(new Seat(id++, car, row, 'C'));
		for(row = 2; row <= 13; ++row) {
			allSeats.add(new Seat(id++, car, row, 'A'));
			allSeats.add(new Seat(id++, car, row, 'B'));
			allSeats.add(new Seat(id++, car, row, 'C'));
			allSeats.add(new Seat(id++, car, row, 'D'));
			allSeats.add(new Seat(id++, car, row, 'E'));
		}
		// Car 2
		car = 2;
		row = 1;
		allSeats.add(new Seat(id++, car, row, 'A'));
		allSeats.add(new Seat(id++, car, row, 'B'));
		allSeats.add(new Seat(id++, car, row, 'C'));
		for(row = 2; row <= 19; ++row) {
			allSeats.add(new Seat(id++, car, row, 'A'));
			allSeats.add(new Seat(id++, car, row, 'B'));
			allSeats.add(new Seat(id++, car, row, 'C'));
			allSeats.add(new Seat(id++, car, row, 'D'));
			allSeats.add(new Seat(id++, car, row, 'E'));
		}
		row = 20;
		allSeats.add(new Seat(id++, car, row, 'A'));
		allSeats.add(new Seat(id++, car, row, 'B'));
		allSeats.add(new Seat(id++, car, row, 'C'));
		// Car 3
		car = 3;
		row = 1;
		allSeats.add(new Seat(id++, car, row, 'A'));
		allSeats.add(new Seat(id++, car, row, 'B'));
		allSeats.add(new Seat(id++, car, row, 'C'));
		for(row = 2; row <= 18; ++row) {
			allSeats.add(new Seat(id++, car, row, 'A'));
			allSeats.add(new Seat(id++, car, row, 'B'));
			allSeats.add(new Seat(id++, car, row, 'C'));
			allSeats.add(new Seat(id++, car, row, 'D'));
			allSeats.add(new Seat(id++, car, row, 'E'));
		}
		// Car 4
		car = 4;
		row = 1;
		allSeats.add(new Seat(id++, car, row, 'A'));
		allSeats.add(new Seat(id++, car, row, 'B'));
		allSeats.add(new Seat(id++, car, row, 'C'));
		for(row = 2; row <= 19; ++row) {
			allSeats.add(new Seat(id++, car, row, 'A'));
			allSeats.add(new Seat(id++, car, row, 'B'));
			allSeats.add(new Seat(id++, car, row, 'C'));
			allSeats.add(new Seat(id++, car, row, 'D'));
			allSeats.add(new Seat(id++, car, row, 'E'));
		}
		row = 20;
		allSeats.add(new Seat(id++, car, row, 'A'));
		allSeats.add(new Seat(id++, car, row, 'B'));
		allSeats.add(new Seat(id++, car, row, 'C'));
		// Car 5
		car = 5;
		row = 1;
		allSeats.add(new Seat(id++, car, row, 'A'));
		allSeats.add(new Seat(id++, car, row, 'B'));
		allSeats.add(new Seat(id++, car, row, 'C'));
		for(row = 2; row <= 17; ++row) {
			allSeats.add(new Seat(id++, car, row, 'A'));
			allSeats.add(new Seat(id++, car, row, 'B'));
			allSeats.add(new Seat(id++, car, row, 'C'));
			allSeats.add(new Seat(id++, car, row, 'D'));
			allSeats.add(new Seat(id++, car, row, 'E'));
		}
		// Car 6
		car = 6;
		row = 1;
		allSeats.add(new Seat(id++, car, row, 'D'));
		allSeats.add(new Seat(id++, car, row, 'E'));
		for(row = 2; row <= 17; ++row) {
			allSeats.add(new Seat(id++, car, row, 'A'));
			allSeats.add(new Seat(id++, car, row, 'C'));
			allSeats.add(new Seat(id++, car, row, 'D'));
			allSeats.add(new Seat(id++, car, row, 'E'));
		}
		// Car 7
		for(row = 1; row <= 11; ++row) {
			allSeats.add(new Seat(id++, car, row, 'A'));
			allSeats.add(new Seat(id++, car, row, 'B'));
			allSeats.add(new Seat(id++, car, row, 'C'));
			allSeats.add(new Seat(id++, car, row, 'D'));
			allSeats.add(new Seat(id++, car, row, 'E'));
		}
		row = 12;
		allSeats.add(new Seat(id++, car, row, 'D'));
		allSeats.add(new Seat(id++, car, row, 'E'));
		for(row = 13; row <= 14; ++row) {
			allSeats.add(new Seat(id++, car, row, 'A', true));
			allSeats.add(new Seat(id++, car, row, 'E', true));
		}
		// Car 8
		car = 8;
		row = 1;
		allSeats.add(new Seat(id++, car, row, 'A'));
		allSeats.add(new Seat(id++, car, row, 'B'));
		allSeats.add(new Seat(id++, car, row, 'C'));
		for(row = 2; row <= 19; ++row) {
			allSeats.add(new Seat(id++, car, row, 'A'));
			allSeats.add(new Seat(id++, car, row, 'B'));
			allSeats.add(new Seat(id++, car, row, 'C'));
			allSeats.add(new Seat(id++, car, row, 'D'));
			allSeats.add(new Seat(id++, car, row, 'E'));
		}
		row = 20;
		allSeats.add(new Seat(id++, car, row, 'A'));
		allSeats.add(new Seat(id++, car, row, 'B'));
		allSeats.add(new Seat(id++, car, row, 'C'));
		// Car 9
		car = 9;
		row = 1;
		allSeats.add(new Seat(id++, car, row, 'A'));
		allSeats.add(new Seat(id++, car, row, 'B'));
		allSeats.add(new Seat(id++, car, row, 'C'));
		for(row = 2; row <= 18; ++row) {
			allSeats.add(new Seat(id++, car, row, 'A'));
			allSeats.add(new Seat(id++, car, row, 'B'));
			allSeats.add(new Seat(id++, car, row, 'C'));
			allSeats.add(new Seat(id++, car, row, 'D'));
			allSeats.add(new Seat(id++, car, row, 'E'));
		}
		// Car 10
		car = 10;
		row = 1;
		allSeats.add(new Seat(id++, car, row, 'A'));
		allSeats.add(new Seat(id++, car, row, 'B'));
		allSeats.add(new Seat(id++, car, row, 'C'));
		for(row = 2; row <= 19; ++row) {
			allSeats.add(new Seat(id++, car, row, 'A'));
			allSeats.add(new Seat(id++, car, row, 'B'));
			allSeats.add(new Seat(id++, car, row, 'C'));
			allSeats.add(new Seat(id++, car, row, 'D'));
			allSeats.add(new Seat(id++, car, row, 'E'));
		}
		row = 20;
		allSeats.add(new Seat(id++, car, row, 'A'));
		allSeats.add(new Seat(id++, car, row, 'B'));
		allSeats.add(new Seat(id++, car, row, 'C'));
		// Car 11
		car = 11;
		row = 1;
		allSeats.add(new Seat(id++, car, row, 'A'));
		allSeats.add(new Seat(id++, car, row, 'B'));
		allSeats.add(new Seat(id++, car, row, 'C'));
		for(row = 2; row <= 18; ++row) {
			allSeats.add(new Seat(id++, car, row, 'A'));
			allSeats.add(new Seat(id++, car, row, 'B'));
			allSeats.add(new Seat(id++, car, row, 'C'));
			allSeats.add(new Seat(id++, car, row, 'D'));
			allSeats.add(new Seat(id++, car, row, 'E'));
		}
		// Car 12
		car = 12;
		for(row = 1; row <= 13; ++row) {
			allSeats.add(new Seat(id++, car, row, 'A'));
			allSeats.add(new Seat(id++, car, row, 'B'));
			allSeats.add(new Seat(id++, car, row, 'C'));
			allSeats.add(new Seat(id++, car, row, 'D'));
			allSeats.add(new Seat(id++, car, row, 'E'));
		}
		row = 14;
		allSeats.add(new Seat(id++, car, row, 'A'));
		allSeats.add(new Seat(id++, car, row, 'B'));
		allSeats.add(new Seat(id++, car, row, 'C'));
		ALL_SEATS = Collections.unmodifiableList(allSeats);
	}
}
