package tw.edu.ntu.esoe.hsru.ticket;

import java.text.NumberFormat;
import java.util.Arrays;

public class TripSelectedList {

	private String[] columns = {};
	private TripSelected outbound = new TripSelected();
	private TripSelected inbound = null;

	public TripSelectedList() {}

	public TripSelectedList(String[] columns, TripSelected outbound, TripSelected inbound) {
		this.columns = columns;
		this.outbound = outbound;
		this.inbound = inbound;
	}

	public String[] getColumns() {
		return columns;
	}

	public TripSelected getOutbound() {
		return outbound;
	}

	public TripSelected getInbound() {
		return inbound;
	}

	public int getTotalPrice() {
		return outbound.getTotalPrice() + (inbound != null ? inbound.getTotalPrice() : 0);
	}

	public String getTotalPriceDesc() {
		return "總票價 TWD "+NumberFormat.getInstance().format(getTotalPrice());
	}
}
