package tw.edu.ntu.esoe.hsru.web.data.timetable.od;

import java.util.Collections;
import java.util.List;

public class PriceTable {

	private List<String> coach = Collections.emptyList();
	private List<String> business = Collections.emptyList();
	private List<String> unreserved = Collections.emptyList();
	private List<PriceTableColumn> column = Collections.emptyList();
	
	public List<String> getCoach() {
		return coach;
	}
	
	public List<String> getBusiness() {
		return business;
	}
	
	public List<String> getUnreserved() {
		return unreserved;
	}
	
	public List<PriceTableColumn> getColumn() {
		return column;
	}

	@Override
	public String toString() {
		return "PriceTable [coach=" + coach + ", business=" + business + ", unreserved=" + unreserved + ", column="
				+ column + "]";
	}
}
