package tw.edu.ntu.esoe.hsru.test;

import tw.edu.ntu.esoe.hsru.ticket.Seat;

public class TestSeats {

	public static void main(String[] args) {
		System.out.println(Seat.ALL_SEATS.size());
		int car = 0;
		int row = 0;
		for(Seat seat : Seat.ALL_SEATS) {
			if(car != seat.getCar()) {
				car = seat.getCar();
				System.out.println();
				System.out.print(car);
			}
			if(row != seat.getRow()) {
				row = seat.getRow();
				System.out.println();
				System.out.print(row);
			}
			System.out.print(seat.getColumn());
		}
	}
}
