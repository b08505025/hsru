package tw.edu.ntu.esoe.hsru.test;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.security.GeneralSecurityException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.EnumSet;
import java.util.List;

import tw.edu.ntu.esoe.hsru.web.SslUtils;
import tw.edu.ntu.esoe.hsru.web.data.JsonUtils;
import tw.edu.ntu.esoe.hsru.web.data.station.RailStation;
import tw.edu.ntu.esoe.hsru.web.data.station.StationRequest;
import tw.edu.ntu.esoe.hsru.web.data.timetable.daily.DailyTimetableEntry;
import tw.edu.ntu.esoe.hsru.web.data.timetable.daily.DailyTimetableRequest;
import tw.edu.ntu.esoe.hsru.web.data.timetable.general.GeneralTimetableEntry;
import tw.edu.ntu.esoe.hsru.web.data.timetable.general.GeneralTimetableRequest;
import tw.edu.ntu.esoe.hsru.web.data.timetable.od.DiscountType;
import tw.edu.ntu.esoe.hsru.web.data.timetable.od.ODTimetableData;
import tw.edu.ntu.esoe.hsru.web.data.timetable.od.ODTimetableRequest;

public class TestApiRequest {

	public static void main(String[] args) {
		try {
			SslUtils.setDefaultSSLSocketFactory();
		}
		catch(IOException | GeneralSecurityException e) {
			throw new RuntimeException("Unable to set SSL socket factory.", e);
		}
		try {
			ODTimetableData data = ODTimetableRequest.getTimetableData(
					"TW",
					"TaiPei",
					"ZuoYing",
					false,
					LocalDateTime.of(2021, 6, 20, 6, 0),
					LocalDateTime.of(2021, 6, 20, 6, 0),
					EnumSet.of(DiscountType.EARLY_BIRD));
			try(PrintStream out = new PrintStream(new FileOutputStream("odTimetable.json"))) {
				out.print(JsonUtils.GSON.toJson(data));
			}

			//List<RailStation> stations = StationRequest.getStations();

			//List<GeneralTimetableEntry> generalTimetable = GeneralTimetableRequest.getTimetableEntries();

			//List<DailyTimetableEntry> dailyTimetable = DailyTimetableRequest.getTimetableEntries(LocalDate.now());
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
}
