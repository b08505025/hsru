package tw.edu.ntu.esoe.hsru.test;

import java.text.NumberFormat;
import java.util.Arrays;
import java.util.stream.Collectors;

import tw.edu.ntu.esoe.hsru.data.Station;
import tw.edu.ntu.esoe.hsru.ticket.CarType;
import tw.edu.ntu.esoe.hsru.ticket.DiscountType;
import tw.edu.ntu.esoe.hsru.ticket.Prices;

public class TestPrices {

	public static void main(String[] args) {
		int[][] prices = new int[12][12];
		for(int i = 0; i < 12; ++i) {
			for(int j = 0; j < 12; ++j) {
				Station from = Station.values()[i];
				Station to = Station.values()[j];
				prices[i][j] = Prices.getDiscount(from, to, DiscountType.COLLEGE_STUDENT_88);
			}
		}
		for(int[] row : prices) {
			System.out.println(Arrays.stream(row).mapToObj(p->String.format("%4d", p)).collect(Collectors.joining(" | ")));
		}
		for(int i = 0; i < 12; ++i) {
			for(int j = 0; j < 12; ++j) {
				if(prices[i][j] != prices[j][i]) {
					System.out.println("Mismatch at ("+i+", "+j+")");
				}
			}
		}
	}
}
