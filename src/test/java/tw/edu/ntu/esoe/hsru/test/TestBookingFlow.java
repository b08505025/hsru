package tw.edu.ntu.esoe.hsru.test;

import java.io.IOException;
import java.security.GeneralSecurityException;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import tw.edu.ntu.esoe.hsru.web.SslUtils;
import tw.edu.ntu.esoe.hsru.web.irs.BookingFlow;

public class TestBookingFlow {

	public static void main(String[] args) {
		try {
			SslUtils.setDefaultSSLSocketFactory();
		}
		catch(IOException | GeneralSecurityException e) {
			throw new RuntimeException("Unable to set SSL socket factory.", e);
		}
		BookingFlow flow = new BookingFlow();
		try {
			flow.connect();
			inspect(flow);
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	static void inspect(BookingFlow flow) throws IOException {
		Document doc = flow.getDocument();
		System.out.println("aaaaaa");
		Element element = doc.selectFirst("div#steps");
		for(int i = 1; i < element.children().size(); ++i) {
			element.children().get(i).data();
		}
	}
}
